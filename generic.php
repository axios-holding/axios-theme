<?php
/**
 * Template Name: Generic
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex">
    <?php include("_styles.php"); ?>

    <title>Axios Holding</title>

    <?php include("_metatags.php"); ?>
</head>
<body>

<?php include("_header.php"); ?>

<main id="generic-template" class="axios-bg-light blog">

    <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <div class="hero-content-container"></div>
                <div class="hero-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
            </div>
        </div>
    </div>
    <div class="position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="main-content">
                        <h1>Privacy policy</h1>
                        <p class="intro-text">The purpose of this Privacy Policy (“the Policy”) is to provide adequate safeguards for the processing of Personal Data by Blend Digital Agency (“Agency” or “we”) in accordance with General Data Protection Regulation (GDPR) and any other EU or Cypriot privacy laws. This privacy policy sets out how we use and protect any personal data that you provide when you use this website and our services. BLEND Digital Agency is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.</p>

                        <h2>What we collect</h2>

                        <strong>We may collect the following information:</strong>
                        <ul>
                            <li>name and job title</li>
                            <li>contact information including email address</li>
                            <li>demographic information such as postcode, preferences and interests</li>
                            <li>other information relevant to customer surveys and / or offers.</li>
                        </ul>

                        <h2>Disclosure</h2>

                        <p>We do not disclose your personal data to third parties, except for the purposes described in this Policy. Third party disclosures may include sharing such personal data with non-affiliated companies that perform support services for your account or facilitate your transactions with us, including those that provide professional, legal, or accounting advice to us.</p>

                        <h2>Purposes and legal bases of Personal Data Processing</h2>

                        <strong>We collect and process your personal data:</strong>
                        <ul>
                            <li>For marketing services we believe might be of interest to you by using the email address you provided to us, provided that you are a client/user and/or there is an
                                established commercial relationship between us. If not, we undertake to request your informed consent before processing any of your personal data for specific
                                marketing purposes, as well as for determining correspondence preferences, for example, e-mail, post and social media. You will always have the option to
                                ‘unsubscribe’ from such communication at any time.</li>
                            <li>On the basis of our legitimate interest for the purpose of improving the content and functionality of our website (such as to determine pages of our site visited by
                                users) in connection with notifications of changes to your personal details,</li>
                            <li>On the basis of necessity to perform activities in relation to the services we provide to you as described in the website.</li>
                        </ul>

                        <p>We also collect and process your personal data to comply with applicable laws and regulations, for instance, to protect against fraud, and to otherwise cooperate with law enforcement or regulatory authorities where required.</p>

                        <h2>Your rights</h2>

                        <p>You have the right to access and update your personal information, the right to erasure, the right to portability of your data in a universally used form, the right of objection, and the right to object to automated decision-making. Please note that these rights are not absolute and therefore subject to exceptions under the Community or Cyprus Laws.</p>

                        <h2>data retention</h2>

                        <p>As long as there is no predetermined retention period required by law to retain any of your personal data, we will only retained your personal data for as long as it is necessary for the purposes of processing explained in this Policy. We will then delete them safely taking all appropriate legal and technical measures.</p>

                        <h2>security</h2>

                        <p>We are committed to ensuring that your personal data is secure. In order to prevent unauthorised access or disclosure, we have put in place suitable physical, electronic and managerial procedures to secure the personal data we collect online and prevent any violation or breach of personal data.</p>

                        <h2>links</h2>

                        <p>If you follow a link to any other websites, we encourage you to keep in mind that these sites have their own privacy policies and that we do not take any responsibility for these policies.</p>

                        <h2>how we use cookies</h2>

                        <p>A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.</p>

                        <p>We use traffic log cookies to identify which pages are being used. This helps us analyse data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.</p>

                        <p>Overall, cookies help us provide you with a better website by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or personal data, other than the data you choose to share with us.</p>

                        <p>You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you do not agree to the use of such cookies, you can block cookies by turning on the browser setting that allows you to refuse all cookies or certain cookies. However, we remind you that this may prevent you from accessing all or parts of the site.</p>

                        <h2>revisions</h2>

                        <p>We reserve the right to modify this Policy at any time. Any changes will be posted on this page and, if necessary, will be communicated to you by email.</p>

                        <h2>complaints and contact details</h2>

                        <p>If at any time a person would like to know more or believes that personal data concerning him / her are being processed in violation of this Policy, then he/she can file his/her complaints by sending an email to info@blenddigital.com  or by telephone at +357 22 255001.</p>

                        <h2>supervising authority</h2>

                        <p>You also have the right to file a complaint with the Supervisory Authority, which is the Data Protection Commissioner's Office:</p>

                        <p>
                            <span>1, Iason Street, 1082 Nicosia</span>
                            <span>WHO Vox 23378, 1682 Nicosia</span>
                            <span>Tel: <a href="tel:+35722818456">+357 22818456</a></span>
                            <span>Fax: <a href="tel:+35722304565">+357 22304565</a></span>
                            <span>E-mail: <a href="mailto:commissioner@dataprotection.gov.cy">commissioner@dataprotection.gov.cy</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script>
    $(document).ready(function() {


    });
</script>
</body>
</html>