<?php
/**
 * Template Name: Axios Brands Ru
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
        @media screen and (max-width: 600px){
            .news2_stats h1{
                font-size: 16px;
            }
            .news2_stats h2{
                 font-size: 8px;
             }
            .news2_stats h3{
                font-size: 12px;
            }
        }
        /*start carousel */        
    	@media (min-width: 768px) {

    /* show 3 items */
    .carouselPrograms .carousel-inner .active,
    .carouselPrograms .carousel-inner .active + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        display: block;
    }

    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,
	.carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
        transition: none;
    }

    .carouselPrograms .carousel-inner .carousel-item-next,
    .carouselPrograms .carousel-inner .carousel-item-prev {
        position: relative;
        transform: translate3d(0, 0, 0);
    }

    

    /* left or forward direction */
    .carouselPrograms .active.carousel-item-left + .carousel-item-next.carousel-item-left,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* farthest right hidden item must be abso position for animations */
    .carouselPrograms .carousel-inner .carousel-item-prev.carousel-item-right {
        position: absolute;
        top: 0;
        left: 0%;
        z-index: -1;
        display: block;
        visibility: visible;
    }

    /* right or prev direction */
    .carouselPrograms .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
.carousel-item{
        margin: 2% 1.6%;
        -webkit-transition: none;
        transition: none;
}
img.img-fluid.mx-auto.d-block{
    height: 190px;
    width: 350px;
}
.thumb img{
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
}
.thumb img:hover{
    -webkit-filter: unset;
    filter: unset;
}
.panel-thumbnail:hover .thumb img{
    -webkit-filter: unset;
    filter: unset;
}
.carousel-control-prev, .carousel-control-next{
    width: 2%;
}
.number-overlay{
    position: absolute;
    top: 35%;
    left: 38%;
    color: #fff;
    font-size: 100px;
    text-align: center;
}
.places-to-go {
    height: 110px;
}
.book-info h2, .book-info p{
    color: #fff;
}
.brand_photos img{
    width: 100%;
}
.axios-brands-everfx{
    width: 200px;
}
    </style>
</head>
<body>

    <!--Preloader-->
    <div class="preloader position-fixed w-100">
        <div class="loaderContainer">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>

    <!--Mobile Device Landscape Mode Message-->
    <div class="landscape">
        <div class="landscape__text">Please turn your device</div>
    </div>


    <main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0 newsletter-axios-companies">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                          <li class="d-inline-block px-2"><a href="/axios-universe-ru">Вселенная Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-ru">Бренды Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-ru">Axios в лицах</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-ru">Новости Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-ru">Интересное чтение</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-ru">Axios рекомендует</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-ru">Опрос</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">РУССКИЙ
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-brands-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-brands-en">ENGLISH</a></li>
                                </ul>
                            </div>
                        </ul>
                        <h1 class="text-center underline underline-light inner-template-heading">Знакомство с Axios: EverFX</h1>
                        <div class="col-12 col-md-12 col-lg-6 text-center pb-5 mx-auto">
                            <img class="company-newsletter-size py-5 axios-brands-everfx" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfx-black.png">
                            <p>Наш эксклюзивный партнёр EverFX - это международная брокерская компания, которая со дня своего основания стремится предоставлять 
                            наилучший сервис и техподдержку всем своим клиентам. Компания не только помогает трейдерам выходить на рынки форекс, 
                            но и предлагает необходимые ресурсы и поддержку, чтобы они смогли улучшить свои результаты и сделать трейдинг своей карьерой.
                            </p>
                            <p>
                            По этой причине EverFX активно инвестирует в образовательные ресурсы по трейдингу, 
                            инструменты и инфраструктуру, которая позволяет компании предоставлять широчайший выбор активов по лучшим условиям и наивысшей скоростью исполнения. 
                            </p>
                        </div>
                    </div>
                    <div class="pt-5"></div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="container">
                <div class="row py-5 text-center">  
                    <div class="col-8 py-2 text-center mx-auto">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">EverFX в лицах</h2>
                        <img alt="Anton Safonov" class="pt-5" src="<?php echo get_template_directory_uri(); ?>/assets/img/asafonov.png">
                        <h1 class="text-center inner-template-heading text-white pt-4">Антон Сафонов</h1>
                        <h2 class="text-center inner-template-heading text-white pb-4">Менеджер проекта/продукта</h1>
                        <p class="text-center text-white">EverFX — сложный проект, который требует постоянных ресурсов на разработку и поддержку, непрерывного маркетинга и активного взаимодействия со всеми членами команды. Каждый день — это новые вызовы и "пощёчины", но обороты набираются. Мы движемся в правильном направлении.
                        </p>
                    </div>
                </div>
                <div class="row py-2 text-center">  
                    <div class="col-8 py-2 text-center mx-auto">
                        <img alt="Tatiana Rybchak" src="<?php echo get_template_directory_uri(); ?>/assets/img/trybchak.png">
                        <h1 class="text-center inner-template-heading text-white pt-4">Татьяна Рибчак</h1>
                        <h2 class="text-center inner-template-heading text-white pb-4">Менеджер по персоналу</h1>
                        <p class="text-center text-white">Моя цель как менеджера по персоналу - создание рабочей среды, в которой каждый член нашей многонациональной команды сможет проявлять свои таланты и чувствовать себя частью одной большой семьи.</p>
                    </div>
                </div>
                <div class="row py-2 text-center">  
                    <div class="col-8 py-2 text-center mx-auto">
                        <img alt="Antonis Kazoulis" src="<?php echo get_template_directory_uri(); ?>/assets/img/akazoulis.png">
                        <h1 class="text-center inner-template-heading text-white pt-4">Антонис Казулис</h1>
                        <h2 class="text-center inner-template-heading text-white pb-4">Копирайтер</h1>
                        <p class="text-center text-white">Появляясь в офисе каждое утро, я совсем не ощущаю, что я пришёл на работу. EverFX - это семья. Атмосфера в компании и мои коллеги позволяют мне быть собой и расти как личность и как профессионал. Креатив, оригинальность и проактивность у нас поощряются, и это заставляет меня постоянно поднимать планку и реализовывать весь мой потенциал. Наша компания находится на правильном пути по одной простой причине. Она ценит самый важный ресурс: людей.</p>
                    </div>
                </div>
                <div class="row py-1 text-center">  
                    <div class="col-8 py-2 text-center mx-auto">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">ЖИЗНЬ EVERFX</h2>
                    
                    </div>
                </div>
                <div class="row py-5 text-center">  
                    <div class="col-8 py-2 text-center mx-auto">
                        <h5 class="text-center underline underline-light inner-template-heading text-white p-5">Офис EverFX в Киеве, Украина</h2>

                    </div>
                </div>
                <div class="row py-2 text-center brand_photos">  
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto">
                        <img alt="Kiev Office photo" src="<?php echo get_template_directory_uri(); ?>/assets/img/Kiev_office1.jpg">
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto d-none d-sm-block">
                        <img alt="Kiev Office photo 2" src="<?php echo get_template_directory_uri(); ?>/assets/img/Kiev_office2.jpg">
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto d-none d-sm-block">
                        <img alt="Kiev Office photo 3" src="<?php echo get_template_directory_uri(); ?>/assets/img/Kiev_office3.jpg">
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto">
                        <img alt="Kiev Office photo 4" src="<?php echo get_template_directory_uri(); ?>/assets/img/Kiev_office4.jpg">
                    </div>
                </div>
                <div class="row py-5 text-center">  
                    <div class="col-8 py-2 text-center mx-auto">
                        <h5 class="text-center underline underline-light inner-template-heading text-white p-5">Воллейбольный турнир</h2>

                    </div>
                </div>
                <div class="row py-2 text-center brand_photos">  
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto">
                        <img alt="Everfx Volley photo" src="<?php echo get_template_directory_uri(); ?>/assets/img/volley-1.jpg">
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto d-none d-sm-block">
                        <img alt="Everfx Volley photo 2" src="<?php echo get_template_directory_uri(); ?>/assets/img/volley-2.jpg">
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto">
                        <img alt="Everfx Volley photo 3" src="<?php echo get_template_directory_uri(); ?>/assets/img/volley-3.jpg">
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto d-none d-sm-block">
                        <img alt="Everfx Volley photo 4" src="<?php echo get_template_directory_uri(); ?>/assets/img/volley-4.jpg">
                    </div>
                </div>
                <div class="row py-2 text-center">  
                    <div class="col-8 py-2 text-center mx-auto">
                        <h5 class="text-center underline underline-light inner-template-heading text-white p-5">Work Hard Play Hard</h2>

                    </div>
                </div>
                <div class="row py-2 text-center brand_photos">  
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto">
                        <img alt="EverFX Works Hard Play Hard photo" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfx-works-1.png">
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto d-none d-sm-block">
                        <img alt="EverFX Works Hard Play Hard photo 2" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfxparty2.jpg">
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto">
                        <img alt="EverFX Works Hard Play Hard photo 3" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfx-works-3.jpeg">
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-2 text-center mx-auto d-none d-sm-block">
                        <img alt="EverFX Works Hard Play Hard photo 4" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfx-works-4.jpg">
                    </div>
                </div>
            </div>    
            <div class="row pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x">
                    </div>
                </div>
            </div>
        </div>
</main>

    <footer>
        <div class="axios-bg-white">
            <div class="container py-2 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                    <li class="d-inline-block px-2"><a href="/axios-universe-ru">Вселенная Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-ru">Бренды Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-ru">Axios в лицах</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-ru">Новости Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-ru">Интересное чтение</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-ru">Axios рекомендует</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-ru">Опрос</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">РУССКИЙ
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-brands-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-brands-en">ENGLISH</a></li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <h2 class="pt-3 text-center">THE AXIOS HOLDING <br> NEWSLETTER</h2>
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 cookie-policy-content">
                    <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>

<script>
    $('#carouselExample').on('slide.bs.carousel', function (e) {

    
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
    });





    $(document).ready(function() {
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function(event){
    event.preventDefault();
    var content = $('.modal-body');
    content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    });

    });
</script>
</body>
</html>