<!--JQuery-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery-3.3.1.min.js"></script>
<!--Bootstrap-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/bootstrap.min.js"></script>
<!--TweenLite-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/TweenMax.min.js"></script>
<!--Smoothscroll-->
<!--<script src="assets/js/dist/ScrollToPlugin.js"></script>-->
<!--Animations Library-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/animate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/animation.gsap.min.js"></script>
<!--Modernizer-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/modernizr.js"></script>
<!--Nicescroll-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nicescroll.min.js"></script>
<!--Main Javascript File-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/main.js"></script>
