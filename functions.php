<?php
/**
 * Created by PhpStorm.
 * User: astavrou
 */
add_theme_support( 'post-thumbnails' );
add_theme_support('html5', array('search-form'));

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="page-link"';
}


function pagination_bar($max_pages) {



    $links = paginate_links( array(
        'prev_next'          => false,
        'type'               => 'array',
        'total'  => $max_pages
    ) );

    if ( $links ) :

        echo '<ul class="pagination">';

        // get_previous_posts_link will return a string or void if no link is set.
        if ( $prev_posts_link = get_previous_posts_link( __( '<i class="fas fa-angle-left"></i> Previous' ) ) ) :
            echo '<li class="prev-list-item">';
            echo $prev_posts_link;
            echo '</li>';
        endif;

        echo '<li class="page-item">';
        echo join( '</li><li class="page-item">', $links );
        echo '</li>';

        // get_next_posts_link will return a string or void if no link is set.

        if ( $next_posts_link = get_next_posts_link( __( 'Next <i class="fas fa-angle-right"></i>'), $max_pages ) ) :
            echo '<li class="next-list-item">';
            echo $next_posts_link;
            echo '</li>';
        endif;
        echo '</ul>';
    endif;
}
function show_tags()
{
    $post_tags = get_the_tags();
    
    if (!empty($post_tags)) {
        foreach ($post_tags as $tag) {
            
            $output .= '<span class="d-block tag"><a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a></span>';
        }
        return $output;
    }
}