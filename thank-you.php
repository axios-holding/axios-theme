<?php
/**
 * Template Name: Thank You
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en" style="height: 100%;">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--JQuery-->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery-3.3.1.min.js"></script>
    <!--Bootstrap 4.2.1-->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.css" rel="stylesheet">

    <title>Axios Holding - Thank You</title>
    <style>
        body{
            font-family: "Montserrat", sans-serif;
            cursor: none;
        }
        img{
            width: 25%;
        }


    </style>
    <script>
        $(document).ready(function(){
            setTimeout(function() {
                window.location.href = "home";
            }, 3000);

        });
    </script>
    <?php include("_metatags.php"); ?>
</head>
<body style="height: 100%; background: black;">


<main style="width:100%; height: 100%;">

    <div class="container py-5">
        <div class="row">
            <div class="col-12 mx-auto text-center">
                <h1 class="text-white text-center py-4">We received your message!</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mx-auto text-center">
                <h3 class="text-white text-center py-4 display-3">THANK YOU</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mx-auto text-center">
                <h3 class="text-white text-center py-4">You will now be redirected to the home page</h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-12 mx-auto text-center py-5">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/axios_thank_you.png">
            </div>
        </div>

    </div>
    </div>



</main>


</body>
</html>
