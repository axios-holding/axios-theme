<!--Preloader-->
<div class="preloader position-fixed w-100">
    <div class="loaderContainer">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
</div>

<!--Search-->
<div id="search" class="d-flex">
    <div class="d-flex text-center m-auto search-cont">
        <div class="container">
            <div class="row">
                <div class="col-12 searchmenu__form">
                    <form name="searchForm" id="searchForm">
                        <div class="row">
                            <div class="col-12">
                                <div class="searchmenu__form__field">
                                    <input type="search" name="s" id="searchField" placeholder="Type to search..." autocomplete="off" value="<?php echo get_search_query() ?>">
                                    <a href="#" id="searchIcon"><i  class="far fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3 searchmenu__form__text"><div class="col-12">Hit enter to search or ESC to close</div></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Navbar-->
<div id="menu-navbar" class="mega-menu-container">
    <div class="container">
        <div class="row py-3">
            <div class="col-12 px-4 pt-md-4 d-flex justify-content-between navbar-inner-container">
                <div class="logo-cont">
                    <a href="<?php echo esc_url(home_url());?>">
                        <img alt="axios-logo-horizontal" class="logo-img svg" src="/wp-content/uploads/2020/05/axios-logo_horizontal.svg">
                    </a>
                </div>
                <div class="d-flex position-relative right-cont">
                    <div class="px-md-4 position-absolute d-flex my-auto search">
                        <a id="search-button" class="m-auto" href="#"><i class="far fa-search"></i></a>
                    </div>
                    <div class="px-md-4 position-absolute my-auto toggle-menu">
                        <div id="menu-toggle-button" class="wrapper">
                            <a class="menu-trigger">
                                <span class="line line-1"></span>
                                <span class="line line-3"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Mobile Device Landscape Mode Message-->
<div class="landscape">
    <div class="landscape__text">Please turn your device</div>
</div>
<!--Header Navigation-->
<div id="mega-menu" class="mega-menu">
    <div class="nav-overlay-first"></div>
    <div id="menu" class="mega-menu-content-container">
        <div class="mega-menu-content">
            <div class="container m-auto">
                <div class="row">
                    <div class="col-12">
                        <ul class="text-center">
                            <li><a href="<?php echo esc_url(home_url() . '/about-us/'); ?>">about</a></li>
                            <li><a href="<?php echo esc_url(home_url() . '/what-we-do/'); ?>">what we do</a></li>
                            <li><a href="<?php echo esc_url(home_url() . '/investors-overview/'); ?>">investors</a></li>
                            <li><a href="<?php echo esc_url(home_url() . '/blog-media/'); ?>">blog & media</a></li>
                            <li><a href="<?php echo esc_url(home_url() . '/careers/'); ?>">careers</a></li>
                            <li><a href="<?php echo esc_url(home_url() . '/contact-us/'); ?>">contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>