<?php
/**
 * Template Name: Newsletter 2
 * Created by PhpStorm.
 * User: astavrou
 */?>
 <?php
    header("HTTP/1.1 301 Moved Permanently"); 
    header("Location: https://axiosholding.com/"); 
    exit(); 
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
        @media screen and (max-width: 600px){
            .news2_stats h1{
                font-size: 16px;
            }
            .news2_stats h2{
                 font-size: 8px;
             }
            .news2_stats h3{
                font-size: 12px;
            }
        }
    </style>
</head>
<body>

    <!--Preloader-->
    <div class="preloader position-fixed w-100">
        <div class="loaderContainer">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>

    <!--Mobile Device Landscape Mode Message-->
    <div class="landscape">
        <div class="landscape__text">Please turn your device</div>
    </div>


    <main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <ul class="p-0 text-center mb-4 pb-5">
                            <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
                        </ul>
                        <h2 class="text-center underline underline-light inner-template-heading">OUR UNIVERSE</h2>
                        <div class="content mx-auto">
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                Axios Holdings is not merely a group of FinTech companies. It’s an innovation hub, a breeding ground
                                of ideas, an end-to-end FinTech incubator with innovation resting at its core.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center pb-5">
                                Axios Holding is home to 14 companies which operate in 7 verticals: Education Platforms, Technology Providers, Online Lending, Online Payments, Brokerage & Liquidity, Investment Platforms,
                                and User Engagement Platforms.
                            </p>
                        </div>
                    </div>
                    <div class="pt-5"></div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">EDUCATION PLATFORMS</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/edu2co-newsletter.png">
                        </div>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            Edu2Co, a birthchild of the industry leading brand, “Traders Education”, is poised to disrupt the market.
                            The platform aims at offering a customized educational solution for brokerages all over the world.
                            The company's signature “Knowledge Base” platform contains educational tools, ebooks and 15 online courses with 300 lessons in 30+ topics taught by financial professionals and translated into over
                            30 languages. For the past 6 years, the company has been providing unique technology-driven educational solutions catered to brokers that want to use customer training as a powerful vehicle to increase return
                            on their marketing activities and stand out from the competition.
                        </p>

                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">2012</h1>
                    <h2 class="underline underline-light text-white">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="text-white">Limassol <br> Cyprus</h3>
                    <h2 class="underline underline-light text-white">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">5+</h1>
                    <h2 class="underline underline-light text-white">EMPLOYEES</h2>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5 text-white">MAIN GOALS FOR THE YEAR</h2>
                    <div class="col-12 col-lg-8 px-0 pt-3 mx-auto text-center text-white">
                        <ol class="text-white text-left pb-5">
                            <li>Finalize Traders Education as a product.</li>
                            <li>Kick off the development of the mobile app F2C (Finance to Community)
                                and deliver the proof of concept version.</li>
                        </ol>
                     </div>

                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5 pt-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading">TECHNOLOGY PROVIDERS</h2>
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/new-age-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">New Age Solutions is a technology hub for the companies under the umbrella of Axios Holding. It helps incubate and build startups of the holding by providing them with proprietary tech solutions. In particular,
                                the company develops software for Forex platforms, Forex CRM, and online gaming platforms. Being a part
                                of such a diverse fintech corporation as Axios, New Age Solutions has a broader view compared to other technology providers, which allows it to find creative solutions to the technical problems and develop products
                                of the highest quality. Founded in 2016, New Age Solutions currently employs over 50 people across 3 offices
                                in Ukraine. The company’s vision is to be the tech incubator of Axios Holding and support it throughout
                                its rapid growth.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="">2016</h1>
                    <h2 class="underline underline-light">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="">Kiev <br> Ukraine</h3>
                    <h2 class="underline underline-light">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="">50+</h1>
                    <h2 class="underline underline-light">EMPLOYEES</h2>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5">MAIN GOALS FOR THE YEAR</h2>
                    <div class="col-12 col-lg-8 px-0 pt-3 mx-auto text-center">
                        <ol class=" text-left">
                            <li>Finalize Traders Education as a product.</li>
                            <li>Kick off the development of the mobile app F2C (Finance to Community)
                                and deliver the proof of concept version.</li>
                        </ol>
                    </div>

                </div>
            </div>
            <div class="row mx=0">
                <div class="col-12 px-0">
                    <div class="">
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/overonix-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                Since its establishment in 2019, Overonix has been a leader in providing such technological solutions for Forex industry as CRM system, engagement tools,
                                data warehouse, BI, and Trading Platform. The company sees its mission in developing the most advanced and user-friendly eco system that will solve a wide
                                range of customers’ problems.
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="">2019</h1>
                    <h2 class="underline underline-light">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="">Kiev <br> Ukraine </h3>
                    <h2 class="underline underline-light">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="">30+</h1>
                    <h2 class="underline underline-light">EMPLOYEES</h2>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5">MAIN GOALS FOR THE YEAR</h2>
                    <div class="col-12 col-lg-8 px-0 pt-3 mx-auto text-center">
                        <ol class=" text-left pb-5">
                            <li>Improve UI and UX of our products. </li>
                        </ol>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">ONLINE LENDING</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/equfin-white.png">
                        </div>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            Equfin Holding was established in 2012 with the vision of giving anyone with an Internet connection access to finance through the latest technologies
                            and most advanced channels. The company has been growing fast and steadily ever since, with presence and significant market share in Spain, Georgia and Ukraine,
                            and Armenia. Equfin’s product range includes short-term consumer loans to people with financial deficit. Its customer-driven service and an advanced in-house online
                            lending platform make it a prominent name in the online lending space. The company currently employs over 250 people across its offices in Cyprus, Ukraine, Spain, and
                            Georgia, and Armenia.

                        </p>

                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">2012</h1>
                    <h2 class="underline underline-light text-white">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="text-white">Limassol, Cyprus <br> Tbilisi, Georgia <br> Kiev, Ukraine <br> Madrid, Spain</h3>
                    <h2 class="underline underline-light text-white">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">250+</h1>
                    <h2 class="underline underline-light text-white">EMPLOYEES</h2>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5 text-white">MAIN GOALS FOR THE YEAR</h2>
                    <div class="col-12 col-lg-8 px-0 pt-3 mx-auto text-center text-white">
                        <ol class="text-white text-left">
                            <li>Double the revenue.</li>
                            <li>Enter a new geographical market.</li>
                        </ol>
                        <h2 class="text-center underline underline-light inner-template-heading p-5 text-white">
                            EQUFIN OPERATES UNDER 2 BRANDS: CC LOAN AND MISTERCASH.
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/ccloan-white.png">
                        </div>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            CC Loan is an international company designed to give anyone in need of finance fast and easy access
                            to it at a click of a button. Since its establishment in 2012 in Georgia, CC Loan has been growing steadily
                            and has acquired a significant market share in Ukraine and Spain. Through the company’s in-house online lending platform, any resident of these countries can receive a short-term personal loan in minutes after providing only their ID. The cutting-age technology together with customer-driven service have made
                            CC Loan a leader in the European lending market.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/mistercash-white.png">
                        </div>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            Mistercash is a refreshing new personal loan product; initially designed to be launched in the Spanish market. It incorporates all the great elements of the CCLoan technology
                            along with a new market approach to cater to younger audiences that require immediacy in loan approval and disbursement. An approachable brand that allows us to service a younger
                            market with a quick and simple onboarding process along with frequent offers.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5 pt-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading">ONLINE PAYMENTS</h2>
                        <div class="content mx-auto">
                            <div class="text-center pb-5">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/bigwallet-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                BigWallet, a brand of Silvergate Technologies Limited, is a full-fledged Online Payment Solution Provider.
                                It holds a European Payment Institution license from the Central Bank of Cyprus. It also holds a European EMI license from the Central Bank of Lithuania.
                                BigWallet’s vision is to disrupt the traditional banking sector and give people and companies freedom to receive, use and transfer money whenever and wherever they want,
                                at a click of a button.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="">2018</h1>
                    <h2 class="underline underline-light">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="">Limassol <br> Cyprus</h3>
                    <h2 class="underline underline-light">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="">10+</h1>
                    <h2 class="underline underline-light">EMPLOYEES</h2>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5">MAIN GOALS FOR THE YEAR</h2>
                    <div class="col-12 col-lg-8 px-0 pt-3 mx-auto text-center">
                        <ol class=" text-left pb-5">
                            <li>Have 8 current running merchants boarded on the PSP model.</li>
                            <li>Create a fully operational, user centric, ewallet that caters for all online payment needs.
                            </li>
                        </ol>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">BROKERAGE & LIQUIDITY</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfx-white.png">
                        </div>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            EverFX is a CySec and CIMA regulated broker of CFDs on forex, metals, shares, and commodities
                            that was established in Cyprus in 2016 with the vision of bringing broker and trader closer to one another.
                            The company is determined to lead the way in creating an ideal environment for traders with a user-friendly platform, excellent trading conditions, comprehensive education, and cutting-edge technology.
                            Its transparency and impeccable reputation (zero complaints from institutions, banks and customers since November 2016) together with its unprecedented client-oriented service have made EverFX the “Best New Multi-Asset Broker” in Europe according to Global Brand Awards. With over 100 employees of more than
                            15 nationalities operating in 9 languages from 2 offices in Cyprus and Ukraine, EverFX is rapidly growing
                            and expanding to new international markets. Since 2018, EverFX is an Official Global Partner of Sevilla FC.
                        </p>

                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">2016</h1>
                    <h2 class="underline underline-light text-white">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="text-white">Limassol, Cyprus <br> Kiev, Ukraine </h3>
                    <h2 class="underline underline-light text-white">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">150+</h1>
                    <h2 class="underline underline-light text-white">EMPLOYEES</h2>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5 text-white">MAIN GOALS FOR THE YEAR</h2>
                    <div class="col-12 col-lg-8 px-0 pt-3 mx-auto text-center text-white">
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            Expand globally with innovative technology while maintaining the reputation.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/inflyx-white.png">
                        </div>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            Inflyx is a liquidity provider for brokers, hedge funds, and professional traders who want to have access to tier one liquidity. The company was founded in 2018
                            as part of ICC Capital Ltd to offer a transparent and flexible solution that can satisfy the most demanding liquidity seekers from Europe and Asia and is fully tailored
                            to their needs. Inflyx also provides white label and risk management solutions.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">2018</h1>
                    <h2 class="underline underline-light text-white">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="text-white">Limassol <br> Cyprus </h3>
                    <h2 class="underline underline-light text-white">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">10+</h1>
                    <h2 class="underline underline-light text-white">EMPLOYEES</h2>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5 text-white">MAIN GOALS FOR THE YEAR</h2>
                    <div class="col-12 col-lg-8 px-0 pt-3 mx-auto text-center text-white">
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            Expand the client portfolio.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5 pt-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading">EXCLUSIVE PARTNERS</h2>
                    </div>
                </div>
            </div>
            <div class="row mx=0">
                <div class="col-12 px-0">
                    <div class="">
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/naspay-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                Naspay is an exclusive service provider for Axios Holding. The company offers e-cashier and payment processing services to merchants and other businesses that collect or want to collect money online.
                                Naspay was founded in 2016 by a team of industry experts with a vision to give merchants full control of their financial flows through an intuitive interface. The company’s services match the most advanced security standards (PCI DSS Level 1 and GDPR). It is notable that in 3 years Naspay had only 4 seconds of downtime; making it an industry leader in reliability. This all makes the company a go-to solution for merchants who wish
                                to process payments from all over the globe. Currently Naspay successfully caters for European
                                and Middle East clients.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="">2016</h1>
                    <h2 class="underline underline-light">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="">Limassol <br> Cyprus</h3>
                    <h2 class="underline underline-light">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="">10+</h1>
                    <h2 class="underline underline-light">EMPLOYEES</h2>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5">MAIN GOALS FOR THE YEAR</h2>
                    <div class="col-12 col-lg-8 px-0 pt-3 mx-auto text-center">
                        <ol class=" text-left">
                            <li>Increase our clientele and revenue. </li>
                        </ol>
                    </div>

                </div>
            </div>
            <div class="row mx=0">
                <div class="col-12 px-0">
                    <div class="">
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/fincue-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">Fincue’s cloud based loan management CRM platform is a true leader in the niche. The technology supports
                                the whole business cycle, with full automation on the majority of loan application and disbursement processes and robust integration with third party service
                                providers. The company offers both a sound technological backbone for online lending providers and granular insights in data analysis.
                                A best of breed and speed solution.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="">2018</h1>
                    <h2 class="underline underline-light">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="">Kiev <br> Ukraine</h3>
                    <h2 class="underline underline-light">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="">20+</h1>
                    <h2 class="underline underline-light">EMPLOYEES</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">INVESTMENT PLATFORMS</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/global-innovation-white.png">
                        </div>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            The Global Innovations Fund, established in 2019, provides investors with exposure to the fast-growing verticals in Europe. The purpose of the GI Fund is to
                            provide investors with access to unique opportunities
                            in various sectors of the economy. Its primary focus is in projects relating to Agri & Environment, Energy
                            & Transport, Technology and Real Estate. The GI Fund is an innovative investment vehicle which seeks to pair project owners with international
                            investors; through a rigorous project selection process coupled with a personal approach. The GI Fund team brings together decades of experience at
                            high level investment management with a strong track record in high returns and successful exits.
                        </p>

                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">2019</h1>
                    <h2 class="underline underline-light text-white">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="text-white">Tbilisi <br> Georgia </h3>
                    <h2 class="underline underline-light text-white">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">3+</h1>
                    <h2 class="underline underline-light text-white">EMPLOYEES</h2>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5 text-white">MAIN GOALS FOR THE YEAR</h2>
                    <div class="col-12 col-lg-8 px-0 pt-3 mx-auto text-center">
                        <ol class="text-white text-left pb-5">
                            <li class="text-white">Expand Project Portfolio </li>
                            <li class="text-white">Secure Selected Projects within the Tech, Agri, Real Estate and Infrastructure Sectors</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">ONLINE GAMBLING</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/cerberus-white.png">
                        </div>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center text-white">
                            Founded in 2017 by a team of visionary iGaming veterans, Cerberus is on a mission to give the best online gaming solutions through a
                            data-driven and customer-centric approach. The company’s Horizon Gaming Platform offers a complete, bleeding edge ecosystem containing
                            everything needed to operate an online gambling website: casino hub, payments gateway, CRM, middleware integration and a state of the art
                            Big Data Environment. By developing and providing this reliable, flexible and secure platform, Cerberus intends
                            to change the way iGaming and other digital industries interact with technology.
                        </p>

                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">2017</h1>
                    <h2 class="underline underline-light text-white">YEAR OF ESTABLISHMENT</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h3 class="text-white">Sliema <br> Malta </h3>
                    <h2 class="underline underline-light text-white">LOCATION</h2>
                </div>
                <div class="col-4 text-center mx-auto">
                    <h1 class="text-white">6+</h1>
                    <h2 class="underline underline-light text-white">EMPLOYEES</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>

    </div>


</main>

    <footer>
        <div class="axios-bg-white">
            <div class="container py-4 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                        <ul class="p-0 text-center mb-4">
                            <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <h2 class="pt-3 pb-3">THE AXIOS HOLDING NEWSLETTER</h2>
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 cookie-policy-content">
                    <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>
<script>
    $(window).on('load ', function() {

        var newsletter_scroll_ctrl = new ScrollMagic.Controller();

        /***************
         * newsletter Hero Scroll Reveal Animation
         **************/
        var tween_newsletter_hero = new TimelineMax();
        tween_newsletter_hero.add([
            TweenMax.fromTo("#newsletter .hero-container h2", 1.5,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#newsletter .content", 1.3,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#newsletter .hero-bottom-img .bg-img", 1.1,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
        ]);
        var scene_anewsletter_hero = new ScrollMagic.Scene({
            triggerElement: '#newsletter',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_newsletter_hero.setTween(tween_newsletter_hero);
        scene_newsletter_hero.addTo(newsletter_scroll_ctrl);
        scene_newsletter_hero.reverse(true);

        /***************
         * newsletter Section 1 Scroll Reveal Animation
         **************/
        var tween_newsletter_section_1 = new TimelineMax();
        tween_newsletter_section_1.add([
            TweenMax.staggerFromTo("#newsletter-section-1 .team-container .team-member",0.4, {x: "-220px", opacity: '0'}, {ease: Power2.easeOut, x: 0, opacity: '1'}, 0.25),
        ]);
        var scene_tween_newsletter_section_1 = new ScrollMagic.Scene({
            triggerElement: '.team-container',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_tween_newsletter_section_1.setTween(tween_newsletter_section_1);
        scene_tween_newsletter_section_1.addTo(newsletter_scroll_ctrl);
        scene_tween_newsletter_section_1.reverse(true);


    });
</script>
</body>
</html>