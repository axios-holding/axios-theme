<?php
/**
 * Template Name: Privacy
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>

    <title>Axios Holding - Privacy</title>

    <?php include("_metatags.php"); ?>
</head>
<body>

<?php include("_header.php"); ?>

<main id="generic-template" class="axios-bg-light blog">

    <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <div class="hero-content-container"></div>
                <div class="hero-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
            </div>
        </div>
    </div>
    <div class="position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="main-content">
                        <h1>Privacy policy</h1>
                        <h2>General</h2>
                        <p>
                            Axios Holding is committed to protecting our users’ privacy. Therefore, we would like to give our website visitors the required information according to Article 13 and 14 of the General Data Protection Regulation (“GDPR”) with this Privacy Policy.
                        </p>
                        <p>
                            All your data collected on our website, transferred, processed and maintained is treated in accordance with the principles as stated by the General Data Protection Regulation (GDPR) (EU) 2016/679. We recognise our responsibility to keep your information confidential and secure. Any personal information about you which we obtain in connection with our services provided to you we undertake to keep safe.
                        </p>
                        <p>
                            The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, unless otherwise defined in this Privacy Policy.
                        </p>
                        <h2>Controller</h2>
                        <p>
                            Axios Holding
                        </p>
                        <p>
                            Antheon 2, Monolivko 4, Kato Polemodia, 4151
                        </p>
                        <p>
                            Limassol, Cyprus
                        </p>
                        <p>
                            Email: contact@axiosholding.com
                        </p>
                        <h2>Personal Data</h2>
                        <p>‘Personal Data’ means any information relating to a natural person who can be identified, directly or indirectly (‘data subject’).</p>
                        <h3>How we collect information</h3>
                        <p>All personal information you provide via the Website will be treated as private and confidential.</p>
                        <p>To enable us to provide our services, we need to obtain your Personal Data including your name, your e-mail address, and other relevant details to tailor your needs. The data you enter in the online form or that you share with us at events or exhibitions includes your name, your job title, email address, telephone number and your place of work. All this information can be considered personally identifiable information.</p>
                        <h3>How we use your information</h3>
                        <p>Your Personal Data will only be processed for specific, explicit and legitimate purposes. In particular, Personal Data will be processed for the following purposes:</p>
                        <ul>
                            <li>To respond to your inquiries and fulfil your requests, such as to respond to your questions and comments</li>
                            <li>To send administrative information to you, for example, information regarding our websites and changes to our Terms and Conditions</li>
                            <li>To re-contact you if we have not heard from you in a while</li>
                            <li>To offer and facilitate the provision of services upon your request</li>
                            <li>To improve our service and developing new services</li>
                            <li>To resolve conflicts, manage litigation, resolve issues, and provide you customer service (including troubleshooting in connection with customer issues)</li>
                            <li>The execution of risk management</li>
                            <li>The processing of transactions</li>
                            <li>To complete and fulfil your service-order, have your order delivered to you, communicate with you regarding the service and provide you with related customer service</li>
                            <li>To provide you with updates and announcements concerning our products, promotions and programs and to send you invitations to participate in special programs</li>
                            <li>To personalise your experience on the website by presenting products and offers tailored to you</li>
                            <li>For our business purposes, such as analysing and managing our businesses, market research, audits, developing new products, enhancing our websites, identifying usage trends, determining the effectiveness of our promotional campaigns and gauging customer satisfaction</li>
                            <li>As we believe to be necessary or appropriate: (a) under applicable law, including laws outside your country of residence; (b) to comply with legal process; (c) to respond to requests from public and government authorities, including public and government authorities outside your country of residence; (d) to enforce our terms and conditions; (e) to protect our operations or those of any of our affiliates; (f) to protect our rights, privacy, safety or property, and/or that of our affiliates, you or others; and (g) to allow us to pursue available remedies or limit the damages that we may sustain</li>
                            <li>Protection of the legal rights and interests of Axios Holding, including, but not limited to, the discovery, conduct or defence of legal rights</li>

                        </ul>

                        <h2>Legal Basis</h2>
                        <p>Axios Holding is processing only your Personal Data where one or more of the following conditions apply:</p>
                        <ul>
                            <li>The data subject has given consent to the processing of his or her Personal Data for one or more specific purposes</li>
                            <li>Processing is necessary for the performance of a contract to which the data subject is party or in order to take steps at the request of the data subject prior to entering into a contract</li>
                            <li>Processing is necessary for compliance with a legal obligation to which the controller is subject</li>
                            <li>Processing is necessary for the purposes of the legitimate interests pursued by Axios Holding or by a third party, except where such interests are overridden by the interests or fundamental rights and freedoms of the data subject which require protection of Personal Data, in particular where the data subject is a child</li>
                        </ul>
                        <h2>
                            Log Data
                        </h2>
                        <p>
                            We want to inform you that whenever you use our service or visit our websites, we collect information that your browser sends to us that is called Log Data. This Log Data includes information such as your computer’s Internet Protocol (“IP”) address, browser version, pages of our service that you visit, the time and date of your visit, the time spent on those pages, and other statistics.
                        </p>
                        <h2>
                            Cookies
                        </h2>
                        <p>In the course of your visit to our website, your computer may be issued with cookies. Cookies are files containing a small amount of data that is commonly used as an anonymous unique identifier. These are sent to your browser from our website when you visit and are stored on your computer’s hard drive.</p>
                        <p>
                            Our website uses these “cookies” to collect information and to improve our service. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your computer. If you choose to refuse our cookies, some portions of our service will not be available any longer.
                        </p>
                        <p>
                            Cookies are commonly used on the Internet and do not harm your system. Cookies have a number of uses.
                        </p>
                        <p>
                            The cookies used on our website (https://axiosholding.com) fall into three categories:
                        </p>
                        <p>
                            Functional: These cookies are used to enable core website functionality and do not contain any personal information.
                        </p>
                        <p>
                            Analytics: These cookies allow us to count page visits and traffic sources, so we can monitor and improve the performance of our website.
                        </p>
                        <p>
                            Third Party: We partner with affiliate networks and other websites to help promote Axios Holding. If you use their websites, or have come to our site via these affiliates, then their cookies will be sent through our website.
                        </p>
                        <p>
                            You can delete and block cookies at any time from this site through your browser, but some features on this site will not function without cookies.
                        </p>

                        <p>
                            You can change the preferences or settings in your web browser to control cookies. In some cases, you can choose to accept cookies from the primary site but block them from third parties. In others, you can block cookies from specific advertisers, or clear out all cookies.
                        </p>

                        <h2>
                            Transfer of Data to Third-Parties
                        </h2>

                        <p>
                            We employ third-party companies and individuals that may be located outside of the European Economic Area (EEA) due to the following reasons:
                        </p>

                        <ul>
                            <li>To facilitate our service</li>
                            <li>To provide the service on our behalf</li>
                            <li>To perform service-related services</li>
                            <li>To assist us in analysing how our service is used</li>
                        </ul>

                        <p>We want to inform our service users that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>

                        <p>Personal information will only be transferred in the following circumstances:</p>

                        <ul>
                            <li>To other companies that provide us services. We share Personal Data with other partners who perform services and functions on our behalf. These partners, for example, provide services to you as defined in our service contracts</li>
                            <li>To financial institutions with whom we work together to develop or provide a product or service</li>
                            <li>To other parties when you use their services, such as: to merchants, and service providers: We may disclose information to other participants in your transactions when you use the services. The information we share includes: person-related data required to complete the transaction</li>
                            <li>Personal Data needed by other transactional participants to resolve conflicts and to investigate and prevent fraud</li>
                            <li>Anonymised data and performance analytics that help better understand the use of our services and increase the satisfaction of our customers</li>
                            <li>To third parties for our business purposes or as permitted or required by law</li>
                            <li>To protect the essential interests of a person</li>
                            <li>To investigate violations of any User Agreement or other legal provision applicable to our services or to enforce such legal instruments to protect our assets, services and rights</li>
                        </ul>
                        <p>To fulfil some of our processes we must pass your Personal Data to other parts of our group companies, which may be in other countries. We make sure that they agree to apply the same levels of protection as we are required to apply to information held in the UK and to use your information only for the purpose to provide our service to you. In any event Axios Holding will capture and process such personal information in accordance with the requirements set out in the Data Protection Regulation (EU 2016-679)</p>
                        <p>Currently, Axios Holding employees in the following countries can access your Personal Data: Cyprus.</p>

                        <h2>Links to Other Websites</h2>
                        <p>
                            Our service contains links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over, and take no responsibility for, the content, privacy policies, or practices of any third-party sites or services.
                        </p>
                        <h2>Storage of Data</h2>
                        <p>We retain personal information in an identifiable format as long as required by law or regulation, or as needed for our business purposes. We retain personal information for longer periods of time than is legally required if it is in our legitimate business interests and is not prohibited by law.</p>
                        <h2>Data Protection Rights</h2>
                        <p>As the Affected Person, you have the right at any time to obtain information about your stored Personal Data, its origin and the recipient as well as the purpose of the data processing. You also have the right to correct and transmit your data and, if necessary, to object to, restrict the processing of, or deleting of, your Personal Data.</p>
                        <p>If you want us to execute your Data Protection Rights as described above, you can request this by sending us an email to: contact@axiosholding.com</p>
                        <p>If you believe that the processing of your Personal Data by Axios Holding violates the applicable data protection law or your data protection claims have been violated in another way, you may file a complaint with the competent supervisory authority.</p>
                        <h2>Children’s Privacy</h2>
                       <p> We do not knowingly collect personal identifiable information from or about children under 13 years of age. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to take necessary actions.</p>
                        <h2>Data encryption and Technical Security Measures</h2>
                        <p>To prevent the illegal manipulation through a third person, the IP address of the logged-on computer will be requested and saved. In addition, all your Personal Data is protected from unauthorised access by a firewall – a computer that is fitted with complex security technology specifically designed to shield the company’s network from the Internet. Additionally, the company uses reliable internal data protection mechanisms combined with a restrictive security system.</p>
                        <h2>Contacting us</h2>
                        <p>If you have any questions about this Privacy Policy, do not hesitate to contact us at: contact@axiosholding.com</p>
                        <h2>Changes to this Privacy Policy</h2>
                        <p>We reserve the right to amend this Privacy Policy as necessary, for example due to technical developments or legal changes, or to update it in connection with the offer of new services or products. The updated Privacy Policy will be published on our website. Please check the relevant page regularly.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script>
    $(document).ready(function() {


    });
</script>
</body>
</html>