<?php
/**
 * Template Name: Axios fintech reads ru
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
        #search-results #blog-results .articles-container .article-cont {
            max-width: 450px;
            overflow: hidden;
        }
        #search-results #blog-results .articles-container .article .article-cont a .article-img-cont .article-img-cont-in .bg-img {
            width: 450px;
            height: 220px;
        }
        @media screen and (min-width: 576px) and (max-width: 991px){
            #search-results #blog-results .articles-container .article .article-cont a .article-img-cont .article-img-cont-in .bg-img {
                max-width: 450px;
                height: 220px;
                width: 240px;
            }
        }
        @media screen and (min-width: 768px) and (max-width: 991px){
            #search-results #blog-results .articles-container .article .article-cont a .article-img-cont .article-img-cont-in .bg-img {
                max-width: 450px;
                height: 220px;
                width: 330px;
            }
        }
    </style>
</head>
<body>

<?php include("_header.php"); ?>


<main id="search-results" class="axios-bg-light blog">

    <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <div class="hero-content-container"></div>
                <div class="hero-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
            </div>
        </div>
    </div>
    <div class="main-content py-5 position-relative">
    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
    <li class="d-inline-block px-2"><a href="/axios-universe-ru">Вселенная Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-ru">Бренды Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-ru">Axios в лицах</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-ru">Новости Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-ru">Интересное чтение</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-ru">Axios рекомендует</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-ru">Опрос</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">РУССКИЙ
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/fintech-reads-es">ESPAÑOL</a></li>
                                    <li><a href="/fintech-reads-en">ENGLISH</a></li>
                                </ul>
                            </div>
                        </ul>
        <div class="newsletter-axios-companies">
            <h1 class="axios-text-dark text-center underline underline-light inner-template-heading">Интересное чтение</h1>
        </div>    
        <div id="blog-results">
            <div class="container articles-container">
                <div class="row pt-3 pt-lg-4">
                    <div class="col-12 col-sm-6 pb-5 article">
                        <div class="mx-auto article-cont bg-white">
                            <a href="https://axiosholding.com/under-the-microscope-elon-musk/" class="text-center" target="_blank">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="Elon Musk" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/elon-musk-fintech.png"></div> </div></div>
                                <div class="row mx-auto"><h2 class="p-4">Под микроскопом: Илон Маск</h2></div>
                                <div class="row mx-auto"><p style="text-align: center;" class="px-4">
                                Илон Маск - личность неординарная. Предприниматель из ЮАР стал синонимом инновации, технологии и прогресса. Человеком, раздвигающим границы мышления до такой степени, что его часто сравнивают с Томасом Эдисоном, Генри Фордом, Стивом Джобсом и Биллом Гейтсом. Пионер, первооткрыватель, трендсеттер и настоящий герой нашего времени.
                                </p></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 pb-5 article">
                        <div class="mx-auto article-cont bg-white">
                            <a href="https://axiosholding.com/apple-card-everything-you-need-to-know/" class="text-center" target="_blank">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="AppLe Card" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/apple-card-fintech.png"></div> </div></div>
                                <div class="row mx-auto"><h2 class="p-4 mx-auto">Всё, что вам нужно знать <br> о Apple Card</h2></div>
                                <div class="row mx-auto"><p style="text-align: center;" class="px-4">
                                Apple продолжает динамично развиваться и удивлять нас новыми системными обновлениями, дополнительными сервисами и впечатляющими проектами.От iPhone и классического iPod до макбуков, Apple TV, Apple news, iTunes and Airpods. В очередной раз компания подняла свою планку, создав собственную кредитную карту.
                                    </p></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 pb-5 article">
                        <div class="mx-auto article-cont bg-white">
                            <a href="https://bigwallet.com/3-keys-to-improving-your-mobile-payments-strategy/" class="text-center" target="_blank">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="Mobile Payment" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/mobile-payment-fintech.png"></div> </div></div>
                                <div class="row mx-auto"><h2 class="p-4">3 ключа к улучшению вашей <br> стратегии мобильных платежей</h2></div>
                                <div class="row mx-auto"><p style="text-align: center;" class="px-4">
                                Когда 67% населения Земли (5,1 млрд людей) подписаны на тот или иной мобильный сервис, самое время всерьёз задуматься о переходе на мобильные платежи. Вот в чём заключаются 3 ключа к успешной стратегии мобильных платежей.
                                    </p></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 pb-5 article">
                        <div class="mx-auto article-cont bg-white">
                            <a href="https://axiosholding.com/klarna-eus-top-fintech-startup/" class="text-center" target="_blank">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="Klarna" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/klarna-fintech.png"></div> </div></div>
                                <div class="row mx-auto"><h2 class="p-4">Klarna: топовый финтех стартап Европы</h2></div>
                                <div class="row mx-auto"><p style="text-align: center;" class="px-4">
                                        Что если мы вам скажем, что вы можете покупать любые вещи в кредит без процентов? Держу пари, вы такого не ожидали. Тогда познакомьтесь с Klarna.
                                    </p></div>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>
<footer>
        <div class="axios-bg-white">
            <div class="container py-4 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                    <li class="d-inline-block px-2"><a href="/axios-universe-ru">Вселенная Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-ru">Бренды Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-ru">Axios в лицах</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-ru">Новости Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-ru">Интересное чтение</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-ru">Axios рекомендует</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-ru">Опрос</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">РУССКИЙ
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/fintech-reads-es">ESPAÑOL</a></li>
                                    <li><a href="/fintech-reads-en">ENGLISH</a></li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <h2 class="pt-3 text-center">THE AXIOS HOLDING <br> NEWSLETTER</h2>
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
</footer>
<div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
    <div class="container">
        <div class="row">
            <div class="col-12 py-4 cookie-policy-content">
                <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
            </div>
        </div>
    </div>
</div>

<div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nice-select.min.js"></script>
<script>

    $(window).on('load ', function() {

        var tweenPosts =new TimelineMax()
        tweenPosts.add([
            TweenMax.staggerFromTo("#blog-results .article",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1', delay:0.8}, 0.15),
        ]);


    });
</script>
</body>
</html>