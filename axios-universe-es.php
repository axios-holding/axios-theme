<?php
/**
 * Template Name: Axios Universe Es
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
        @media screen and (max-width: 600px){
            .news2_stats h1{
                font-size: 16px;
            }
            .news2_stats h2{
                 font-size: 8px;
             }
            .news2_stats h3{
                font-size: 12px;
            }
        }
        /*start carousel */        
    	@media (min-width: 992px) {

    /* show 3 items */
    .carouselPrograms .carousel-inner .active,
    .carouselPrograms .carousel-inner .active + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        display: block;
    }

    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,
	.carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
        transition: none;
    }

    .carouselPrograms .carousel-inner .carousel-item-next,
    .carouselPrograms .carousel-inner .carousel-item-prev {
        position: relative;
        transform: translate3d(0, 0, 0);
    }

    

    /* left or forward direction */
    .carouselPrograms .active.carousel-item-left + .carousel-item-next.carousel-item-left,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* farthest right hidden item must be abso position for animations */
    .carouselPrograms .carousel-inner .carousel-item-prev.carousel-item-right {
        position: absolute;
        top: 0;
        left: 0%;
        z-index: -1;
        display: block;
        visibility: visible;
    }

    /* right or prev direction */
    .carouselPrograms .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
.carousel-item{
        margin: 2% 1.6%;
        -webkit-transition: none;
        transition: none;
}
img.img-fluid.mx-auto.d-block{
    height: 190px;
    width: 350px;
}
.thumb img{
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
}
.thumb img:hover{
    -webkit-filter: unset;
    filter: unset;
}
.panel-thumbnail:hover .thumb img{
    -webkit-filter: unset;
    filter: unset;
}
.carousel-control-prev, .carousel-control-next{
    width: 2%;
}
.number-overlay{
    position: absolute;
    top: 35%;
    left: 38%;
    color: #fff;
    font-size: 100px;
    text-align: center;
}
.places-to-go {
    height: 110px;
}
.carousel-control-next {
    right: -5px;
}
.places-to-go .company-newsletter-size{
    width: auto;
}
.newsletter-axios-companies-img img{
    width: 200px;
}
ol.text-white.text-center.pb-5.ol-top-things {
    padding: 0;
}
@media screen and (min-width:992px) and (max-width:1140px){
    .news2_stats h2{
        font-size: 12px;
    }
    .news2_stats h3{
        font-size: 11px;
    }
    .news2_stats p{
        font-size: 10px;
    }
}

    </style>
</head>
<body>

    <!--Preloader-->
    <div class="preloader position-fixed w-100">
        <div class="loaderContainer">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>

    <!--Mobile Device Landscape Mode Message-->
    <div class="landscape">
        <div class="landscape__text">Please turn your device</div>
    </div>


    <main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                        <li class="d-inline-block px-2"><a href="/axios-universe-es">Universo Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-es">Empresas Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-es">Axios en caras</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-es">Noticias de Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-es">Lecturas Interesantes</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-es">Axios Recomienda</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-es">Encuesta Axios</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ESPAÑOL
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-universe-en">ENGLISH</a></li>
                                    <li><a href="/axios-universe-ru">РУССКИЙ</a></li>
                                </ul>
                            </div>
                        </ul>
                        <h2 class="text-center underline underline-light inner-template-heading">La Geografía de Axios</h2>
                        <div class="content mx-auto">
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center text-black">
                            En esta nueva sección de nuestro boletín, os presentamos los países donde las empresas de Axios operan. Un poco de geografía no le hará daño a nadie, ¿verdad? Todo lo contrario, os ayudará para aprender más sobre las partes diferentes del Universo Axios y así conocer más a vuestros colegas de toda Europa. 
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center pb-5 text-black">
                                <strong>Nuestro héroe para hoy es Ucrania, uno de los países más grandes del continente y por cierto, patria de la mayoría de los empleados de Axios. 
                                </strong>
                            </p>
                        </div>
                    </div>
                    <div class="pt-5"></div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">Aquí hay algunos datos sobre Ucrania:</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/kiev.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-6 col-md-6 col-lg-3 text-center mx-auto">
                    <h2 class="underline underline-light text-white">Ubicación</h2>
                    <h3 class="text-white"><strong>Europa del Este</strong></h3>
                </div>
                <div class="col-6 col-md-6 col-lg-3 text-center mx-auto">
                    <h2 class="underline underline-light text-white">Población</h2>
                    <h3 class="text-white"><strong>42+M</strong></h3>
                </div>
                <div class="col-6 col-md-6 col-lg-3 text-center mx-auto">
                    <h2 class="underline underline-light text-white">Idioma</h2>
                    <h3 class="text-white"><strong>Ucraniano</strong></h3>
                    <p class="text-center text-white">(con una gran parte de la población, especialmente en las regiones central y oriental, de habla también rusa) </p>
                </div>
                <div class="col-6 col-md-6 col-lg-3 text-center mx-auto">
                    <h2 class="underline underline-light text-white">CAPITAL</h2>
                    <h3 class="text-white"><strong>Kiev</strong></h3>
                    <p class="text-center text-white">( y patria de 5 empresas Axios)</p>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5 text-white">Las 5 mejores cosas que puedes hacer en Kiev:</h2>
                </div>
        
            </div>
            <div class="row pb-5">
                <!-- start of carousel -->
                <div id="carouselExample" class="carouselPrograms carousel  mx-auto" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner row w-100 mx-auto" role="listbox">
                            <div class="carousel-item col-lg-2 col-md-12  active">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 1" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-1.png" alt="slide 1">
                                    </a>
                                    <h1 class="number-overlay">1</h1>
                                </div>
                                </div>
                            </div>
                            <div class="carousel-item col-lg-2 col-md-12  ">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 3" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-2.png" alt="slide 2">
                                    </a>
                                    <h1 class="number-overlay">2</h1>
                                </div>
                                </div>
                            </div>
                            <div class="carousel-item col-lg-2 col-md-12  ">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 4" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-3.png" alt="slide 3">
                                    </a>
                                    <h1 class="number-overlay">3</h1>
                                </div>
                                </div>
                            </div>
                            <div class="carousel-item col-lg-2 col-md-12  ">
                                <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 5" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-4.png" alt="slide 4">
                                    </a>
                                    <h1 class="number-overlay">4</h1>
                                </div>
                                </div>
                            </div>
                            <div class="carousel-item col-lg-2 col-md-12  ">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 6" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-5.png" alt="slide 5">
                                    </a>
                                    <h1 class="number-overlay">5</h1>
                                </div>
                                </div>
                            </div>
                            
                        </div>
                        <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                        <!-- end of carousel -->
                    <div class="col-12 pt-3 justify-content-center text-white">
                            <ol class="text-white text-center pb-5 ol-top-things">
                                <li>Caminar la calle famosa Khreschatyk hasta la Plaza de la Independencia (Maidan Nezalezhnosti)</li>
                                <li>Visitar las iglesias y las cuevas de Kyiv Pechersk Lavra </li>
                                <li>Caminar la Bajada de Andriivski ( o subirla para ejercitarse)</li>
                                <li>Usar en el funicular</li>
                                <li>Probar comida tradicional ucraniana: borsch, vareniki y salo</li>
                            </ol>
                 </div>
            </div>
            <div class="row justify-content-md-center pb-5 text-white text-center">
            <div class="col-12 px-0">
                 <h2 class="text-center inner-template-heading p-5 text-white">Lugares para visitar:</h2>
            </div>
                <div class="col col-lg-2">
                <div class="places-to-go">
                    <img class="company-newsletter-size" alt="puzata hata" src="<?php echo get_template_directory_uri(); ?>/assets/img/to-go-1.png">
                </div>
                    <p class="text-white">
                        Puzata Hata ($) <br> Zhizn Zamechatelnyh Lyudey ($$) <br> Nam ($$$)
                    </p>
                </div>
                <div class="col-lg-2">
                  <div class="places-to-go">
                    <img class="company-newsletter-size" alt="Lviv Croissants" src="<?php echo get_template_directory_uri(); ?>/assets/img/to-do-2.png">
                  </div>
                    <p class="text-white">
                         Lviv Croissants <br> One Love espresso bar <br> Chashka
                    </p>
                </div>
                <div class="col col-lg-2">
                    <div class="places-to-go">
                     <img class="company-newsletter-size" alt="Loggerhead" src="<?php echo get_template_directory_uri(); ?>/assets/img/to-do-3.png">
                    </div>
                    <p class="text-white">
                         Loggerhead <br> Parovoz <br> Kedy iskusstvoveda
                    </p>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator newsletter-block-separator-gray separator-bottom position-absolute fixed-bottom angled-separator flip-x separate-gray">
                    </div>
                </div>
            </div>
        </div>
        <!-- Start of Axios Companies -->
        <div class="container-fluid bg-gray newsletter-axios-companies newsletter-axios-companies-img">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5 pt-5">
                    <div class="">
                        <h1 class="text-center inner-template-heading py-4 px-2">Las empresas de Axios con oficinas en Kiev</h1>
                        <h2 class="text-center underline underline-light inner-template-heading">Proveedores de tecnología</h2>
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/new-age-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                            NewAge Solutions es un centro de tecnología para las empresa dentro de la cobertura de Axios Holdings. La firma ayuda en desarrollar y construir nuevas empresas del grupo al proporcionarles soluciones tecnológicas propias. En particular, la empresa desarrolla programas para plataformas Forex, el CRM Forex, y plataformas de participación de usuarios en línea. La visión de la empresa es ser la incubadora de la tecnolog
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                <a href="https://newage.io/" target="_blank"> Aprended más </a>
                            </p>    
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx=0">
                <div class="col-12 px-0">
                    <div class="">
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/fincue-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                            La plataforma CRM de gestión de préstamos basada en la nube de Fincue es un verdadero líder en el nicho. La tecnología es compatible con todo el ciclo comercial, con una automatización total en la mayoría de los procesos de solicitud y desembolso de préstamos e integración robusta con proveedores de servicios externos. La empresa ofrece una sólida  columna vertebral tecnológica a proveedores de préstamos en línea y una visión granular en el análisis de datos. La mejor solución por su clase y velocidad.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                <a href="http://www.fincue.com/" target="_blank"> Aprended más </a>
                            </p>  
                            
                        </div>
                    </div>
                </div>
            </div> <!-- END OF FINCUE -->
            <div class="row mx=0">
                <div class="col-12 px-0">
                    <div class="">
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/overonix-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                            Desde su creación en 2017, Overonix ha sido líder en el suministro de soluciones tecnológicas para la industria Forex cómo el sistema CRM, las herramientas de interacción, el almacén de datos y el motor de trading. La empresa centra su misión en desarrollar sistemas más avanzados y fáciles de usar que resolverán una amplia gama de problemas de clientes.
                            </p>
                        </div>

                    </div>
                </div>
            </div><!-- end of overonix -->
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading p-5">Préstamo en línea</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/equfin-black.png">
                        </div>
                        <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                        Equfin Holding se fundó en 2012, con la visión de dar acceso a finanzas a cualquier persona con conexión a Internet. Usando las últimas tecnologías y los canales más avanzados. La empresa ha estado creciendo rápida y constantemente desde entonces, con presencia y una significativa participación en España, Georgia y Ucrania, y Armenia. La gama de productos de Equfin incluye préstamos de consumo a corto plazo a personas con déficit financiero.

                        </p>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                <a href="https://equfin.com/" target="_blank"> Aprended más </a>
                        </p>  

                    </div>
                </div>
            </div><!-- end of equfin -->
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading p-5">Corretaje & Liquidez</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfx-black.png">
                        </div>
                        <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                            Un socio exclusivo de Axios Holding. EverFX es un Bróker forex de CFDs regulado por CySec y CIMA, metales y materias primas. Establecido en Chipre el 2016, con la visión de traer al Bróker y al trader a una relación más cercana. Su transparencia y reputación impecable (cero quejas de instituciones, bancos y clientes desde noviembre de 2016) junto con su servicio orientado al cliente, sin precedentes, han convertido a EverFX en el "Mejor nuevo corredor de activos múltiples" en Europa según los Premios de Marca Global. Desde 2018, EverFX es el socio global oficial del Sevilla FC.
                        </p>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                <a href="https://everfx.com/" target="_blank"> Aprended más </a>
                        </p>  

                    </div>
                </div>
            </div><!-- end of everfx -->
            
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div><!-- End of Axios Companies -->
        <div class="container-fluid axios-bg-white newsletter-axios-companies">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5 pt-5">
                    <div class="">
                        <h1 class="text-center underline inner-template-heading">AXIOS EN UCRANIA </h1>
                        <h2 class="text-center underline underline-light inner-template-heading p-5">85% de empleados en Axios</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="ukraine statistics" src="<?php echo get_template_directory_uri(); ?>/assets/img/ukraine-stats.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">Empleados por nacionalidad</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/nationality.png">
                        </div>
                        

                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>
</main>

    <footer>
        <div class="axios-bg-white">
            <div class="container py-4 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                    <li class="d-inline-block px-2"><a href="/axios-universe-es">Universo Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-es">Empresas Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-es">Axios en caras</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-es">Noticias de Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-es">Lecturas Interesantes</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-es">Axios Recomienda</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-es">Encuesta Axios</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ESPAÑOL
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-universe-en">ENGLISH</a></li>
                                    <li><a href="/axios-universe-ru">РУССКИЙ</a></li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <h2 class="pt-3 text-center">THE AXIOS HOLDING <br> NEWSLETTER</h2>
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 cookie-policy-content">
                    <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>

<script>
    $('#carouselExample').on('slide.bs.carousel', function (e) {

    
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
    });





    $(document).ready(function() {
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function(event){
    event.preventDefault();
    var content = $('.modal-body');
    content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    });

    });
</script>
</body>
</html>