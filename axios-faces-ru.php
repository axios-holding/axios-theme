<?php
/**
 * Template Name: Axios Faces Ru
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
            ol, li {
            font-family: "Nunito Sans", sans-serif;
            font-size: 15px;
        }
            .newsletter-interview p {
            color: #000;
        }
        @media screen and (max-width: 600px){
            .news2_stats h1{
                font-size: 16px;
            }
            .news2_stats h2{
                 font-size: 8px;
             }
            .news2_stats h3{
                font-size: 12px;
            }
        }
        /*start carousel */        
    	@media (min-width: 768px) {

    /* show 3 items */
    .carouselPrograms .carousel-inner .active,
    .carouselPrograms .carousel-inner .active + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        display: block;
    }

    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,
	.carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
        transition: none;
    }

    .carouselPrograms .carousel-inner .carousel-item-next,
    .carouselPrograms .carousel-inner .carousel-item-prev {
        position: relative;
        transform: translate3d(0, 0, 0);
    }

    

    /* left or forward direction */
    .carouselPrograms .active.carousel-item-left + .carousel-item-next.carousel-item-left,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* farthest right hidden item must be abso position for animations */
    .carouselPrograms .carousel-inner .carousel-item-prev.carousel-item-right {
        position: absolute;
        top: 0;
        left: 0%;
        z-index: -1;
        display: block;
        visibility: visible;
    }

    /* right or prev direction */
    .carouselPrograms .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
.carousel-item{
        margin: 2% 1.6%;
        -webkit-transition: none;
        transition: none;
}
img.img-fluid.mx-auto.d-block{
    height: 190px;
    width: 350px;
}
.thumb img{
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
}
.thumb img:hover{
    -webkit-filter: unset;
    filter: unset;
}
.panel-thumbnail:hover .thumb img{
    -webkit-filter: unset;
    filter: unset;
}
.carousel-control-prev, .carousel-control-next{
    width: 2%;
}
.number-overlay{
    position: absolute;
    top: 35%;
    left: 38%;
    color: #fff;
    font-size: 100px;
    text-align: center;
}
.places-to-go {
    height: 110px;
}
@media screen and (max-width: 598px){
    iframe{
        width: 100%!important;
    }
}
    </style>
</head>
<body>

    <!--Preloader-->
    <div class="preloader position-fixed w-100">
        <div class="loaderContainer">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>

    <!--Mobile Device Landscape Mode Message-->
    <div class="landscape">
        <div class="landscape__text">Please turn your device</div>
    </div>


    <main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0 newsletter-axios-companies">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                    <li class="d-inline-block px-2"><a href="/axios-universe-ru">Вселенная Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-ru">Бренды Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-ru">Axios в лицах</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-ru">Новости Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-ru">Интересное чтение</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-ru">Axios рекомендует</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-ru">Опрос</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">РУССКИЙ
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-faces-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-faces-en">ENGLISH</a></li>
                                </ul>
                            </div>
                        </ul>
                        <h1 class="text-center underline underline-light inner-template-heading">Axios в лицах: Давид Алимбарашвили </h1>
                        <div class="content mx-auto">
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center text-black">
                            Мы продолжаем знакомить вас с лицами Axios Holding - людьми, которые вкладывают свои усилия, талант и упорство в развитие нашего инновационного центра финансовых технологий.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center pb-5 text-black">
                            Герой этого выпуска - Давид Алимбарашвили, операционный директор Equfin Holdings. В ходе короткого интервью с нашей PR менеджером Марией Бабченко Давид не только поделился своими главными жизненными уроками, но и рассказал, что делает его счастливым и куда он бы отправился в путешествие мечты.
                            </p>
                        </div>
                    </div>
                    <div class="pt-5"></div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="container newsletter-interview">
       
                <div class="row mx=0">
                    <div class="col-12 px-0 pb-5">
                        <div class="">
                            <h2 class="text-center inner-template-heading text-white p-5">MEET DAVID ALIMBARASHVILI, <br> COO EQUFIN HOLDINGS</h2>
                            <div class="text-center pb-5">
                                <img style="width: 220px;" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/david-alimbarashvili.jpg">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row m-md-5 bg-white p-5"  style="margin-top: -160px!important;">
                        <div class="col-md-6 p-5">
                            <p>Maria B.: <strong>Здравствуйте, Давид! Вы готовы принять вызов Axios и ответить на 7 личных вопросов?</strong></p>
                            <p>David A.: <strong>Давайте попробуем!</strong></p>
                            <p><strong>Какое у Вас хобби? Чем Вы занимаетесь в свободное время?</strong></p>
                            <p>Мне нравится плавать и кататься на лыжах. Я даже был полупрофессиональным пловцом. Мне также нравится читать. Раньше я много читал, но из-за постоянных командировок у меня почти не остаётся времени на чтение чего-то помимо профессиональной литературы. Но всё-таки мне иногда удаётся найти время для чтения некоторых бестселлеров, например “Sapiens. Краткая история человечества” Юваля Ноя Харари.</p>
                            <p><strong>Представьте, что прямо сейчас Вы можете оказаться в любой точке мира. Куда бы Вы отправились?</strong></p>
                            <p>В Нью Йорк. Как ни странно, я там ни разу не был..</p>
                            <p><strong>У Вас есть любимое место, город или страна для путешествий, куда Вам хочется возвращаться снова и снова?</strong></p>
                            <p>Мне нравится разнообразие, поэтому я не могу сказать, что у меня есть какое-то любимое направление. Но вообще я очень люблю путешествовать, и мне без разницы, куда я еду: в Казахстан, Испанию или Замбию. Когда я куда-то отправляюсь, мне важно, чтобы у меня было время на осмотр как центра города, так и его окрестностей.</p>
                        </div>
                        <div class="col-md-6 p-5">
                            <p><strong>Как выглядит Ваш идеальный уикенд?</strong></p>
                            <p>Шашлыки с друзьями и семьёй во дворе моего дома.</p>
                            
                            <p><strong>Какое приложение на телефоне Вы чаще всего используете?</strong></p>
                            <p>Facebook - чтобы быть в курсе того, что происходит в жизни членов семьи и друзей, которые живут в разных уголках планеты. Ну и чтобы следить за новостями из нашей отрасли, разумеется.</p>

                            <p><strong>Какими тремя жизненными уроками Вы бы хотели поделиться с миром?</strong></p>
                            <p>
                                <ol>
                                    <li>Если вы хотите добиться успеха, поставьте себе цель, разработайте план её достижения и терпеливо и упорно следуйте ему каждый день.</li>
                                    <li>Если на вашем пути вы встречаете препятствие, сделайте шаг назад и воспользуйтесь по максимуму теми данными, которые у вас есть. Отступая назад, делая передышку, отвлекаясь от ежедневной рутины, вы даёте возможность вашему мозгу найти решение проблемы, которое вы так долго искали. А если вам кажется, что то решение, которое вам пришло, не очень рационально, сделайте ещё один шаг назад и поспите на нём. Как говорится, утро вечера мудренее. На следующий день и на свежую голову вы точно сможете определить, является ли решение рациональным или нет.</li>
                                    <li>Нужно принимать ответственность за свои поступки. Мы должны быть не менее требовательны к себе чем к другим.</li>
                                </ol>
                            </p>

                            <p><strong>Какое Ваше определение счастья?</strong></p>
                            <p>
                                Счастье для меня - это скорее процесс чем конечная цель. Но ключ к счастью - это понимание того, чего конкретно вы хотите от жизни.
                            </p>

                        </div>

                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>
</main>

    <footer>
        <div class="axios-bg-white">
            <div class="container py-4 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                    <li class="d-inline-block px-2"><a href="/axios-universe-ru">Вселенная Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-ru">Бренды Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-ru">Axios в лицах</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-ru">Новости Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-ru">Интересное чтение</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-ru">Axios рекомендует</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-ru">Опрос</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">РУССКИЙ
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-faces-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-faces-en">ENGLISH</a></li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <h2 class="pt-3 text-center">THE AXIOS HOLDING <br> NEWSLETTER</h2>
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 cookie-policy-content">
                    <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>

<script>
    $('#carouselExample').on('slide.bs.carousel', function (e) {

    
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
    });





    $(document).ready(function() {
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function(event){
    event.preventDefault();
    var content = $('.modal-body');
    content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    });

    });
</script>
</body>
</html>