<?php
/**
 * Template Name: main page
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Axios Holding | A group of Financial Technology companies, an innovation hub, a breeding ground of ideas, plus an end-to-end FinTech incubator with innovation.">

    <!--Slick Slider Styles-->
<!--    <link href="assets/slick/slick.css" rel="stylesheet">-->
<!--    <link href="assets/slick/slick-theme.css" rel="stylesheet">-->
    <?php include("_styles.php"); ?>

    <title>Axios Holding</title>

    <?php include("_metatags.php"); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139538735-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-139538735-1');
    </script>
</head>
<body>

<?php include("_header.php"); ?>

<main class="home">
    <!--Hero Section-->
    <div class="home-hero position-relative">
        <div class="home-hero-video">

        </div>
        <div class="position-absolute d-flex justify-content-center w-100 home-hero-header">
            <h1 class="text-white text-center">Powered by Technology
                <span class="d-block thin" style="font-weight: 400!important;">designed by people</span>
            </h1>
            <div id="indicator" class="animate-indicator">
                <div id="scroll-indicator-animation">
                    <span class="d-block scroll-indicator"></span>
                </div>
            </div>
        </div>
        <div class="home-block-separator angled-separator flip-x hero-separator"></div>
    </div>

    <!--Section 1-->
    <div id="home-section-1" class="py-5 home-section-1 axios-bg-light">
        <div class="container py-4 animate-fade">
            <div class="row">
                <div class="col-12 col-sm-10 col-lg-8 mx-auto text-center">
                    <h2 class="axios-text-dark underline">Our Ecosystem</h2>
                    <p>Axios Holding is not merely a group of FinTech companies. It’s an innovation hub, a breeding ground of ideas, an end-to-end FinTech incubator with innovation resting at its core.</p>
                    <a class="btn-axios btn-axios-dark" href="<?php echo esc_url(home_url() . '/what-we-do/');?>">view all verticals</a>
                </div>
            </div>
        </div>
    </div>

    <!--Section 2-->
    <div id="home-section-2" class="home-section-2 position-relative axios-bg-dark">
        <div class="home-block-separator separator-top position-absolute fixed-top angled-separator invert separator-bg-none"></div>
        <div class="home-section-2-container container animate-fade">
            <div class="row">
                <div class="col-12 col-sm-10 col-lg-6 mx-auto text-center">
                    <h2 class="axios-text-light-white underline underline-light">INVESTORS</h2>
                    <p class="axios-text-light">Personal and professional relationships empower Axios to break new ground and innovate. Investments in capital, ideas and intellectual property are sewed in the fabric of our culture and raison d'etre. Read more about our Investor Relations here.</p>
                    <a class="btn-axios btn-axios-light" href="<?php echo esc_url(home_url() . '/investors-overview/');?>">LEARN MORE</a>
                </div>
            </div>
        </div>
        <div class="home-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x-y separator-bg-none"></div>
    </div>

    <!--Section 3-->
    <div id="home-section-3" class="py-5 home-section-3 axios-bg-light">
        <div class="container py-4 animate-fade">
            <div class="row">
                <div class="col-12 col-sm-10 col-lg-6 mx-auto text-center">
                    <h2 class="axios-text-dark underline">CAREERS at axios Holding</h2>
                    <p>Talent, expertise and creativity rest within our core. We’re always on the hunt for exceptional people that possess these qualities and propel the Axios team into the future. Check our Careers page for vacancies.</p>
                    <a class="btn-axios btn-axios-dark" href="<?php echo esc_url(home_url() . '/careers/'); ?>">new vacancies</a>
                </div>
            </div>
        </div>
    </div>

    <!--Section 4-->
    <div id="home-section-4" class="home-section-4 position-relative axios-bg-dark">
        <div class="home-block-separator separator-top position-absolute fixed-top angled-separator invert flip-y separator-bg-none"></div>
        <div class="home-section-4-container py-2 py-sm-5 animate-fade">
            <div class="container articles-container">
                <div class="row"><div class="m-auto text-center"><h2 class="axios-text-light-white underline">news headlines</h2></div> </div>
                <div class="row pt-3 pb-4">
                    <?php
                    // the query
                    $the_query = new WP_Query( array(
                        'posts_per_page' => 3,
                        'category__not_in' => 2 ,
                    ));
                    ?>

                    <?php if ( $the_query->have_posts() ) : ?>
                        <?php $count = 0; ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <?php if ($count == 0) {?>
                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="mx-auto article-cont">
                                        <a href="<?php the_permalink(); ?>" class="text-center">
                                            <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div> </div></div>
                                            <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                                            <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                                        </a>
                                    </div>
                                </div>
                            <?php  } ?>
                            <?php if ($count == 1) {?>
                                <div class="d-none d-sm-block col-12 col-sm-6 col-md-4">
                                    <div class="mx-auto article-cont">
                                        <a href="<?php the_permalink(); ?>" class="text-center">
                                            <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div> </div></div>
                                            <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                                            <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($count == 2) {?>
                                <div class="d-none d-md-block col-12 col-sm-6 col-md-4">
                                    <div class="mx-auto article-cont">
                                        <a href="<?php the_permalink(); ?>" class="text-center">
                                            <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div> </div></div>
                                            <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                                            <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                                        </a>
                                    </div>
                                </div>
                            <?php  } ?>

                            <?php $count++ ; endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                    <?php else : ?>
                        <p><?php __('No Postss'); ?></p>
                    <?php endif; ?>
                <!--<div class="col-12 col-sm-6 col-md-4">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_1.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                            </a>
                        </div>
                    </div>-->
                    <!-- <div class="d-none d-sm-block col-12 col-sm-6 col-md-4">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_2.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="text">Cras et augue varius dolor molestie interdum nec quis libero</span></div>
                            </a>
                        </div>
                    </div>-->
                    <!-- <div class="d-none d-md-block col-12 col-sm-6 col-md-4">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_3.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="text">Sed sed magna et elit malesuada lobortis.</span></div>
                            </a>
                        </div>
                    </div>-->
                    <div class="pt-5 mx-auto text-center all-articles-cont"><a class="btn-axios btn-axios-light" href="<?php echo esc_url(home_url() . '/blog/'); ?>">all news</a></div>
                </div>
            </div>
        </div>
        <div class="home-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>
</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<!--Count To Number Javascript File-->
<script>
    $(window).on('load resize orientationchange', function() {
        if ($(window).width() > 767) {
            $video = '<video autoplay muted loop poster="/wp-content/uploads/2020/05/hero_video_poster.jpg" id="bgvid"><source src="/wp-content/uploads/2020/05/heroVideo.mp4" type="video/mp4"></video>';
            $('.home-hero-video').html($video);
        } else {
            $img = '<div class="bg-img video-bg-img"></div>';
            $('.home-hero-video').html($img);
        }
    });
    $(window).on('load ', function() {

        var homepage_scroll_ctrl = new ScrollMagic.Controller();


        /***************
         * Home Section 1 Scroll Reveal Animation
         **************/
        var tween_homepage_section_1 = new TimelineMax();
        tween_homepage_section_1.add([
            TweenMax.fromTo("#home-section-1 .animate-fade h2", 1.5,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#home-section-1 .animate-fade p", 1.3,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#home-section-1 .animate-fade .btn-axios", 1.1,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
        ]);
        var scene_homepage_homepage_section_1 = new ScrollMagic.Scene({
            triggerElement: '#home-section-1',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_homepage_homepage_section_1.setTween(tween_homepage_section_1);
        scene_homepage_homepage_section_1.addTo(homepage_scroll_ctrl);
        scene_homepage_homepage_section_1.reverse(true);

        /***************
         * Home Section 2 Scroll Reveal Animation
         **************/
        var tween_homepage_section_2 = new TimelineMax();
        tween_homepage_section_2.add([
            TweenMax.fromTo("#home-section-2 .animate-fade h2", 1.5,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#home-section-2 .animate-fade p", 1.3,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#home-section-2 .animate-fade .btn-axios", 1.1,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
        ]);
        var scene_homepage_homepage_section_2 = new ScrollMagic.Scene({
            triggerElement: '#home-section-2',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_homepage_homepage_section_2.setTween(tween_homepage_section_2);
        scene_homepage_homepage_section_2.addTo(homepage_scroll_ctrl);
        scene_homepage_homepage_section_2.reverse(true);

        /***************
         * Home Section 3 Scroll Reveal Animation
         **************/
        var tween_homepage_section_3 = new TimelineMax();
        tween_homepage_section_3.add([
            TweenMax.fromTo("#home-section-3 .animate-fade h2", 1.5,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#home-section-3 .animate-fade p", 1.3,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#home-section-3 .animate-fade .btn-axios", 1.1,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
        ]);
        var scene_homepage_homepage_section_3 = new ScrollMagic.Scene({
            triggerElement: '#home-section-3',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_homepage_homepage_section_3.setTween(tween_homepage_section_3);
        scene_homepage_homepage_section_3.addTo(homepage_scroll_ctrl);
        scene_homepage_homepage_section_3.reverse(true);

        /***************
         * Home Section 4 Scroll Reveal Animation
         **************/
        var tween_homepage_section_4 = new TimelineMax();
        tween_homepage_section_4.add([
            TweenMax.fromTo("#home-section-4 .animate-fade h2", 1.5,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.staggerFromTo("#home-section-4 .animate-fade .article-cont",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1'}, 0.15),
        ]);
        var scene_homepage_homepage_section_4 = new ScrollMagic.Scene({
            triggerElement: '#home-section-4',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_homepage_homepage_section_4.setTween(tween_homepage_section_4);
        scene_homepage_homepage_section_4.addTo(homepage_scroll_ctrl);
        scene_homepage_homepage_section_4.reverse(true);


    });
</script>
</body>
</html>