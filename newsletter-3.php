<?php
/**
 * Template Name: Newsletter 3
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
        ol{
            font-family: "Nunito Sans", sans-serif;
            font-size: 15px;
            font-weight: 400;
            line-height: 24px;
            color: #666;
            margin-bottom: 30px;
            margin-left: 15px;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
</head>
<body>
<!--Preloader-->
<div class="preloader position-fixed w-100">
    <div class="loaderContainer">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
</div>

<!--Mobile Device Landscape Mode Message-->
<div class="landscape">
    <div class="landscape__text">Please turn your device</div>
</div>


<main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <ul class="p-0 text-center mb-4 pb-5">
                            <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
                        </ul>
                        <h2 class="text-center underline underline-light inner-template-heading">AXIOS FACES</h2>
                        <div class="content mx-auto">
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                In each edition of our newsletter, we shall be meeting people who work at Axios Holding in different positions, different companies and different countries.
                                Each edition will contain an interview with someone from the staff or the management team in which they will tell their story and uncover what they are like
                                outside office hours.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center pb-5">
                                The character of our first newsletter is the lady familiar to everyone who works at Axios (or at least at its headquarters).
                                This is the Head of HR Aliki Karmiotou, who is based at the Axios headquarters in Limassol, Cyprus.
                            </p>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center inner-template-heading text-white p-5">MEET ALIKI, HEAD OF HR OF AXIOS HOLDING,
                            LIMASSOL, CYPRUS</h2>
                        <div class="text-center pb-5">
                            <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-meet-aliki.png">
                        </div>

                    </div>
                </div>
            </div>
            <div class="row m-md-5 bg-white p-5"  style="margin-top: -160px!important;">
                    <div class="col-md-6 p-5">
                        <h2> Hi, Aliki! Thank you for agreeing to become the first character of the Axios newsletter!</h2>

                       <p>It’s my pleasure!</p>

                        <h2>First, tell me your story. Where do you come from and how did you choose your occupation?</h2>

                        <p>
                            I come from the vibrant city of Limassol in Cyprus. After I graduated from school, I went to Athens to study Social Anthropology and Psychology at Panteion University which was the best years of my life!!!
                        </p>
                        <p>
                            After those wonderful years i came back to Cyprus, and I started looking around for my next educational step. While looking around about Marketing, International Law, Psychology i came across a section on Organizational Behavior. That was it!
                        </p>
                        <p>
                            I decided to study something related to my degrees but at the time practical for the business industry in Cyprus. So I enrolled in the “Human Resources and Organizational Behavior” Master Degree of the Cyprus International Institute of Management, and this was the best choice I’ve ever made! I loved the variety and in the business world, the understanding of business practises and systems.It was the right mix for me.
                            Since then, almost ten years now, i am working in the HR field.
                        </p>
                        <h2>What do you like about working at Axios Holding?</h2>

                        <p>
                            I love my job very very much. I like interacting with people. And I also like that you never know what’s going to happen every day because everything depends on the mood of people you are going to interact with, on what’s going on in their personal and business life. Being an HR manager isn’t a boring job at all. You don’t do the same things every day. When you enter the office in the morning, you don’t know how your day will turn out to be. And for me this is the best part of my job.
                        </p>
                        <p>
                            I’m also lucky,  because as most organization Leaders nowadays, and especially here in Axios, they recognize the critical nature of the HR function and the need to have professional knowledge and HR expertise working through and giving advice on the organizational complexities .They understand that HR had to be part of the transition process right from the beginning and that makes me more loyal,confident and keeps me motivated.
                        </p>
                        <h2>
                            What are the three main lessons you’ve learnt in your life that you’d like to share with the world?
                        </h2>
                        <p>I have five important lessons to share:</p>
                        <ol>
                            <li>Be patient.</li>
                            <li>Develop emotional intelligence.</li>
                            <li>Listen to people and care about them. Give them a chance to prove something.</li>
                            <li>Keep trying even if you fail.</li>
                            <li>And most importantly, believe in yourself!</li>
                        </ol>
                    </div>
                    <div class="col-md-6 p-5">
                        <h2>What do you do besides your work? What are your hobbies, interests or any sports you do?</h2>

                        <p>
                            Jogging, music and of course going out with my friends to my favorite places.
                        </p>

                        <h2>
                            What is your favorite travel destination?
                        </h2>
                        <p>Amsterdam in April - May. I enjoy the city and also its culture and people.  </p>

                        <h2>
                            What is your dream destination?
                        </h2>

                        <p>
                            Not a dream one but in progress right now it’s Vietnam and Singapore.
                        </p>

                        <h2>
                            What is your definition of happiness?
                        </h2>

                        <p>
                            Happiness it's like pizza.its small pieces of certain categories such us, friends, family, personal time, love etc etc.
                        </p>
                        <p>
                            You can cut as many pieces as you like and start eating and enjoy them.Its your pizza after all, you can put anything you want on it.
                        </p>

                       <h2>
                           What would you do if you were the mayor of Limassol
                           for a day?
                       </h2>

                        <p>
                            One day isn’t enough as there are too many things to do. But I would definitely start with fixing the streets and sidewalks and creating bikeways throughout the town.
                        </p>


                    </div>

            </div>
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center inner-template-heading text-white p-5">WHAT IS YOUR PERSONAL MOTTO?
                        </h2>
                        <h1 class="text-center inner-template-heading text-white p-5">Believe in yourself. Its only YOU against YOU. Beat yourself!
                        </h1>

                    </div>
                </div>
            </div>
            <div class="row m-md-5 bg-white p-5" style="margin-bottom: 150px!important;">
                <div class="col-md-6">
                    <h2> And now we'll have a series of short questions. I say a word and you tell me your first association with it. Ready?</h2>

                    <p>Yes! Let's do it.</p>

                    <h2>The music:</h2>

                    <p>
                        Contemporary British and American music (Nina Simone,Damien Rice, Lana del Rey, Sade) and Swing music.
                    </p>

                    <h2>The film:</h2>

                    <p>
                        I don't have a favorite film but my favorite TV series is "Friends"
                    </p>
                    <h2>The flower:</h2>

                    <p>
                        Gardenia and sunflowers
                    </p>

                    <h2>The breakfast:</h2>

                    <p>
                        A benedict egg with spinach, avocado and mushrooms
                    </p>

                    <h2>The time of the year:</h2>

                    <p>
                        Summer of course!
                    </p>

                    <h2>The cuisine:</h2>

                    <p>
                        All!!! But if I really have to say something then I'll pick Lebanese and Indian
                    </p>

                    <h2>The sport:</h2>

                    <p>
                        Beach rackets
                    </p>

                    <h2>Ice cream flavor:</h2>

                    <p>
                        Mocca & lemon
                    </p>

                    <h2>Theater or cinema?</h2>

                    <p>
                       Theater
                    </p>


                </div>
                <div class="col-md-6">

                    <h2>Coffee or tea?</h2>

                    <p>
                        Coffee
                    </p>

                    <h2>Sweets or fruit?</h2>

                    <p>
                        Fruit
                    </p>

                    <h2>Mountains or sea?</h2>

                    <p>
                        Sea
                    </p>

                    <h2>Early bird or night owl?</h2>

                    <p>
                        I used to be a night owl but as the years passed I have to say Early bird :)
                    </p>

                    <h2>TV or Youtube?</h2>

                    <p>
                        TV
                    </p>

                    <h2>Facebook or Linkedin?</h2>

                    <p>
                        Linkedin
                    </p>

                    <h2>Beer or wine?</h2>

                    <p>
                        Wine
                    </p>

                    <h2>Roadtrip or cruise?</h2>

                    <p>
                        Roadtrip
                    </p>

                    <h2>City break or island escape?</h2>

                    <p>
                        Island escape
                    </p>

                    <h2>Spa or gym?</h2>

                    <p>
                        Spa
                    </p>

                    <h2>
                        And that's it! Thank you Aliki for such an interesting interview!
                    </h2>
                    <p>
                        It was my pleasure! And thank you for starting this initiative with the newsletter that will help us build
                        strong corporate culture of Axios Holding that will help the company operate effectively and fulfill its big mission!
                    </p>


                </div>

            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>




    </div>


</main>

<footer>
    <div class="axios-bg-white">
        <div class="container py-4 px-md-0">
            <div class="row">
                <div class="col-12 text-center logo-container">
                    <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                </div>
                <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4">
                        <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
                    </ul>
                </div>
                <div class="col-12 social-menu">
                    <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                        <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                        <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                        <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                    </ul>


                </div>
                <div class="col-12 text-center">
                    <h2 class="pt-3 pb-3">THE AXIOS HOLDING NEWSLETTER</h2>
                    <p style="font-size: 12px;">
                        The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                        The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                    </p>
                </div>
                <div class="col-12 copyright-container">
                    <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
    <div class="container">
        <div class="row">
            <div class="col-12 py-4 cookie-policy-content">
                <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
            </div>
        </div>
    </div>
</div>

<div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>
<script>
    $(window).on('load ', function() {

        var newsletter_scroll_ctrl = new ScrollMagic.Controller();

        /***************
         * newsletter Hero Scroll Reveal Animation
         **************/
        var tween_newsletter_hero = new TimelineMax();
        tween_newsletter_hero.add([
            TweenMax.fromTo("#newsletter .hero-container h2", 1.5,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#newsletter .content", 1.3,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#newsletter .hero-bottom-img .bg-img", 1.1,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
        ]);
        var scene_anewsletter_hero = new ScrollMagic.Scene({
            triggerElement: '#newsletter',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_newsletter_hero.setTween(tween_newsletter_hero);
        scene_newsletter_hero.addTo(newsletter_scroll_ctrl);
        scene_newsletter_hero.reverse(true);

        /***************
         * newsletter Section 1 Scroll Reveal Animation
         **************/
        var tween_newsletter_section_1 = new TimelineMax();
        tween_newsletter_section_1.add([
            TweenMax.staggerFromTo("#newsletter-section-1 .team-container .team-member",0.4, {x: "-220px", opacity: '0'}, {ease: Power2.easeOut, x: 0, opacity: '1'}, 0.25),
        ]);
        var scene_tween_newsletter_section_1 = new ScrollMagic.Scene({
            triggerElement: '.team-container',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_tween_newsletter_section_1.setTween(tween_newsletter_section_1);
        scene_tween_newsletter_section_1.addTo(newsletter_scroll_ctrl);
        scene_tween_newsletter_section_1.reverse(true);


    });
</script>
</body>
</html>