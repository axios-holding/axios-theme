<?php
/**
 * Template Name: Test form
 * Created by PhpStorm.
 * User: astavrou
 * Date: 19/08/2019
 * Time: 14:34
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <title>Axios Holding</title>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery-3.3.1.min.js"></script>
    <?php //Access-Control-Allow-Origin header with wildcard.
    header('Access-Control-Allow-Origin: *'); ?>
    <script>

        $(function(){
            $(".ajaxForm").submit(function(e){
                e.preventDefault();
                if($("#firstName").val().length == 0) {
                    $("#name-error").text("Please enter your full name");
                    return false;
                }else {
                    var href = $(this).attr("action");
                    $.ajax({
                        type: "POST",
                        url: href,
                        data: $(this).serialize(),
                        success: function(response){
                            if(response.status == "success"){
                                window.location.href = "https://axiosholding.com/thank-you/";
                            }else{
                                alert("An error occured: " + response.message);
                            }
                        }
                    });
                }
            });
        });
    </script>
</head>
<body>



<main>
<form class="ajaxForm" action="https://formcarry.com/s/2IeiNOEnvLH" method="POST" accept-charset="UTF-8" >

    <input type="email" id="email" name="email"><!-- use this to reply visitors and prevent spam via akismet -->
    <input type="text" id="firstName" name="firstName">
    <p style="color: #E4252F; font-size: 13px;" id="name-error"></p>
    <input type="text" name="lastName">
    <input type="text" name="anotherInput">
    <input type="hidden" name="_gotcha"><!-- use this to prevent spam -->
    <input type="submit" value="Submit">
</main>



</body>
</html>