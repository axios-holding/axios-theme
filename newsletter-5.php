<?php
/**
 * Template Name: Newsletter 5
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
        #search-results #blog-results .articles-container .article-cont {
            max-width: 450px;
            overflow: hidden;
        }
        #search-results #blog-results .articles-container .article .article-cont a .article-img-cont .article-img-cont-in .bg-img {
            width: 450px;
            height: 220px;
        }
    </style>
</head>
<body>

<?php include("_header.php"); ?>


<main id="search-results" class="axios-bg-light blog">

    <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <div class="hero-content-container"></div>
                <div class="hero-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
            </div>
        </div>
    </div>
    <div class="main-content py-5 position-relative">
        <ul class="p-0 text-center mb-4 pb-5">
            <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
            <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
            <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
            <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
            <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
            <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
        </ul>
        <h1 class="axios-text-dark text-center underline underline-light inner-template-heading">INTERESTING FINTECH READS</h1>
        <div id="blog-results" >
            <div class="container articles-container">
                <div class="row pt-3 pt-lg-4">
                    <div class="col-12 col-sm-6 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="https://equfin.com/2019/04/04/investing-in-technology-ai-vs-ia/" class="text-center" target="_blank">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-news-1.png"></div> </div></div>
                                <div class="row mx-auto"><h2>Investing in Technology: AI vs IA</h2></div>
                                <div class="row mx-auto"><p style="text-align: left;">Artificial Intelligence and Intelligence Augmentation have been “battling” each other for a few decades now. We just didn’t know much about it.
                                        And why would we? These terminologies are to this day very foggy and unclear for the average non-techy individual.
                                        It is time to actually differentiate the meaning between the two concepts. Once we understand their differences, it will become much clearer which
                                        is a more viable investing option in the technology sector.</p></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="https://everfx.com/en/blog/news/private-brexit-how-is-the-lingering-uncertainty-impacting-financial-institutions/" class="text-center" target="_blank">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-news-2.png"></div> </div></div>
                                <div class="row mx-auto"><h2>Private: Brexit: How Is The Lingering Uncertainty Impacting Financial Institutions?</h2></div>
                                <div class="row mx-auto"><p style="text-align: left;">
                                        The Brexit fiasco has gone from a surprise referendum result to a political quagmire that has threatened to bring down U.K. Prime minister
                                        Theresa May’s administration and cast a pall over Britain’s future. With a third defeat for Mrs May’s withdrawal proposal, and a planned
                                        fourth vote on the way, there is no clue as to what an actual exit from the European Union (EU) will look like, apart from disorganized.
                                    </p></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="https://equfin.com/2019/03/26/investment-trends-to-be-aware-of-in-2019/" class="text-center" target="_blank">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-news-3.png"></div> </div></div>
                                <div class="row mx-auto"><h2>Investment Trends to be Aware of in 2019</h2></div>
                                <div class="row mx-auto"><p style="text-align: left;">
                                        To accurately predict an investment trend is virtually impossible. Why? Because the global economy is not based on reason or logic.
                                        There are so many shifting variables, so many moving parts, such a high level of volatility, that you can never be sure about anything.
                                        What you can do though, is make an educated guess – evaluate, examine and analyze the current state of affairs and follow patterns to see
                                        where it leads you. What follows is a brief breakdown of the industries, sectors and products that present investing opportunities in the new year.
                                    </p></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="https://www.naspay.com/apple-credit-card-welcome-to-the-future-of-online-payments/" class="text-center" target="_blank">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-news-4.png"></div> </div></div>
                                <div class="row mx-auto"><h2>Apple Credit Card: Welcome to the Future of Online Payments</h2></div>
                                <div class="row mx-auto"><p style="text-align: left;">
                                        Apple has been on the forefront of innovation since introducing the iPhone back in 2007. The journey we’ve been through the past decade with Apple products has seen milestones such as tablets, laptops, headphones and even an Apple TV. In their latest special event, Apple announced the arrival of the Apple Credit Card in collaboration with Goldman Sachs and Mastercard. You know what that means, right? That the credit world will never be the same.
                                    </p></div>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>
<footer>
    <div class="axios-bg-white">
        <div class="container py-4 px-md-0">
            <div class="row">
                <div class="col-12 text-center logo-container">
                    <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                </div>
                <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4">
                        <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
                        <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
                    </ul>
                </div>
                <div class="col-12 social-menu">
                    <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                        <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                        <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                        <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                    </ul>


                </div>
                <div class="col-12 text-center">
                    <h2 class="pt-3 pb-3">THE AXIOS HOLDING NEWSLETTER</h2>
                    <p style="font-size: 12px;">
                        The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                        The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                    </p>
                </div>
                <div class="col-12 copyright-container">
                    <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
    <div class="container">
        <div class="row">
            <div class="col-12 py-4 cookie-policy-content">
                <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
            </div>
        </div>
    </div>
</div>

<div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nice-select.min.js"></script>
<script>

    $(window).on('load ', function() {

        var tweenPosts =new TimelineMax()
        tweenPosts.add([
            TweenMax.staggerFromTo("#blog-results .article",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1', delay:0.8}, 0.15),
        ]);


    });
</script>
</body>
</html>