<?php
/**
 * Template Name: Media Kit
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>

    <title>Axios Holding - Media Kit</title>

    <?php include("_metatags.php"); ?>
    <meta name="description" content="Axios Holding provides this set of resources and guidelines to help provide an understanding of the Axios Holding Brand.">

</head>
<body>

<?php include("_header.php"); ?>

<main id="media-kit">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="media-kit-header" src="<?php echo get_template_directory_uri(); ?>/assets/img/mediakit-header_BG.jpg">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">

                                <div class="hero-content-container">
                                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">Media Kit</h1>
                                    <div class="col-12 back-button">
                                        <a href="<?php echo esc_url(home_url() . '/blog-media/');?>" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                            <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                                <svg class="arrow-icon" width="32" height="32">
                                                    <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                        <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                        <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                                    </g>
                                                </svg>
                                            </span> Back to Blog & Media
                                        </a>
                                    </div>
                                    <div class="content text-center">
                                        <h3 class="text-center">Media & Press Kit</h3>
                                        <p class="col-12 col-md-6 mx-auto text-white">The following is a set of resources and guidelines that will help communicate the Axios Holding Brand.</p>
                                        <div class="pt-5 mx-auto text-center"><a class="btn-axios btn-axios-light" href="<?php echo get_template_directory_uri(); ?>/assets/docs/axios-media-kit.pdf" target="_blank"><i class="fas fa-file-pdf"></i>Download Kit</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-us-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
                </div>
            </div>
        </div>
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>

<script>

    $(window).on('load ', function() {

    });
</script>
</body>
</html>