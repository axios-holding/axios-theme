<?php
/**
 * Template Name: Newsletter 4
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
</head>
<body>

<!--Preloader-->
<div class="preloader position-fixed w-100">
    <div class="loaderContainer">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
</div>

<!--Mobile Device Landscape Mode Message-->
<div class="landscape">
    <div class="landscape__text">Please turn your device</div>
</div>


<main id="blog-media">
    <div class="position-relative blog-media-cont">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                    <h2 class="text-center underline underline-light inner-template-heading text-white pt-5">AXIOS NEWS DIGEST</h2>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-white">


                    </div>
                </div>
            </div>
        </div>
        <div id="blog-media-section" class="py-5">
        <div class="container section-cont py-2 py-md-3">
            <ul class="p-0 text-center mb-4 pb-5">
                <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
                <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
                <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
                <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
                <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
                <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
            </ul>
            <div class="row mx-0 section-cont-inner axios-bg-white">
                <a class="col-12" href="#">
                    <div class="row h-100">
                        <div class="col-12 col-md-6 p-4 text-cont">
                            <div class="pl-2 text-cont-in">
                                <p class="d-block description" style="font-size: 18px; color: #000; font-weight: 600;">EverFX renewed its sponsorship as the Official Global Partner of FC Sevilla for the next season.
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 px-0">
                            <div class="d-none d-md-block position-relative img-cont">
                                <div class="position-absolute bg-img">
                                    <img alt="everfx-news" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfx-digest.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="container section-cont py-2 py-md-3">
            <div class="row mx-0 section-cont-inner axios-bg-white">
                <a class="col-12" href="#">
                    <div class="row h-100">
                        <div class="col-12 col-md-6 p-4 text-cont">
                            <div class="pl-2 text-cont-in">
                                <p class="d-block description" style="font-size: 18px; color: #000; font-weight: 600;">
                                    BigWallet received PI license from Cyprus Central Bank.
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 px-0">
                            <div class="d-none d-md-block position-relative img-cont">
                                <div class="position-absolute bg-img">
                                    <img alt="everfx-news" src="<?php echo get_template_directory_uri(); ?>/assets/img/bigwallet-digest.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

            <div class="container section-cont py-2 py-md-3">
                <div class="row mx-0 section-cont-inner axios-bg-white">
                    <a class="col-12" href="#">
                        <div class="row h-100">
                            <div class="col-12 col-md-6 p-4 text-cont">
                                <div class="pl-2 text-cont-in">
                                    <p class="d-block description" style="font-size: 18px; color: #000; font-weight: 600;">
                                        BigWallet received EMI license from the Central Bank of Lithuania
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0">
                                <div class="d-none d-md-block position-relative img-cont">
                                    <div class="position-absolute bg-img">
                                        <img alt="everfx-news" src="<?php echo get_template_directory_uri(); ?>/assets/img/bigwallet2-digest.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="container section-cont py-2 py-md-3">
                <div class="row mx-0 section-cont-inner axios-bg-white">
                    <a class="col-12" href="#">
                        <div class="row h-100">
                            <div class="col-12 col-md-6 p-4 text-cont">
                                <div class="pl-2 text-cont-in">
                                    <p class="d-block description" style="font-size: 18px; color: #000; font-weight: 600;">
                                        Naspay started partnerships with NeoPay and PayRetailers.
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0">
                                <div class="d-none d-md-block position-relative img-cont">
                                    <div class="position-absolute bg-img">
                                        <img alt="everfx-news" src="<?php echo get_template_directory_uri(); ?>/assets/img/naspay-digest.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="container section-cont py-2 py-md-3">
                <div class="row mx-0 section-cont-inner axios-bg-white">
                    <a class="col-12" href="#">
                        <div class="row h-100">
                            <div class="col-12 col-md-6 p-4 text-cont">
                                <div class="pl-2 text-cont-in">
                                    <p class="d-block description" style="font-size: 18px; color: #000; font-weight: 600;">
                                        Equfin completed its rebranding
                                        with the launch of the new website (www.equfin.com) and LinkedIn profile.
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0">
                                <div class="d-none d-md-block position-relative img-cont">
                                    <div class="position-absolute bg-img">
                                        <img alt="everfx-news" src="<?php echo get_template_directory_uri(); ?>/assets/img/equfin-digest.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="container section-cont py-2 py-md-3">
                <div class="row mx-0 section-cont-inner axios-bg-white">
                    <a class="col-12" href="#">
                        <div class="row h-100">
                            <div class="col-12 col-md-6 p-4 text-cont">
                                <div class="pl-2 text-cont-in">
                                    <p class="d-block description" style="font-size: 18px; color: #000; font-weight: 600;">
                                        Equfin’s brand new website (www.equfin.com) received
                                        4 prestigious design awards CSSDA: Innovation, UI (User Interface), UX (User Experience),
                                        and Special Kudos Award” (a special commendation for noteworthy websites).
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0">
                                <div class="d-none d-md-block position-relative img-cont">
                                    <div class="position-absolute bg-img">
                                        <img alt="everfx-news" src="<?php echo get_template_directory_uri(); ?>/assets/img/equfin2-digest.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="container section-cont py-2 py-md-3">
                <div class="row mx-0 section-cont-inner axios-bg-white">
                    <a class="col-12" href="#">
                        <div class="row h-100">
                            <div class="col-12 col-md-6 p-4 text-cont">
                                <div class="pl-2 text-cont-in">
                                    <p class="d-block description" style="font-size: 18px; color: #000; font-weight: 600;">
                                        Axios completed its rebranding
                                        with the launch of its new website (www.axiosholding.com)
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0">
                                <div class="d-none d-md-block position-relative img-cont">
                                    <div class="position-absolute bg-img">
                                        <img alt="everfx-news" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-digest.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="container section-cont py-2 py-md-3">
                <div class="row mx-0 section-cont-inner axios-bg-white">
                    <a class="col-12" href="#">
                        <div class="row h-100">
                            <div class="col-12 col-md-6 p-4 text-cont">
                                <div class="pl-2 text-cont-in">
                                    <p class="d-block description" style="font-size: 18px; color: #000; font-weight: 600;">
                                        EverFX hosted the business club meeting in Sevilla.
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0">
                                <div class="d-none d-md-block position-relative img-cont">
                                    <div class="position-absolute bg-img">
                                        <img alt="everfx-news" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfx2-digest.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="container section-cont py-2 py-md-3 pb-5 bg-white">
                <div class="row mx-0 section-cont-inner axios-bg-white">
                    <a class="col-12" href="#">
                        <div class="row h-100">
                            <div class="col-12 col-md-6 p-4 text-cont">
                                <div class="pl-2 text-cont-in">
                                    <p class="d-block description" style="font-size: 18px; color: #000; font-weight: 600;">
                                        CCLoan Ukraine successfully completed its revamp of the product site
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0">
                                <div class="d-none d-md-block position-relative img-cont">
                                    <div class="position-absolute bg-img">
                                        <img alt="everfx-news" src="<?php echo get_template_directory_uri(); ?>/assets/img/ccloan-digest.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="container-fluid axios-bg-light px-0 pt-5 bg-white">

                <div class="">
                </div>

            </div>
            <div class="container-fluid axios-bg-light px-0 pt-5 bg-white">
                <footer>
                    <div class="axios-bg-white">
                        <div class="container py-4 px-md-0">
                            <div class="row">
                                <div class="col-12 text-center logo-container">
                                    <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                                </div>
                                <div class="col-12 text-center">
                                    <ul class="p-0 text-center mb-4">
                                        <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
                                        <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
                                        <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
                                        <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
                                        <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
                                        <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
                                    </ul>
                                </div>
                                <div class="col-12 social-menu">
                                    <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                                        <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                                        <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                                        <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                                    </ul>


                                </div>
                                <div class="col-12 text-center">
                                    <h2 class="pt-3 pb-3">THE AXIOS HOLDING NEWSLETTER</h2>
                                    <p style="font-size: 12px;">
                                        The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                                        The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                                    </p>
                                </div>
                                <div class="col-12 copyright-container">
                                    <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>

        </div>



    </div>
</main>
<div class="clearfix"></div>
<div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
    <div class="container">
        <div class="row">
            <div class="col-12 py-4 cookie-policy-content">
                <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
            </div>
        </div>
    </div>
</div>

<div class="custom-cursor"></div>

<?php include("_scripts.php"); ?>
<script>
    $(window).on('load ', function() {

        var newsletter_scroll_ctrl = new ScrollMagic.Controller();

        /***************
         * newsletter Hero Scroll Reveal Animation
         **************/
        var tween_newsletter_hero = new TimelineMax();
        tween_newsletter_hero.add([
            TweenMax.fromTo("#newsletter .hero-container h2", 1.5,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#newsletter .content", 1.3,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#newsletter .hero-bottom-img .bg-img", 1.1,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
        ]);
        var scene_anewsletter_hero = new ScrollMagic.Scene({
            triggerElement: '#newsletter',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_newsletter_hero.setTween(tween_newsletter_hero);
        scene_newsletter_hero.addTo(newsletter_scroll_ctrl);
        scene_newsletter_hero.reverse(true);

        /***************
         * newsletter Section 1 Scroll Reveal Animation
         **************/
        var tween_newsletter_section_1 = new TimelineMax();
        tween_newsletter_section_1.add([
            TweenMax.staggerFromTo("#newsletter-section-1 .team-container .team-member",0.4, {x: "-220px", opacity: '0'}, {ease: Power2.easeOut, x: 0, opacity: '1'}, 0.25),
        ]);
        var scene_tween_newsletter_section_1 = new ScrollMagic.Scene({
            triggerElement: '.team-container',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_tween_newsletter_section_1.setTween(tween_newsletter_section_1);
        scene_tween_newsletter_section_1.addTo(newsletter_scroll_ctrl);
        scene_tween_newsletter_section_1.reverse(true);


    });
</script>
</body>
</html>