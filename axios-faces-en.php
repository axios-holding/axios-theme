<?php
/**
 * Template Name: Axios Faces En
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
            ol, li {
            font-family: "Nunito Sans", sans-serif;
            font-size: 15px;
        }
        .newsletter-interview p {
            color: #000;
        }
        @media screen and (max-width: 600px){
            .news2_stats h1{
                font-size: 16px;
            }
            .news2_stats h2{
                 font-size: 8px;
             }
            .news2_stats h3{
                font-size: 12px;
            }
        }
        /*start carousel */        
    	@media (min-width: 768px) {

    /* show 3 items */
    .carouselPrograms .carousel-inner .active,
    .carouselPrograms .carousel-inner .active + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        display: block;
    }

    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,
	.carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
        transition: none;
    }

    .carouselPrograms .carousel-inner .carousel-item-next,
    .carouselPrograms .carousel-inner .carousel-item-prev {
        position: relative;
        transform: translate3d(0, 0, 0);
    }

    

    /* left or forward direction */
    .carouselPrograms .active.carousel-item-left + .carousel-item-next.carousel-item-left,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* farthest right hidden item must be abso position for animations */
    .carouselPrograms .carousel-inner .carousel-item-prev.carousel-item-right {
        position: absolute;
        top: 0;
        left: 0%;
        z-index: -1;
        display: block;
        visibility: visible;
    }

    /* right or prev direction */
    .carouselPrograms .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
.carousel-item{
        margin: 2% 1.6%;
        -webkit-transition: none;
        transition: none;
}
img.img-fluid.mx-auto.d-block{
    height: 190px;
    width: 350px;
}
.thumb img{
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
}
.thumb img:hover{
    -webkit-filter: unset;
    filter: unset;
}
.panel-thumbnail:hover .thumb img{
    -webkit-filter: unset;
    filter: unset;
}
.carousel-control-prev, .carousel-control-next{
    width: 2%;
}
.number-overlay{
    position: absolute;
    top: 35%;
    left: 38%;
    color: #fff;
    font-size: 100px;
    text-align: center;
}
.places-to-go {
    height: 110px;
}
@media screen and (max-width: 598px){
    iframe{
        width: 100%!important;
    }
}
    </style>
</head>
<body>

    <!--Preloader-->
    <div class="preloader position-fixed w-100">
        <div class="loaderContainer">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>

    <!--Mobile Device Landscape Mode Message-->
    <div class="landscape">
        <div class="landscape__text">Please turn your device</div>
    </div>


    <main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0 newsletter-axios-companies">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                    <li class="d-inline-block px-2"><a href="/axios-universe-en">Axios Universe</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-en">Axios Brands</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-en">Faces of Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-en">Axios News</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-en">Interesting Reads</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-en">Axios Recommends</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-en">Axios Poll</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ENGLISH
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-faces-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-faces-ru">РУССКИЙ</a></li>
                                </ul>
                            </div>
                        </ul>
                        <h1 class="text-center underline underline-light inner-template-heading">AXIOS FACES</h1>
                        <div class="content mx-auto">
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center text-black">
                                We continue introducing you to the faces of Axios, people who daily contribute to the development of our innovative Fintech 
                                hub with their talent, passion and effort.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center pb-5 text-black">
                            Our hero today is David Alimbarashvili, the COO of Equfin Holdings. During an interview with our PR Officer Maria Babchenko, David not only shared his main life lessons but also uncovered what makes him happy and where he would like to go for his dream vacation.
                            </p>
                        </div>
                    </div>
                    <div class="pt-5"></div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="container newsletter-interview">
       
                <div class="row mx=0">
                    <div class="col-12 px-0 pb-5">
                        <div class="">
                            <h2 class="text-center inner-template-heading text-white p-5">MEET DAVID ALIMBARASHVILI, <br> COO EQUFIN HOLDINGS</h2>
                            <div class="text-center pb-5">
                                <img style="width: 220px;" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/david-alimbarashvili.jpg">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row m-md-5 bg-white p-5"  style="margin-top: -160px!important;">
                        <div class="col-md-6 p-5">
                            <p>Maria B.: <strong>Hi, David! Are you ready to take the #axiosfaces challenge and answer a few questions about David the person?</strong></p>
                            <p>David A.: <strong>Hi, Maria! I love challenges.</strong></p>
                            <p>Maria B.: <strong>So I’m gonna ask you 7 questions, and your task is to answer all of them in 15 minutes. Are you ready?</strong></p>
                            <p>David A.: <strong>Let’s do it!</strong></p>
                            <p><strong>What is your hobby? What do you do in your free time?</strong></p>
                            <p>I’m keen on swimming and skiing. I used to be a half-professional swimmer. Also, I like reading. 
                            I used to read a lot but right now, due to extensive travelling, I have time almost only for the professional literature. 
                            But still, I try to find time to read some bestsellers, 
                            for example “Sapiens: A Brief History of Humankind” by Yuval Noah Harari.</p>
                            <p><strong>What is your favorite travel destination?</strong></p>
                            <p>I like diversity, so I can’t say I have a single favorite destination. But I generally love travelling, and it doesn’t matter 
                            if I go to Spain, Kazakhstan or Zambia. When I go somewhere, I want to have some time to see the sights both inside the capital and outside.</p>
                            <p><strong>What is your dream destination?</strong></p>
                            <p>New York City. You would be surprised, but I’ve never been there.</p>
                        </div>
                        <div class="col-md-6 p-5">
                            <p><strong>What is your ideal weekend like?</strong></p>
                            <p>BBQ with my family and friends in the backyard.</p>
                            
                            <p><strong>What’s the phone app you use most frequently?</strong></p>
                            <p>Facebook, to check out posts and photos of my friends, who live all over the world, and read the industry news.</p>

                            <p><strong>What are the three main lessons you’ve learned in your life that you would like to share with the world?</strong></p>
                            <p>
                                <ol>
                                    <li>If you want to be successful, you need to set a goal, develop a plan for its achievement and patiently and meticulously follow it every single day.</li>
                                    <li>If you are facing a challenge, step back and use your information as much as possible. By stepping back, relaxing, distracting yourself from the day-to-day activities, you let your mind come up with a solution of the problem you’ve been looking for. Also, when your mind comes up with a solution of the problem that doesn’t seem to be very rational, it’s better to step back and sleep on it. The next day you’ll see if the solution is rational or not.</li>
                                    <li>We need to take responsibility. We have to pray what we preach. We need to demand from ourselves what we demand from others.</li>
                                </ol>
                            </p>

                            <p><strong>What is your definition of happiness?</strong></p>
                            <p>
                                 Happiness for me is more about a continuous process rather than a final goal. But the key to happiness is knowing exactly what you want.
                            </p>

                            <p><strong>Thank you!</strong></p>
                        </div>

                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>
</main>

    <footer>
        <div class="axios-bg-white">
            <div class="container py-4 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                            <li class="d-inline-block px-2"><a href="/axios-universe-en">Axios Universe</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-en">Axios Brands</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-en">Faces of Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-en">Axios News</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-en">Interesting Reads</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-en">Axios Recommends</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-en">Axios Poll</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ENGLISH
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-faces-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-faces-ru">РУССКИЙ</a></li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <h2 class="pt-3 text-center">THE AXIOS HOLDING <br> NEWSLETTER</h2>
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 cookie-policy-content">
                    <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>

<script>
    $('#carouselExample').on('slide.bs.carousel', function (e) {

    
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
    });





    $(document).ready(function() {
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function(event){
    event.preventDefault();
    var content = $('.modal-body');
    content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    });

    });
</script>
</body>
</html>