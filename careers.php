<?php
/**
 * Template Name: careers
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>

    <title>Axios Holding - Careers</title>

    <?php include("_metatags.php"); ?>
    <meta name="description" content="Looking for starting a new job entails much more than a job title and a salary. At Axios, we value the commitment and we do all in our power to reciprocate.">

    <style>
        .article-text{
            width: 350px!important;
        }
        .BambooHR-ATS-Department-Header {
            background: #fff;
            padding: 20px 20px;
            font-family: "Montserrat", sans-serif;
            font-size: 20px;
            line-height: 20px;
            font-weight: 500;
            color: #000;
        }
        .BambooHR-ATS-board ul ul {
            padding: 2px 20px 12px 20px;
            border-bottom: 1px solid #d7d7d7;
            margin-bottom: 10px;
            background: #fff;
        }
        #BambooHR-Footer{
            display: none;
        }
    </style>

</head>
<body>

<?php include("_header.php"); ?>

<main id="careers">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="careers-header" src="/wp-content/uploads/2020/05/careers-header_BG.jpg">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="hero-content-container">
                                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">Careers</h1>
                                    <h3 class="text-center">Working at Axios</h3>
                                    <div class="content">
                                        <div class="text-center hero-text">
                                            <p class="col-12 col-lg-6 px-0 mx-auto text-center axios-text-light">Joining a company and starting a new job entails so much more than a job description and a salary. At Axios, we value your commitment to our company and we do everything in our power to reciprocate.</p>
                                        </div>
                                    </div>
                                    <div class="map-container">
                                        <div class="text-center map">
                                            <img alt="map-vector-careers" class="img-fluid mx-auto d-md-block" src="/wp-content/uploads/2020/05/Map-Vector_Careers.png">
                                            <div class="pins-container">
                                                <span id="spain" data-select="spain" class="pin"><span class="pin-tooltip">Spain</span></span>
                                                <span id="ukraine" data-select="ukraine" class="pin"><span class="pin-tooltip">Ukraine</span></span>
                                                <span id="georgia" data-select="georgia" class="pin"><span class="pin-tooltip">Georgia</span></span>
                                                <span id="cyprus" data-select="cyprus" class="pin"><span class="pin-tooltip">Cyprus</span></span>
                                                <span id="lithuania" data-select="lithuania" class="pin"><span class="pin-tooltip">Lithuania</span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
                </div>
            </div>
        </div>
        <div class="careers-positions pt-4 axios-bg-light">
            <div class="container">

                <!--  <div class="row"><div class="col-12"><h2 class="axios-text-dark text-center underline underline-light inner-template-heading">careers</h2></div></div>
                  <!-- Desktop Filter -->
                <!--<div class="row">
                  <div class="col-12 col-lg-7 mx-auto">
                      <div class="pb-3 country-filter">
                          <label for="filter-by-country" class="d-none"></label>
                          <select id="filter-by-country" size="5" name="lstStates" multiple="multiple">
                              <option value="spain">Spain</option>
                              <option value="ukraine">Ukraine</option>
                              <option value="georgia">Georgia</option>
                              <option value="cyprus">Cyprus</option>
                          </select>
                      </div>
                  </div>
              </div>
              <div class="row"><div class="col-12 text-center"><p>There are available positions</p></div> </div> -->

                <!--<div class="row mt-6">
                    <div class="col-12 d-block d-sm-flex justify-content-between">
                        <div class="careers__section1__notification d-flex">
                            <input type="checkbox" name="notification" value="yes" id="notification" ><label for="notification">
                                <span>Notify me as soon as a position becomes available.</span>
                            </label>
                        </div>
                        <div class="careers__section1__expand underlineBtn underlineBtn--black text-right"> Expand All <i class="fas fa-plus"></i></div>
                    </div>
                </div>-->
                <!-- Positions Container-->
                <!-- <div id="BambooHR"><script src="https://axiosholding.bamboohr.com/js/jobs2.php" type="text/javascript"></script><div id="BambooHR-Footer">Powered by<a href="http://www.bamboohr.com" target="_blank" rel="noopener external nofollow noreferrer"><img src="https://resources.bamboohr.com/images/footer-logo.png
 https://resources.bamboohr.com/images/footer-logo.png
 " alt="BambooHR - HR software"/></a></div></div> -->
                <div id="BambooHR">
                    <script src="https://axiosholding.bamboohr.com/js/jobs2.php" type="text/javascript"></script><div id="BambooHR-Footer">Powered by<a href="http://www.bamboohr.com" target="_blank" rel="noopener external nofollow noreferrer"><img src="https://resources.bamboohr.com/images/footer-logo.png" alt="BambooHR - HR software"/></a>
                    </div>
                </div>
                <!-- <div class="row mt-2">

                     <div class="col-12 pb-4">
                         <!-- Position -->
                <!--  <div class="careers__section1__position">
                     <span>Frontend Software Developer</span>
                     <div class="careers__section1__position__button">
                         <a href="#" class="animatedButton toggle-btn"><span><i class="fas fa-plus"></i></span></a>
                     </div>

                     <div class="careers__section1__position__description main-content"><div class="careers__section1__position__description__content">
                             <strong>Description:</strong>
                             <p>
                                 We are looking for an experienced Digital marketing strategist to join our team. You will help us use web technologies to achieve our business growth goals. You will identify needs and new opportunities and aspire to increase brand awareness.
                                 If you’re a tech-savvy trendsetter who has innovative ideas to improve customer experience, we would like to meet you. For this position, you should be creative and comfortable working with a team.
                                 Ultimately, you should be able to effectively connect our brand with our online customers.
                             </p>

                             <strong>Responsibilities:</strong>
                             <ul class="unorderedList">
                                 <li>Set digital marketing strategies using all necessary tools (e.g. website, emails, social media and blogs).</li>
                                 <li>Research products, services and current strategies to identify new opportunities.</li>
                                 <li>Analyze web traffic metrics and suggest solutions to boost web presence.</li>
                                 <li>Monitor SEO/SEM, marketing and sales performance metrics to forecast trends.</li>
                                 <li>Keep up-to-date with our audience’s preferences and proactively suggest new campaigns.</li>
                                 <li>Liaise with Marketing, Sales, Design and Content teams to optimize customer experience and ensure brand consistency.</li>
                                 <li>Identify advertising needs.</li>
                             </ul>

                             <a data-position="frontend-software-developer" class="btn-axios btn-axios-dark apply-position-btn" href="#form-container">Apply for this job</a>

                         </div></div>
                 </div>
                 <!-- /Position End -->
                <!-- Position -->
                <!--  <div class="careers__section1__position">
                     <span>Senior Backend Software Developer</span>
                     <div class="careers__section1__position__button"><a href="#" class="animatedButton toggle-btn"><span><i class="fas fa-plus"></i></span></a></div>

                     <div class="careers__section1__position__description main-content"><div class="careers__section1__position__description__content">
                             <strong>Description:</strong>
                             <p>
                                 We are looking for an experienced Digital marketing strategist to join our team. You will help us use web technologies to achieve our business growth goals. You will identify needs and new opportunities and aspire to increase brand awareness.
                                 If you’re a tech-savvy trendsetter who has innovative ideas to improve customer experience, we would like to meet you. For this position, you should be creative and comfortable working with a team.
                                 Ultimately, you should be able to effectively connect our brand with our online customers.
                             </p>

                             <strong>Responsibilities:</strong>
                             <ul class="unorderedList">
                                 <li>Set digital marketing strategies using all necessary tools (e.g. website, emails, social media and blogs).</li>
                                 <li>Research products, services and current strategies to identify new opportunities.</li>
                                 <li>Analyze web traffic metrics and suggest solutions to boost web presence.</li>
                                 <li>Monitor SEO/SEM, marketing and sales performance metrics to forecast trends.</li>
                                 <li>Keep up-to-date with our audience’s preferences and proactively suggest new campaigns.</li>
                                 <li>Liaise with Marketing, Sales, Design and Content teams to optimize customer experience and ensure brand consistency.</li>
                                 <li>Identify advertising needs.</li>
                             </ul>
                             <a data-position="senior-backend-software-developer" class="btn-axios btn-axios-dark apply-position-btn" href="#form-container">Apply for this job</a>

                         </div></div>
                 </div>
                 <!-- /Position End -->
                <!-- Position -->
                <!--    <div class="careers__section1__position">
                       <span>Digital Marketing Strategist</span>
                       <div class="careers__section1__position__button"><a href="#" class="animatedButton toggle-btn"><span><i class="fas fa-plus"></i></span></a></div>

                       <div class="careers__section1__position__description main-content"><div class="careers__section1__position__description__content">
                               <strong>Description:</strong>
                               <p>
                                   We are looking for an experienced Digital marketing strategist to join our team. You will help us use web technologies to achieve our business growth goals. You will identify needs and new opportunities and aspire to increase brand awareness.
                                   If you’re a tech-savvy trendsetter who has innovative ideas to improve customer experience, we would like to meet you. For this position, you should be creative and comfortable working with a team.
                                   Ultimately, you should be able to effectively connect our brand with our online customers.
                               </p>

                               <strong>Responsibilities:</strong>
                               <ul class="unorderedList">
                                   <li>Set digital marketing strategies using all necessary tools (e.g. website, emails, social media and blogs).</li>
                                   <li>Research products, services and current strategies to identify new opportunities.</li>
                                   <li>Analyze web traffic metrics and suggest solutions to boost web presence.</li>
                                   <li>Monitor SEO/SEM, marketing and sales performance metrics to forecast trends.</li>
                                   <li>Keep up-to-date with our audience’s preferences and proactively suggest new campaigns.</li>
                                   <li>Liaise with Marketing, Sales, Design and Content teams to optimize customer experience and ensure brand consistency.</li>
                                   <li>Identify advertising needs.</li>
                               </ul>
                               <a data-position="digital-marketing-strategist" class="btn-axios btn-axios-dark apply-position-btn" href="#form-container">Apply for this job</a>
                           </div>
                       </div>
                   </div>
                   <!-- /Positions Container End -->
                <!--   </div>
              </div>-->
                <!-- /Position End -->
                <!-- Form Container-->
                <!--<div id="form-container" class="row pt-4 form-cont">
                    <div class="col-12"><h2 class="axios-text-dark text-center underline underline-light inner-template-heading">enter your details</h2></div>
                    <div class="col-12 col-lg-6 mx-auto text-center"><p>If you are interested in becoming part of a world class team that places people first, please enter your details and upload your CV below.</p></div>
                    <div class="col-12 px-0 px-sm-4">
                        <div class="form pt-3 pb-5 pt-sm-4 pt-lg-0 px-3 px-sm-4 contact-form axios-bg-white">
                            <form name="contactForm" id="contactForm">
                                <span class="d-block pt-3 form-text text-left form-required"><i class="fas fa-asterisk"></i> Required fields</span>
                                <div class="row mt-4">
                                    <div class="col-12 col-md-6">
                                        <div class="py-2 form-group">
                                            <input type="text" class="form-control" id="first-name" name="first-name">
                                            <label for="first-name">First Name <i class="fas fa-asterisk"></i></label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="py-2 form-group">
                                            <input type="text" class="form-control" id="last-name" name="last-name">
                                            <label for="last-name">Last Name <i class="fas fa-asterisk"></i></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-lg-3 pt-lg-2">
                                    <div class="col-12 col-md-6">
                                        <div class="py-2 form-group">
                                            <input type="email" class="form-control" id="email-address" name="email-address">
                                            <label for="email-address">Email Address <i class="fas fa-asterisk"></i></label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="py-2 mb-0 form-group">
                                            <input type="tel" class="form-control" id="phone-number" name="phone-number">
                                            <label for="phone-number">Phone Number </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-6 py-2">
                                        <div class="form-group">
                                            <label for="country"></label>
                                            <select id="country">
                                                <option disabled selected value>Country</option>
                                                <option value="cyprus">Cyprus</option>
                                                <option value="spain">Spain</option>
                                                <option value="greece">Greece</option>
                                                <option value="uk">United Kingdom</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 py-2">
                                        <div class="form-group">
                                            <label for="city"></label>
                                            <select id="city">
                                                <option disabled selected value>City</option>
                                                <option value="nicosia">Nicosia</option>
                                                <option value="paphos">Paphos</option>
                                                <option value="larnaka">Larnaka</option>
                                                <option value="limassol">Limassol</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-lg-4 pt-lg-2">
                                    <div class="col-12 py-2">
                                        <div class="form-group">
                                            <label for="position"></label>
                                            <select id="position">
                                                <option disabled selected value>Select Available Position *</option>
                                                <option value="0">Select available position</option>
                                                <option value="frontend-software-developer">Frontend Software Developer</option>
                                                <option value="senior-backend-software-developer">Senior Backend Software Developer</option>
                                                <option value="digital-marketing-strategist">Digital Marketing Strategist</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-lg-3 pt-lg-2">
                                    <div class="col-12 col-md-6 py-2">
                                        <span class="pb-3 radio-text">Do you have any relatives working in AXIOS Holding?</span>
                                        <label class="pl-3 radio-inline">
                                            <input type="radio" name="relatives-employed" value="yes">Yes
                                        </label>
                                        <label class="pl-5 radio-inline">
                                            <input type="radio" name="relatives-employed" value="no" checked="checked">No
                                        </label>
                                    </div>
                                    <div class="col-12 col-md-6 py-2">
                                        <span class="pb-3 radio-text">Are you currently or have previously, been employed by the AXIOS Holding?</span>
                                        <label class="pl-3 radio-inline">
                                            <input type="radio" name="previously-employed" value="yes">Yes
                                        </label>
                                        <label class="pl-5 radio-inline">
                                            <input type="radio" name="previously-employed" value="no" checked="checked">No
                                        </label>
                                    </div>
                                </div>
                                <div class="row mt-2 mt-md-9">
                                    <div class="col-12 py-2">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="uploadCV" aria-describedby="uploadCV">
                                                <label class="custom-file-label" for="uploadCV">Choose file *</label>
                                            </div>
                                        </div>
                                        <div class="formNote mt-3">Supported file types: pdf, txt, .doc, .docx</div>
                                    </div>
                                </div>
                                <div class="row mt-4 mt-md-9">
                                    <div class="col-12 py-2">
                                        <div class="input-group consent" >
                                            <input type="checkbox" name="consent" value="yes" id="consent"><label for="consent">
                                                <span>I accept the website <a href="privacy">Privacy Policy</a>.</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 pt-0 pb-4 recaptcha">
                                        <div class="g-recaptcha" data-sitekey="6Ld-C5AUAAAAAJ0bhNUf8Htk2OA5eKjIzIWlyyd_"></div>
                                    </div>
                                </div>
                                <div class="row mt-2"><div class="col-sm-12 text-center text-sm-left"><a class="btn-axios btn-axios-dark" href="#">submit application</a></div> </div>
                            </form>
                        </div>
                    </div>
                </div>-->
                <!-- /Form Container End-->
                <!-- Discount Scheme Container-->
                <div class="row discount-container text-center">
                    <div class="col-12 pb-4"><h2 class="axios-text-dark text-center underline underline-light inner-template-heading pt-5">axios discount card scheme</h2></div>
                    <div class="col-12 mb-4"><div class="bg-img"><img src="/wp-content/uploads/2020/05/benefits.jpg" alt="axios-card-scheme"></div></div>
                    <div class="container row mx-auto"><div class="col-12 col-lg-5 mx-auto"><h3>Axios Discount Card</h3></div></div>
                    <div class="col-12 col-lg-8 mx-auto"><p>The Axios Discount Card is just one of the many ways we try to give back and empower our staff. Discounts to gyms, wellness centers, restaurants and bars give our team the chance to enjoy their time outside the office. The list of available options get updated regularly. </p></div>
                    <div class="col-12 text-center"><a class="btn-axios btn-axios-dark" href="<?php echo get_template_directory_uri(); ?>/assets/docs/axios-discount-card.pdf" target="_blank">See More</a></div>
                </div>
                <!-- /Discount Scheme Container End-->
            </div>
        </div>
        <div class="container-fluid position-relative hr-message bg-img">
            <img src="/wp-content/uploads/2020/05/aboutus_HR-message_bg-image.jpg" alt="hr-message">
            <div class="row axios-bg-dark">
                <div class="separator-top position-absolute fixed-top angled-separator invert separator-bg-none"></div>
                <div class="col-12 col-sm-10 col-lg-6 mx-auto text-center message-container">
                    <h2 class="axios-text-light-white underline underline-light">hr message</h2>
                    <p class="axios-text-light text-white">We are committed to provide our employees a stable work environment with equal opportunity for learning and personal growth. It is our mission to support the total operation in meeting our goals through our most valuable recourse, <strong>our people.</strong>
                        We rely on a culture of leadership, diversity and inclusiveness. We aim to employ the world’s brightest minds to help us create a limitless source of ideas and opportunities. We believe in hiring talented people of varied backgrounds, experiences and style, <strong>people like you!</strong> </p>
                    <span class="quote-author">Aliki Karmiotou, Head of Group HR</span>
                </div>
                <div class="separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
            </div>
        </div>
        <div id="more-posts" class="py-2 pb-sm-5 axios-bg-light">
            <div class="container articles-container">
                <div class="row"><div class="m-auto text-center"><h2 class="axios-text-light-dark underline">Our Blog</h2></div> </div>
                <div class="row pt-3 pb-4 pb-lg-5">
                    <?php
                    // the query
                    $the_query = new WP_Query( array(
                        'posts_per_page' => 3,
                        'category__not_in' => 2 ,
                    ));
                    ?>

                    <?php if ( $the_query->have_posts() ) : ?>
                        <?php $count = 0; ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <?php if ($count == 0) {?>
                                <div class="col-12 col-sm-12 col-md-4">
                                    <div class="mx-auto article-cont">
                                        <a href="<?php the_permalink(); ?>" class="text-center">
                                            <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div> </div></div>
                                            <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                                            <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                                        </a>
                                    </div>
                                </div>

                            <?php  } ?>
                            <?php if ($count == 1) {?>
                                <div class="d-none d-md-block col-12 col-sm-6 col-md-4">
                                    <div class="mx-auto article-cont">
                                        <a href="<?php the_permalink(); ?>" class="text-center">
                                            <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div> </div></div>
                                            <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                                            <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($count == 2) {?>
                                <div class="d-none d-md-block col-12 col-sm-6 col-md-4">
                                    <div class="mx-auto article-cont">
                                        <a href="<?php the_permalink(); ?>" class="text-center">
                                            <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div> </div></div>
                                            <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                                            <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                                        </a>
                                    </div>
                                </div>
                            <?php  } ?>

                            <?php $count++ ; endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                    <?php else : ?>
                        <p><?php __('No Postss'); ?></p>
                    <?php endif; ?>
                    <!--<div class="col-12 col-sm-6 col-md-4">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_1.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                            </a>
                        </div>
                    </div>-->
                    <!--<div class="d-none d-sm-block col-12 col-sm-6 col-md-4">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_2.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="text">Cras et augue varius dolor molestie interdum nec quis libero</span></div>
                            </a>
                        </div>
                    </div>-->
                    <!--<div class="d-none d-md-block col-12 col-sm-6 col-md-4">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_3.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="text">Sed sed magna et elit malesuada lobortis.</span></div>
                            </a>
                        </div>
                    </div>-->
                    <div class="pt-5 mx-auto text-center all-articles-cont"><a class="btn-axios btn-axios-dark" href="blog">all news</a></div>
                </div>
            </div>
        </div>

        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nice-select.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/bootstrap-multiselect.min.js"></script>
<script>
    $(document).ready(function(){
        $( "#filter-by-country option" ).change(function() {


            alert("it changed");



        });


    });
</script>
<script>
    function custom_cursor() {
        var cursor = document.querySelector(".custom-cursor");
        var pin = document.querySelectorAll(".pin");
        var expand_btn = $('.careers__section1__expand')[0];
        var input = document.querySelectorAll("input");

        for (var i = 0; i < pin.length; i++) {
            var selfSelect = pin[i];
            selfSelect.addEventListener("mouseover", function() {
                cursor.classList.add("custom-cursor--link");
            });
            selfSelect.addEventListener("mouseout", function() {
                cursor.classList.remove("custom-cursor--link");
            });
        }

        for (var j = 0; j < input.length; j++) {
            var inputSelect = input[j];
            inputSelect.addEventListener("mouseover", function() {
                cursor.classList.add("custom-cursor--link");
            });
            inputSelect.addEventListener("mouseout", function() {
                cursor.classList.remove("custom-cursor--link");
            });
        }

        expand_btn.addEventListener("mouseover", function() {
            cursor.classList.add("custom-cursor--link");
        });
        expand_btn.addEventListener("mouseout", function() {
            cursor.classList.remove("custom-cursor--link");
        });
    }

    $(document).ready(function() {
        custom_cursor();
    });
    $(window).on('load ', function() {

        //Scroll to form
        $('a[href^="#"].apply-position-btn').on('click', function(e) {
            var target = $(this.getAttribute('href'));
            if( target.length ) {
                e.preventDefault();
                $('html, body').stop().animate({
                    scrollTop: target.offset().top -100
                }, 1000);
            }
        });

        // Filters
        $('select').niceSelect();
        $('#filter-by-country').multiselect({
            nonSelectedText: 'Select Countries with job availability'
        });

        /* show file value after file select */
        $('.custom-file-input').on('change',function() {
            var fileName = document.getElementById("uploadCV").files[0].name;
            $(".custom-file-label").html(fileName);
        });

        //Open a specific career
        $('.careers__section1__position__button').click(function(e) {
            e.preventDefault();
            $(this).parent().find('.careers__section1__position__description').slideToggle(500);
            $(this).parent().toggleClass("active");
        });

        //Expand/Collapse all careers
        $('.careers__section1__expand').click(function(e) {
            e.preventDefault();

            if($(this).hasClass("expanded")){
                $(this).removeClass("expanded");
                $(this).html('Expand All <i class="fas fa-plus"></i>');
                $('.careers__section1__position__description').slideUp(500);
                $('.careers__section1__position').removeClass("active");
            }else{
                $(this).html('Collapse All <i class="fas fa-minus"></i>');
                $(this).addClass("expanded");
                $('.careers__section1__position__description').slideDown(500);
                $('.careers__section1__position').addClass("active");
            }

        });

        // Apply for this job nutton
        $('.apply-position-btn').on('click', function (e) {
            e.preventDefault();
            var selected = $(this).data('position');
            $('#position').val(selected);
            $('select').niceSelect('update');
        })
    });
</script>
</body>
</html>