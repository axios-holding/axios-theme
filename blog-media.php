<?php
/**
 * Template Name: Blog Media
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>

    <title>Axios Holding - Blog Media</title>

    <?php include("_metatags.php"); ?>
    <meta name="description" content="We tackle everything from industry news as well as deep dives into some of  FinTech’s most burning issues. Touching on verticals that range from PSPs to Online Payments.">

</head>
<body>

<?php include("_header.php"); ?>

<main id="blog-media" class="axios-bg-light">
    <div class="position-relative blog-media-cont">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0 position-relative hero-inner">
                    <div class="bg-img hero-bg">
                        <img alt="blog-and-media-header" src="/wp-content/uploads/2020/05/blogandmedia-header_BG.jpg">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="hero-content-container">
                                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">Blog & Media</h1>
                                    <div class="content mx-auto">
                                        <h3 class="text-center">Our Blog & Media section will give you everything from the Axios brand guidelines to blogs and hot news on the newest most interesting FinTech topics.</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
                </div>
            </div>
        </div>
        <div id="blog-media-section" class="py-5">
            <div class="container section-cont py-2 py-md-3">
                <div class="row mx-0 section-cont-inner axios-bg-white">
                    <a class="col-12" href="<?php echo esc_url(home_url() . '/blog/');?>">
                        <div class="row h-100">
                            <div class="col-12 col-md-6 p-4 text-cont">
                                <div class="pl-2 text-cont-in">
                                    <span class="d-block pb-2 title">Our Blog</span>
                                    <p class="d-block description">Our blog will tackle everything from industry news to deep dives into some of FinTech’s most burning issues. Expect topics that touch on verticals that range from PSPs and Online Payments, to Forex and Online Lending. </p>
                                    <span class="d-block text-uppercase arrow-icon-cont">Read more
                                    <svg class="arrow-icon" width="32" height="32">
                                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                            <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                            <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                        </g>
                                    </svg>
                                </span>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0">
                                <div class="d-none d-md-block position-relative img-cont">
                                    <div class="position-absolute bg-img">
                                        <img alt="blog-media-blog-img" src="/wp-content/uploads/2020/05/blogandmedia_blog_img.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="container section-cont py-2 py-md-3">
                <div class="row mx-0 section-cont-inner axios-bg-white">
                    <a class="col-12" href="<?php echo esc_url(home_url() . '/media-kit/'); ?>">
                        <div class="row h-100">
                            <div class="col-12 col-md-6 p-4 text-cont">
                                <div class="pl-2 text-cont-in">
                                    <span class="d-block pb-2 title">Media Kit</span>
                                    <p class="d-block description">For quick access to our Media Kit, click here. </p>
                                    <span class="d-block text-uppercase arrow-icon-cont">Read more
                                    <svg class="arrow-icon" width="32" height="32">
                                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                            <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                            <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                        </g>
                                    </svg>
                                </span>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0">
                                <div class="d-none d-md-block position-relative img-cont">
                                    <div class="position-absolute bg-img">
                                        <img alt="blog-and-media-img" src="/wp-content/uploads/2020/05/blogandmedia_media_img.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nice-select.min.js"></script>
<script>
    $(document).ready(function() {


    });
    $(window).on('load ', function() {

        var tweenPosts =new TimelineMax();
        tweenPosts.add([
            TweenMax.staggerFromTo("#blog-media-section .section-cont",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1', delay:1}, 0.15),
        ]);


    });
</script>
</body>
</html>
