<?php
/**
 * 
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $current_tag_name = single_tag_title( '', false ); ?>
    <?php include("_styles.php"); ?>

    <title>Axios Holding - Blog Tag: <?php echo $current_tag_name;?></title>

    <?php include("_metatags.php"); ?>
    <?php $tag_name_to_Lower = strtolower($current_tag_name); ?>
    <link rel="canonical" href="<?php echo get_site_url() . '/tag/' . $tag_name_to_Lower . '/' ?>" />
    <meta name="description" content="<?php echo "Read the latest fintech news, from interviews, reviews and financial analysis for the category tag " . $current_tag_name . ". Always up to date with our info of what is happening in " . $current_tag_name . "." ?>">
    <style>
        .img-fluid{
            height: 200px!important;
        }
    </style>
</head>
<body>

<?php include("_header.php"); ?>

<main id="search-results" class="axios-bg-light blog">

    <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <div class="hero-content-container"></div>
                <div class="hero-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
            </div>
        </div>
    </div>
    <div class="main-content py-5 position-relative">
        <h1 class="axios-text-dark text-center underline underline-light inner-template-heading"><?php echo $current_tag_name ?></h1>
        <div class="container row mx-auto d-block back-button-cont">
            <div class="col-12 px-0 back-button">
                <a href="<?php echo esc_url(home_url() . '/blog-media/');?>" class="mx-auto d-flex justify-content-center justify-content-md-start mx-md-0">
                        <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                            <svg class="arrow-icon" width="32" height="32">
                                <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                    <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                    <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                </g>
                            </svg>
                        </span>
                    Blog & Media
                </a>
            </div>
        </div>
        <div id="blog-results" >
            <div class="container articles-container">
                <div class="row pt-3 pt-lg-4">
                    <!--<div class="col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_1.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_2.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Cras et augue varius dolor molestie interdum nec quis libero</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_3.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Sed sed magna et elit malesuada lobortis.</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_1.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_2.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Cras et augue varius dolor molestie interdum nec quis libero</span></div>
                            </a>
                        </div>
                    </div>-->
                    <?php
                    //get the current page
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    //pagination fixes prior to loop
                    $temp =  $query;
                    $query = null;
                    //custom loop using WP_Query
                    $query = new WP_Query( array(
                        'post_status' => 'publish',
                        'orderby' => 'date',
                        'order' => 'ASC',
                        'tag' => 'tech'

                    ) );
                    //set our query's pagination to $paged
                    $query -> query('post_type=post&posts_per_page=6&category__not_in=2&tag='. $current_tag_name .'&paged='.$paged);
                    $max_pages = $query->max_num_pages;
                    if ( $query->have_posts() ) :
                    while ( $query->have_posts() ) : $query->the_post();
                        ?>
                        <div class="col-12 col-sm-6 col-md-4 pb-5 article">
                            <div class="mx-auto article-cont">
                                <a href="<?php the_permalink(); ?>" class="text-center">
                                    <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div> </div></div>
                                    <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                                    <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text"> <?php the_title(); ?></span></div>
                                </a>
                            </div>
                        </div>
                    <?php endwhile;?>
                </div>
                <div class="row mt-6">
                    <div class="col-12 text-center">
                        <nav>
                            <ul class="pagination">
                                <?php pagination_bar($max_pages); ?>
                            </ul>
                        </nav>
                    </div>
                </div>
                    <?php else : ?>
                        <p><?php __('No Posts'); ?></p>
                    <?php endif; ?>
                <?php //reset the following that was set above prior to loop
                $query = null; $query = $temp; ?>
                    <!-- Last 3 are hidden on mobile
                    <div class="d-none d-sm-block col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_1.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="d-none d-sm-block col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_2.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Cras et augue varius dolor molestie interdum nec quis libero</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="d-none d-sm-block col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_3.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Sed sed magna et elit malesuada lobortis.</span></div>
                            </a>
                        </div>
                    </div> -->
            </div>
        </div>
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>

<script>

    $(window).on('load ', function() {

        var tweenPosts =new TimelineMax()
        tweenPosts.add([
            TweenMax.staggerFromTo("#blog-results .article",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1', delay:0.8}, 0.15),
        ]);


    });
</script>
</body>
</html>