<?php
/**
 * Template Name: Search
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>

    <title>Axios Holdings - Search Results</title>

    <?php include("_metatags.php"); ?>
</head>
<body>

<?php include("_header.php"); ?>

<main id="search-results" class="axios-bg-light">

    <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <div class="hero-content-container"></div>
                <div class="hero-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
            </div>
        </div>
    </div>
    <div class="main-content py-5 position-relative">
        <h2 class="axios-text-dark text-center underline underline-light inner-template-heading">search results</h2>
        <div id="blog-results" >
            <div class="container article-results-filter">
                <div class="row px-2 px-sm-0">
                    <div class="col-12 col-md-4 order-md-1 text-left d-flex justify-content-left filter-cont filter">
                        <!-- Mobile Filter -->
                        <div class="container row mx-auto px-0 pb-3 d-md-none mobile-filter">
                            <span class="d-block w-100 py-2 text-center">Filter by:</span>
                            <!-- Active state of filter -->
                            <a class="col-12 text-center filter-link active reset" href="#">All</a>
                            <a class="col-6 text-center filter-link" href="#">Blog Articles</a>
                            <a class="col-6 text-center filter-link" href="#">Press Release</a>
                        </div>
                        <!-- Desktop Filter -->
                        <div class="container row d-none d-md-flex justify-content-md-start  desktop-filter">
                            <label for="filter-by">Filter by:</label>
                            <select id="filter-by">
                                <option value="all">All</option>
                                <option value="press-releases">Press Releases</option>
                                <option value="blog-articles">Blog Articles</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-5 order-md-3 text-right d-flex justify-content-md-end filter-cont sort">
                        <!-- Mobile Filter -->
                        <div class="container row mx-auto px-0 pb-3 d-md-none mobile-filter">
                            <span class="d-block w-100 py-2 text-center">Sort by:</span>
                            <a class="col-12 text-center filter-link active reset" href="#">All</a>
                            <a class="col-6 text-center filter-link" href="#">Most relevant</a>
                            <a class="col-6 text-center filter-link" href="#">Last Week</a>
                            <a class="col-6 text-center filter-link" href="#">Last Month</a>
                            <a class="col-6 text-center filter-link" href="#">Last Year</a>
                        </div>
                        <!-- Desktop Filter -->
                        <div class="container row d-none d-md-flex justify-content-md-end  desktop-filter">
                            <label for="blog-sort-by">Filter by:</label>
                            <select id="blog-sort-by">
                                <option value="all">All</option>
                                <option value="most-relevant">Most relevant</option>
                                <option value="last-week">Last Week</option>
                                <option value="last-month">Last Month</option>
                                <option value="last-year">Last Year</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3 order-md-2 py-3 pt-md-0 text-center filter-cont index">
                        <span class="d-block d-sm-none">Showing <strong>1 - 3</strong> of <strong>32</strong></span>
                        <span class="d-none d-sm-block">Showing <strong>1 - 6</strong> of <strong>32</strong></span>
                    </div>
                </div>
            </div>
            <div class="container articles-container">
                <div class="row pt-3 pt-lg-0">
                    <div class="col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="article-thumbnail" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_1.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="article-thumbnail" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_2.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Cras et augue varius dolor molestie interdum nec quis libero</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="article-thumbnail" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_3.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Sed sed magna et elit malesuada lobortis.</span></div>
                            </a>
                        </div>
                    </div>
                    <!-- Last 3 are hidden on mobile -->
                    <div class="d-none d-sm-block col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="article-thumbnail" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_1.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="d-none d-sm-block col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="article-thumbnail" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_2.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Cras et augue varius dolor molestie interdum nec quis libero</span></div>
                            </a>
                        </div>
                    </div>
                    <div class="d-none d-sm-block col-12 col-sm-6 col-md-4 pb-5 article">
                        <div class="mx-auto article-cont">
                            <a href="" class="text-center">
                                <div class="row mx-auto article-img-cont"><div class="position-relative d-flex m-auto article-img-cont-in"><div class="mx-auto bg-img"><img alt="article-thumbnail" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/article_img_3.jpg"></div> </div></div>
                                <div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date">9 May 2019</span></div>
                                <div class="row mx-auto article-text"><span class="col-9 col-12 mx-auto text">Sed sed magna et elit malesuada lobortis.</span></div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row mt-6">
                    <div class="col-12 text-center">
                        <nav>
                            <ul class="pagination">
                                <li><a class="page-link" href="#"><i class="fas fa-arrow-left"></i> <span>PREV</span></a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item active"><a class="page-link" href="#">3</a></li>
                                <li class="page-item dots">...</li>
                                <li class="page-item"><a class="page-link" href="#">23</a></li>
                                <li><a class="page-link" href="#"><span>NEXT</span><i class="fas fa-arrow-right"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <h3 class="axios-text-dark text-center underline underline-light inner-template-heading">Other pages</h3>
        <div id="page-results">
            <div class="container article-results-filter">
                <div class="row px-2 pb-3 pb-lg-0 px-sm-0">
                    <div class="col-12 col-md-4 order-md-1 text-left d-flex justify-content-left filter-cont filter"></div>
                    <div class="col-12 col-md-4 order-md-3 pl-md-0 text-right d-flex justify-content-md-end filter-cont sort">
                        <!-- Mobile Filter -->
                        <div class="container row mx-auto px-0 pb-3 d-md-none mobile-filter">
                            <span class="d-block w-100 py-2 text-center">Sort by:</span>
                            <a class="col-12 text-center filter-link active reset" href="#">All</a>
                            <a class="col-6 text-center filter-link" href="#">Most relevant</a>
                            <a class="col-6 text-center filter-link" href="#">Last Week</a>
                            <a class="col-6 text-center filter-link" href="#">Last Month</a>
                            <a class="col-6 text-center filter-link" href="#">Last Year</a>
                        </div>
                        <!-- Desktop Filter -->
                        <div class="container row d-none d-md-flex pl-md-0 justify-content-md-end  desktop-filter">
                            <label for="page-sort-by">Filter by:</label>
                            <select id="page-sort-by">
                                <option value="all">All</option>
                                <option value="most-relevant">Most relevant</option>
                                <option value="last-week">Last Week</option>
                                <option value="last-month">Last Month</option>
                                <option value="last-year">Last Year</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 order-md-2 py-3 pt-md-0 text-center filter-cont index">
                        <span class="d-block d-sm-none">Showing <strong>1 - 3</strong> of <strong>32</strong></span>
                        <span class="d-none d-sm-block">Showing <strong>1 - 6</strong> of <strong>32</strong></span>
                    </div>
                </div>
            </div>
            <div class="container articles-container">
                <div class="row pb-4">
                    <div class="col-12 col-sm-6 col-md-4 pb-3">
                        <div class="mx-auto px-2 px-sm-0 text-center page-cont">
                            <a href="" class="text-left px-4 bg-white">
                                <div class="row mx-auto page-title"><span class="pt-3 pb-2 title">Reports</span></div>
                                <div class="row mx-auto page-text"><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                                <span class="d-block text-center text-lg-left text-uppercase px-0 page-link">view more
                                    <span class="d-block pl-3 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 pb-3">
                        <div class="mx-auto px-2 px-sm-0 text-center page-cont">
                            <a href="" class="text-left px-4 bg-white">
                                <div class="row mx-auto page-title"><span class="pt-3 pb-2 title">Reports</span></div>
                                <div class="row mx-auto page-text"><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                                <span class="d-block text-center text-lg-left text-uppercase px-0 page-link">view more
                                    <span class="d-block pl-3 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 pb-3">
                        <div class="mx-auto px-2 px-sm-0 text-center page-cont">
                            <a href="" class="text-left px-4 bg-white">
                                <div class="row mx-auto page-title"><span class="pt-3 pb-2 title">Reports</span></div>
                                <div class="row mx-auto page-text"><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                                <span class="d-block text-center text-lg-left text-uppercase px-0 page-link">view more
                                    <span class="d-block pl-3 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <!-- Last 3 are hidden on mobile -->
                    <div class="d-none d-sm-block col-12 col-sm-6 col-md-4 pb-3">
                        <div class="mx-auto px-2 px-sm-0 text-center page-cont">
                            <a href="" class="text-left px-4 bg-white">
                                <div class="row mx-auto page-title"><span class="pt-3 pb-2 title">Reports</span></div>
                                <div class="row mx-auto page-text"><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                                <span class="d-block text-center text-lg-left text-uppercase px-0 page-link">view more
                                    <span class="d-block pl-3 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="d-none d-sm-block col-12 col-sm-6 col-md-4 pb-3">
                        <div class="mx-auto px-2 px-sm-0 text-center page-cont">
                            <a href="" class="text-left px-4 bg-white">
                                <div class="row mx-auto page-title"><span class="pt-3 pb-2 title">Reports</span></div>
                                <div class="row mx-auto page-text"><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                                <span class="d-block text-center text-lg-left text-uppercase px-0 page-link">view more
                                    <span class="d-block pl-3 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="d-none d-sm-block col-12 col-sm-6 col-md-4 pb-3">
                        <div class="mx-auto px-2 px-sm-0 text-center page-cont">
                            <a href="" class="text-left px-4 bg-white">
                                <div class="row mx-auto page-title"><span class="pt-3 pb-2 title">Reports</span></div>
                                <div class="row mx-auto page-text"><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span></div>
                                <span class="d-block text-center text-lg-left text-uppercase px-0 page-link">view more
                                    <span class="d-block pl-3 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row mt-6">
                    <div class="col-12 text-center">
                        <nav>
                            <ul class="pagination">
                                <li><a class="page-link" href="#"><i class="fas fa-arrow-left"></i> <span>PREV</span></a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item active"><a class="page-link" href="#">3</a></li>
                                <li class="page-item dots">...</li>
                                <li class="page-item"><a class="page-link" href="#">23</a></li>
                                <li><a class="page-link" href="#"><span>NEXT</span><i class="fas fa-arrow-right"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nice-select.min.js"></script>
<script>
    $(window).on('load resize orientationchange', function() {
        if ($(window).width() >= 768) {
            $('select').niceSelect();
        }
        else {
            $('select').niceSelect('destroy');
        }
    });

    $(document).ready(function() {

        var cursor = document.querySelector(".custom-cursor");
        var select = document.querySelectorAll(".nice-select");
        var select_dropdown = document.querySelectorAll(".option");

        for (var i = 0; i < select.length; i++) {
            var selfSelect = select[i];
            selfSelect.addEventListener("mouseover", function() {
                cursor.classList.add("custom-cursor--link");
            });
            selfSelect.addEventListener("mouseout", function() {
                cursor.classList.remove("custom-cursor--link");
            });
        }

        for (var j = 0; j < select_dropdown.length; j++) {
            var selfSelect_dropdown = select_dropdown[j];
            selfSelect_dropdown.addEventListener("mouseover", function() {
                cursor.classList.add("custom-cursor--link");
            });
            selfSelect_dropdown.addEventListener("mouseout", function() {
                cursor.classList.remove("custom-cursor--link");
            });
        }


    });
    $(window).on('load ', function() {

        var about_us_scroll_ctrl = new ScrollMagic.Controller();

        var tweenPosts =new TimelineMax()
        tweenPosts.add([
            TweenMax.staggerFromTo("#blog-results .article",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1', delay:0.8}, 0.15),
        ]);

        /***************
         * Home Section 4 Scroll Reveal Animation
         **************/
        var tween_about_us_section_4 = new TimelineMax();
        tween_about_us_section_4.add([
            TweenMax.staggerFromTo("#page-results .page-cont",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1'}, 0.15),
        ]);
        var scene_about_us = new ScrollMagic.Scene({
            triggerElement: '#page-results',
            triggerHook: 'onEnter',
            offset: 0,
        });
        scene_about_us.setTween(tween_about_us_section_4);
        scene_about_us.addTo(about_us_scroll_ctrl);
        scene_about_us.reverse(true);

    });
</script>
</body>
</html>