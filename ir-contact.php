<?php
/**
 * Template Name: IR contact
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        form .form-text.consent a:hover {
            color: #222;
        }
    </style>
    <title>Axios Holding< - Investors Relations/title>

    <?php include("_metatags.php"); ?>
    <meta name="description" content="Our Investor Relations section holds information about Axios Holding's for stakeholders, potential investors, and financial analysts.">

</head>
<body class="withBreadcrumb">

<?php include("_header.php"); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery-3.3.1.min.js"></script>
<script>

    $(function(){
        $(".ajaxForm").submit(function(e){
            e.preventDefault();
            if($("#full-name").val().length == 0) {
                $("#name-error").text("Please enter your full name");
                return false;
            }else if ($("#email-address").val().length == 0){
                $("#name-error").text("");
                $("#email-error").text("Please enter your email address");
                return false;
            }else if ($("#message").val().length == 0){
                $("#name-error").text("");
                $("#email-error").text("");
                $("#message-error").text("Please enter a message");
                return false;
            }else {
                var href = $(this).attr("action");
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: href,
                    data: $(this).serialize(),
                    success: function(response){
                        if(response.status == "success"){
                            window.location.href = "https://axiosholding.com/thank-you/";
                        }else{
                            alert("An error occured: " + response.message);
                        }
                    }
                });
            }
        });
    });
</script>

<main id="ir-contact" class="ir-contact position-relative">

    <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <div class="bg-img hero-bg">
                    <img alt="ir-contact" src="<?php echo get_template_directory_uri(); ?>/assets/img/IRcontact-header_BG.jpg">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-content-container">
                                <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">Investor relations contact</h1>
                                <div class="content">
                                    <div class="text-center hero-text">
                                       <!-- <h3 class="col-12 col-lg-6 px-0 mx-auto text-center axios-text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
            </div>
        </div>
    </div>

    <div class="content axios-bg-light content-container">
        <div class="container">
            <div class="row d-block">
                <div class="col-12 back-button">
                    <a href="<?php echo esc_url(home_url() . '/investors-overview/');?>" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                        <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                            <svg class="arrow-icon" width="32" height="32">
                                <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                    <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                    <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                </g>
                            </svg>
                        </span> Back to investors Overview</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-7">
                    <div class="row py-3 text-center team-member">
                        <a class="col-12 col-sm-5 px-0 m-auto m-sm-0 member-photo-link" href=""><div class="bg-img member-photo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/team/maria.jpg" class="img-fluid" alt="maria-img"></div></a>
                        <div class="col-12 col-sm-6 text-center text-sm-left pl-4 position-sm-relative member-info">
                            <span class="d-block pt-3 pt-sm-0 pb-3 name">Maria Babchenko</span>
                            <span class="d-block title">PR & Investors Relations</span>
                            <div class="email-phone pt-4 pt-md-0">
                                <span class="d-block pb-3 email">Email<a class="d-block" href="mailto:pr@axiosholding.com">pr@axiosholding.com</a></span>
                                <span class="d-block phone">Phone<a class="d-block" href="tel:+35725250515">+357 25 517 577</a></span>
                            </div>
                        </div>
                    </div>
                    <!--<div class="row py-3 text-center team-member">
                        <a class="col-12 col-sm-5 px-0 m-auto m-sm-0 member-photo-link" href=""><div class="bg-img member-photo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/team_member_photo_1.jpg" class="img-fluid" alt=""></div></a>
                        <div class="col-12 col-sm-6 text-center text-sm-left pl-4 position-sm-relative member-info">
                            <span class="d-block pt-3 pt-sm-0 pb-3 name">John C. Doe</span>
                            <span class="d-block title">Head of Reporting & Investor Relations</span>
                            <div class="email-phone pt-4 pt-md-0">
                                <span class="d-block pb-3 email">Email<a class="d-block" href="mailto:johndoe@axiosHolding.com">johndoe@axiosHolding.com</a></span>
                                <span class="d-block phone">Phone<a class="d-block" href="tel:+35725250515">+357 25 250515</a></span>
                            </div>
                        </div>
                    </div>-->
                    <!--<div class="row py-3 text-center team-member">
                        <a class="col-12 col-sm-5 px-0 m-auto m-sm-0 member-photo-link" href=""><div class="bg-img member-photo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/team_member_photo_1.jpg" class="img-fluid" alt=""></div></a>
                        <div class="col-12 col-sm-6 text-center text-sm-left pl-4 position-sm-relative member-info">
                            <span class="d-block pt-3 pt-sm-0 pb-3 name">John C. Doe</span>
                            <span class="d-block title">Head of Reporting & Investor Relations</span>
                            <div class="email-phone pt-4 pt-md-0">
                                <span class="d-block pb-3 email">Email<a class="d-block" href="mailto:johndoe@axiosHolding.com">johndoe@axiosHolding.com</a></span>
                                <span class="d-block phone">Phone<a class="d-block" href="tel:+35725250515">+357 25 250515</a></span>
                            </div>
                        </div>
                    </div>-->
                </div>
                <div class="col-12 col-lg-5">
                    <div class="form pt-5 pt-lg-0 ir-contact-form">
                        <form name="contactForm" class="ajaxForm prForm" id="contactForm" action="https://formcarry.com/s/pbaHMCW-Hcr" method="POST" accept-charset="UTF-8" >
                            <span class="d-block mt-0 pt-2 pb-4 text-center text-sm-left form-text form-title">Find out more about investment opportunities at AXIOS Group.</span>
                            <span class="d-block form-text form-required"><i class="fas fa-asterisk"></i> Required fields</span>
                            <div class="row mt-2">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="full-name" name="full-name">
                                        <label for="full-name">Full Name <i class="fas fa-asterisk"></i></label>
                                        <p style="color: #E4252F; font-size: 13px;" id="name-error"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email-address" name="email-address">
                                        <label for="email-address">Email Address <i class="fas fa-asterisk"></i></label>
                                        <p style="color: #E4252F; font-size: 13px;" id="email-error"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="tel" class="form-control" id="phone-number" name="phone-number">
                                        <label for="phone-number">Phone Number </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="company-organisation" name="company-organisation">
                                        <label for="company-organisation">Company or Organization </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" id="message" name="message"></textarea>
                                        <label for="message">Your message <i class="fas fa-asterisk"></i></label>
                                    </div>
                                </div>
                            </div>
                            <span class="form-text text-left pb-4 consent">By signing up, you consent to our <a href="<?php echo esc_url(home_url() . '/privacy/');?>">Privacy Policy</a></span>
                            <div class="row">
                                <div class="col-sm-12 pt-0 pb-4 recaptcha">
                                    <div class="g-recaptcha" data-sitekey="6Ld0oqUUAAAAAAna7aFwD8gLvQJslMFv2ZCWogiR"></div>
                                </div>
                            </div>
                            <div class="row mt-2"><div class="col-sm-12 text-center text-sm-left"><input class="btn-axios btn-axios-dark" type="submit" value="submit"></div> </div>
                            <input type="hidden" name="_gotcha"><!-- use this to prevent spam -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<!--Google Recaptcha-->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>
    $(document).ready(function(e) {

    });
    $(window).on('load ', function() {

        var tweenPosts =new TimelineMax();
        tweenPosts.add([
            TweenMax.staggerFromTo("#ir-contact .team-member",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1', delay:1.3}, 0.15),
            TweenMax.fromTo("#ir-contact form", 0.4,{x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1', delay:1.5}),
        ]);


    });
</script>
</body>
</html>