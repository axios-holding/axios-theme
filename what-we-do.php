<?php
/**
 * Template Name: what we do
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        .mx-auto.bg-img {
            background-size: contain!important;
        }
    </style>
    <title>Axios Holding - What We Do</title>
    <?php include("_metatags.php"); ?>
</head>
<body>

<?php include("_header.php"); ?>

<main id="what-we-do">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="about-header" src="<?php echo get_template_directory_uri(); ?>/assets/img/aboutus-header_BG.jpg">
                    </div>
                    <div class="container-fluid pb-5">
                        <div class="row">
                            <div class="col-12 px-0 pb-5">
                                <div class="hero-content-container px-3">
                                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">what we do</h1>
                                    <div class="content">
                                        <h3 class="col-10 col-lg-12 mx-auto text-center">Powered by technology. Designed by people</h3>
                                        <div class="text-center hero-text">
                                            <p class="col-12 col-lg-5 px-0 mx-auto text-center axios-text-light">Axios Holding is a FinTech incubator, fathering companies from a range of online verticals such as Education, Lending, Payments, Brokerage & Liquidity, User Engagement and DevOps. Axios offers a comprehensive set of FinTech products and solutions by either breeding its own ideas or partnering with other companies in related verticals. </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Particle Canvas Container -->
                                <div id="particle-canvas" class="text-center position-relative bg-img canvas-container">
                                    <img alt="particles-mobile" src="<?php echo get_template_directory_uri(); ?>/assets/img/whatwedo_particles_mobile.png">
                                    <div class="verticals-list">
                                        <img id="particlelogo"
                                             class="next-particle"
                                             alt="logo"
                                             data-particle-gap="5"
                                             data-mouse-force="-10"
                                             src="<?php echo get_template_directory_uri(); ?>/assets/img/Hero_particle_2.png">
                                        <div id="hotspot-container" class="pb-5 btn-container">
                                            <div class="hotspot-btn" >
                                                <a href="/" class="py-4 py-md-0 button left" id="online-gambling-btn" data-slide="online-gambling">User Engagement Platforms</a>
                                                <a href="/" class="py-4 py-md-0 button" id="education-platforms-btn" data-slide="education-platforms">Education Platforms</a>
                                                <a href="/" class="py-4 py-md-0 button" id="brokerage-and-liquidity-btn" data-slide="brokerage-and-liquidity">Brokerage & Liquidity</a>
                                                <a href="/" class="py-4 py-md-0 button" id="technology-providers-btn" data-slide="technology-providers">Technology Providers</a>
                                                <!--<a href="/" class="py-4 py-md-0 button left" id="physical-payments-btn" data-slide="physical-payments">Payment Aggregator</a>-->
                                                <a href="/" class="py-4 py-md-0 button left" id="trading-platforms-btn" data-slide="trading-platforms">Investment Platforms</a>
                                                <a href="/" class="py-4 py-md-0 button" id="online-payments-btn" data-slide="online-payments">Online Payments</a>
                                                <a href="/" class="py-4 py-md-0 button" id="online-lending-btn" data-slide="online-lending">Online Lending</a>
                                                <a href="/" class="py-4 py-md-0 button" id="exclusive-partners-btn" data-slide="exclusive-partners">Exclusive Partners</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Particle Canvas Container End -->
                            </div>
                        </div>
                    </div>
                    <div class="about-us-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
                </div>
            </div>
        </div>
        <!-- Verticals -->
        <div id="online-gambling" class="container-fluid mx-auto vertical-container">
            <div class="container-fluid">
                <!-- Back Button Container -->
                <div class="row mx-auto pb-3  back-button-cont">
                    <div class="col-12 back-button">
                        <div class="container px-0 mx-auto row">
                            <div class="col-12 px-0">
                                <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    Back to what we do
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Back Button Container End-->
                <!-- Vertical Container -->
                <div class="row mx-auto vertical-cont-inner">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!-- Verticals Title-->
                                <div class="row title">
                                    <div class="col-12">
                                        <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">User Engagement Platforms</h2>
                                    </div>
                                </div>
                                <!-- Verticals Companies-->
                                <div class="row companies">

                                    <div class="text-center hero-text">
                                        <p class=" mx-auto text-center axios-text-light">The industry has been on the up the past decade. The industry recorded a net worth of $41.78 billion in 2016 and that shows no signs of slowing down with projections looking at an $80 billion mark for 2020. The huge attention and popularity of this sector comes with its own set of challenges. Due to elevated competition, customers require and expect service of the highest calibre. Our tech is geared towards satisfying these needs, always improving and adapting to the evolution of the industry, aligned to legal and regulatory protocols.</p>
                                    </div>

                                    <div class="col-12 col-md-6 pb-5 company-cont mx-auto">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="cerberus-logo" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/cerberus.jpg"></div></div>
                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">Cerberus</span>
                                            <p class="text-left">Founded in 2017 by a team of visionary Tech veterans, Cerberus is on a mission to give the best online user engagement solutions; through a data-driven and customer-centric approach. The company’s ‘Horizon’ Platform offers a complete, bleeding edge ecosystem containing everything needed to operate an engaging user environment: recreational gaming, payments gateway, CRM, middleware integration and a state of the art Big Data Environment. By developing and providing this reliable, flexible and secure platform, Cerberus intends to change the way users interact with digital recreation technologies. </p>
                                            <a class="btn-axios btn-axios-light" href="http://cerberus.io/" target="_blank">visit website</a>
                                        </div>
                                    </div>
                                    <!--<div class="col-12 col-md-6 pb-5 company-cont">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/cerberus_company_img.jpg"></div></div>

                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">CC Loan</span>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ornare suscipit ligula sit amet interdum. In efficitur leo purus, et maximus tortor feugiat non. Nulla ornare suscipit ligula sit amet interdum. </p>
                                            <a class="btn-axios btn-axios-light" href="#">visit website</a>
                                        </div>
                                    </div>-->
                                </div>
                                <!-- Verticals Companies End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vertical Container End-->
            </div>
        </div>
        <div id="education-platforms" class="container-fluid mx-auto vertical-container">
            <div class="container-fluid">
                <!-- Back Button Container -->
                <div class="row mx-auto pb-3 back-button-cont">
                    <div class="col-12 back-button">
                        <div class="container px-0 mx-auto row">
                            <div class="col-12 px-0">
                                <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    Back to what we do
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Back Button Container End-->
                <!-- Vertical Container -->
                <div class="row mx-auto vertical-cont-inner">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!-- Verticals Title-->
                                <div class="row title">
                                    <div class="col-12">
                                        <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">Education Platforms</h2>
                                    </div>
                                </div>
                                <!-- Verticals Companies-->
                                <div class="row companies">

                                    <div class="text-center hero-text">
                                        <p class=" mx-auto text-center axios-text-light">Products and services are just a part of the FinTech puzzle and they don’t mean much without the complementary education. In order to attract and retain customers to our brands, we understand that providing them with the necessary knowledge about the products, services and industry is of the utmost importance. Education is a simple yet effective way of giving back, empowering people to make informed decisions but also contribute to the overall betterment of the industry through their feedback and experiences.</p>
                                    </div>

                                    <div class="col-12 col-md-6 pb-5 company-cont mx-auto">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="edu-2-co" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/edu-2-co.jpg"></div></div>
                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">Edu2Co</span>
                                            <p class="text-left">Edu2Co, a birthchild of the industry leading brand, “Traders Education”, is poised to disrupt the market. The platform aims at offering a customized educational solution for brokerages all over the world. The company's signature “Knowledge Base” platform contains educational tools, ebooks and 15 online courses with 300 lessons in 30+ topics taught by financial professionals and translated into over 30 languages. For the past 6 years, the company has been providing unique technology-driven educational solutions catered to brokers that want to use customer training as a powerful vehicle to increase return on their marketing activities and stand out from the competition. </p>
                                            <a class="btn-axios btn-axios-light" href="http://www.traders-group.com/" target="_blank">visit website</a>
                                        </div>
                                    </div>
                                    <!--<div class="col-12 col-md-6 pb-5 company-cont">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/cerberus_company_img.jpg"></div></div>

                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">CC Loan</span>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ornare suscipit ligula sit amet interdum. In efficitur leo purus, et maximus tortor feugiat non. Nulla ornare suscipit ligula sit amet interdum. </p>
                                            <a class="btn-axios btn-axios-light" href="#">visit website</a>
                                        </div>
                                    </div>-->
                                </div>
                                <!-- Verticals Companies End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vertical Container End-->
            </div>
        </div>
        <div id="brokerage-and-liquidity" class="container-fluid mx-auto vertical-container">
            <div class="container-fluid">
                <!-- Back Button Container -->
                <div class="row mx-auto pb-3 back-button-cont">
                    <div class="col-12 back-button">
                        <div class="container px-0 mx-auto row">
                            <div class="col-12 px-0">
                                <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    Back to what we do
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Back Button Container End-->
                <!-- Vertical Container -->
                <div class="row mx-auto vertical-cont-inner">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!-- Verticals Title-->
                                <div class="row title">
                                    <div class="col-12">
                                        <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">Brokerage & Liquidity</h2>
                                    </div>
                                </div>
                                <!-- Verticals Companies-->
                                <div class="row companies">

                                    <div class="text-center hero-text">
                                        <p class=" mx-auto text-center axios-text-light">The Brokerage & Liquidity vertical is one of the most dynamic sectors in the FinTech world. It is  highly regulated and its success traces its roots in qualities like transparency, clear communication and establishing solid relationships with your client-base. The engine powering growth in this vertical lies in the right technical infrastructure, deep understanding and knowledge of the space as well as amassed experience. Our brands are staffed with people that are passionate about the industry and relentlessly work to offer a simpler, supportive and more accessible experience to the end-user.</p>
                                    </div>

                                    <!-- div class="col-12 col-md-6 pb-5 company-cont">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="everfx-logo" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/everfx.jpg"></div></div>
                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">EverFX</span>
                                            <p class="text-left">An <strong>exclusive partner</strong> for Axios Holding. EverFX is a CySec and CIMA regulated broker of CFDs on forex, metals and commodities that was established in Cyprus in 2016 with the vision of bringing broker and trader closer to one another. The company is determined to lead the way in creating an ideal environment for traders with a user-friendly platform, excellent trading conditions and comprehensive education. Its transparency and impeccable reputation (zero complaints from institutions, banks and customers since November 2016) together with its unprecedented client-oriented service have made EverFX the “Best New Multi-Asset Broker” in Europe according to Global Brand Awards. With over 100 employees of more than 15 nationalities operating in 9 languages from 2 offices in Cyprus and Ukraine, EverFX is rapidly growing and expanding to new international markets. Since 2018, EverFX is an Official Global Partner of Sevilla FC. </p>
                                            <a class="btn-axios btn-axios-light" href="https://everfx.com/" target="_blank">visit website</a>
                                        </div>
                                    </div -->
                                    <div class="col-12 col-md-6 pb-5 mx-auto company-cont">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="inflyx-logo" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/inflyx.jpg"></div></div>

                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">Inflyx</span>
                                            <p class="text-left">An <strong>exclusive partner</strong> for Axios Holding. Inflyx is a liquidity provider for small brokers, hedge funds, and professional traders who want to have access to tier one liquidity. The company was founded in 2018 as part of EverFX to offer a transparent and flexible solution that can satisfy the most demanding liquidity seekers from Europe and Asia and is fully tailored to their needs.  </p>
                                            <a class="btn-axios btn-axios-light" href="https://inflyx.com/" target="_blank">visit website</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Verticals Companies End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vertical Container End-->
            </div>
        </div>
        <div id="technology-providers" class="container-fluid mx-auto vertical-container">
            <div class="container-fluid">
                <!-- Back Button Container -->
                <div class="row mx-auto pb-3 back-button-cont">
                    <div class="col-12 back-button">
                        <div class="container px-0 mx-auto row">
                            <div class="col-12 px-0">
                                <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    Back to what we do
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Back Button Container End-->
                <!-- Vertical Container -->
                <div class="row mx-auto vertical-cont-inner">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!-- Verticals Title-->
                                <div class="row title">
                                    <div class="col-12">
                                        <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">Technology Providers</h2>
                                    </div>
                                </div>
                                <!-- Verticals Companies-->
                                <div class="row companies">

                                    <div class="text-center hero-text">
                                        <p class=" mx-auto text-center axios-text-light">The future of the DevOps vertical is quite clear: software should be intuitive to use on any device in spite of location and geography. Understanding that helps us build software that’s using microservices and automations to ensure system stability. Working with cloud hosting providers, we provide easily-scalable platforms which can grow at the same speed as your business. The challenge here is to not recycle and repackage products and services that already exist but try to add value to business through innovation, without any delays or downtimes.</p>
                                    </div>

                                    <div class="col-12 col-md-6 pb-5 company-cont mx-auto">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="new-age-logo" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/new-age.jpg"></div></div>
                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">New Age Solutions</span>
                                            <p class="text-left">New Age Solutions is a technology hub for the companies under the umbrella of Axios Holding. It helps incubate and build startups of the holding by providing them with proprietary tech solutions. In particular, the company develops software for Forex platforms, Forex CRM, and online user engagement platforms. Being a part of such a diverse fintech corporation as Axios, New Age Solutions has a broader view compared to other technology providers, which allows it to find creative solutions to the technical problems and develop products of the highest quality. Founded in 2016, New Age Solutions currently employs over 80 people across 3 offices in Ukraine. The company’s vision is to be the tech incubator of Axios Holding and support it throughout its rapid growth.  </p>
                                            <a class="btn-axios btn-axios-light" href="https://newage.io/" target="_blank">visit website</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Verticals Companies End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vertical Container End-->
            </div>
        </div>
        <div id="physical-payments" class="container-fluid mx-auto vertical-container">
            <div class="container-fluid">
                <!-- Back Button Container -->
                <div class="row mx-auto pb-3 back-button-cont">
                    <div class="col-12 back-button">
                        <div class="container px-0 mx-auto row">
                            <div class="col-12 px-0">
                                <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    Back to what we do
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Back Button Container End-->
                <!-- Vertical Container -->
                <div class="row mx-auto vertical-cont-inner">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!-- Verticals Title-->
                                <div class="row title">
                                    <div class="col-12">
                                        <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">Payment Aggregator</h2>
                                    </div>
                                </div>
                                <!-- Verticals Companies-->
                                <div class="row companies">

                                    <div class="text-center hero-text">
                                        <p class=" mx-auto text-center axios-text-light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div>
                                    <div class="col-12 col-md-6 pb-5 company-cont mx-auto">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="naspay-logo" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/naspay.jpg"></div></div>

                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">Naspay</span>
                                            <p class="text-left">Naspay is an <strong>exclusive service provider</strong> for Axios Holding. The company offers e-cashier and payment processing services to merchants and other businesses that collect or want to collect money online. Naspay was founded in 2016 by a team of industry experts with a vision to give merchants full control of their financial flows through an intuitive interface. The company’s services match the most advanced security standards (PCI DSS Level 1 and GDPR). It is notable that in 3 years Naspay had only 4 seconds of downtime; making it an industry leader in reliability. This all makes the company a go-to solution for merchants who wish to process payments from all over the globe. Currently Naspay successfully caters for European and Middle East clients.</p>
                                            <a class="btn-axios btn-axios-light" href="https://naspay.com/" target="_blank">visit website</a>
                                        </div>
                                    </div>

                                </div>
                                <!-- Verticals Companies End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vertical Container End-->
            </div>
        </div>
        <div id="trading-platforms" class="container-fluid mx-auto vertical-container">
            <div class="container-fluid">
                <!-- Back Button Container -->
                <div class="row mx-auto pb-3 back-button-cont">
                    <div class="col-12 back-button">
                        <div class="container px-0 mx-auto row">
                            <div class="col-12 px-0">
                                <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    Back to what we do
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Back Button Container End-->
                <!-- Vertical Container -->
                <div class="row mx-auto vertical-cont-inner">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!-- Verticals Title-->
                                <div class="row title">
                                    <div class="col-12">
                                        <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">Investment Platforms</h2>
                                    </div>
                                </div>
                                <!-- Verticals Companies-->
                                <div class="row companies">

                                    <div class="text-center hero-text">
                                        <p class=" mx-auto text-center axios-text-light">Investment platforms are no longer an off the shelf product. The dynamic environment and countless variables of the investment world, call for technology that’s adjustable and malleable enough to serve different use cases. The challenge of creating and building our own technology is both inspiring and rewarding. Our goal is to put together platforms that not only serve the end user, but push the boundaries of what was thought to be possible.</p>
                                    </div>

                                    <div class="col-12 col-md-6 pb-5 company-cont mx-auto">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="global-innovation-fund" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/global-innovation-fund.jpg"></div></div>
                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">Global Innovations Fund</span>
                                            <p class="text-left">The Global Innovations Fund, established in 2019, provides investors with exposure to the fast-growing verticals in Europe. The purpose of the GI Fund is to provide investors with access to unique opportunities in various sectors of the economy. Its primary focus is in projects relating to Agri & Environment, Energy & Transport, Technology and Real Estate. The GI Fund is an innovative investment vehicle which seeks to pair project owners with international investors; through a rigorous project selection process coupled with a personal approach. The GI Fund team brings together decades of experience at high level investment management with a strong track record in high returns and successful exits.</p>
                                            <a class="btn-axios btn-axios-light" href="https://gifund.eu" target="_blank">visit website</a>
                                        </div>
                                    </div>
                                    <!--<div class="col-12 col-md-6 pb-5 company-cont">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/cerberus_company_img.jpg"></div></div>

                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">CC Loan</span>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ornare suscipit ligula sit amet interdum. In efficitur leo purus, et maximus tortor feugiat non. Nulla ornare suscipit ligula sit amet interdum. </p>
                                            <a class="btn-axios btn-axios-light" href="#">visit website</a>
                                        </div>
                                    </div>-->
                                </div>
                                <!-- Verticals Companies End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vertical Container End-->
            </div>
        </div>
        <div id="online-payments" class="container-fluid mx-auto vertical-container">
            <div class="container-fluid">
                <!-- Back Button Container -->
                <div class="row mx-auto pb-3 back-button-cont">
                    <div class="col-12 back-button">
                        <div class="container px-0 mx-auto row">
                            <div class="col-12 px-0">
                                <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    Back to what we do
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Back Button Container End-->
                <!-- Vertical Container -->
                <div class="row mx-auto vertical-cont-inner">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!-- Verticals Title-->
                                <div class="row title">
                                    <div class="col-12">
                                        <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">Online Payments</h2>
                                    </div>
                                </div>
                                <!-- Verticals Companies-->
                                <div class="row companies">

                                    <div class="text-center hero-text">
                                        <p class=" mx-auto text-center axios-text-light">Online payments have overshadowed physical payments for quite some time now and the reasons why that’s happening are simple: it’s easier, faster and more convenient. Online payments have quickly evolved from a luxury feature to a necessity, an integral part of any business that wants to be taken seriously. The obvious challenge of this vertical was and will always be the security of funds and protection of sensitive information. Our state-of-the-art technology and custom-built platforms are paving the way for a new era of online payments, catering to the needs of modern businesses whilst never failing to provide the highest form of security.</p>
                                    </div>

                                    <div class="col-12 col-md-6 pb-5 company-cont mx-auto">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="big-wallet-logo" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/big-wallet.jpg"></div></div>
                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">BigWallet</span>
                                            <p class="text-left">BigWallet, a brand of Silvergate Technologies Limited, is a full-fledged Online Payment Solution Provider. It holds a European Payment Institution license from the Central Bank of Cyprus. BigWallet’s vision is to disrupt the traditional banking sector and give people and companies freedom to receive, use and transfer money whenever and wherever they want, at a click of a button. </p>
                                            <a class="btn-axios btn-axios-light" href="https://bigwpay.com" target="_blank">visit website</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Verticals Companies End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vertical Container End-->
            </div>
        </div>
        <div id="online-lending" class="vertical-container">
            <div class="container-fluid">
                <!-- Back Button Container -->
                <div class="row mx-auto pb-3 back-button-cont">
                    <div class="col-12 back-button">
                        <div class="container px-0 mx-auto row">
                            <div class="col-12 px-0">
                                <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    Back to what we do
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Back Button Container End-->
                <!-- Vertical Container -->
                <div class="row mx-auto vertical-cont-inner">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!-- Verticals Title-->
                                <div class="row title">
                                    <div class="col-12">
                                        <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">Online Lending</h2>
                                    </div>
                                </div>
                                <!-- Verticals Companies-->
                                <div class="row companies">

                                    <div class="text-center hero-text">
                                        <p class=" mx-auto text-center axios-text-light">Online lending is a relatively new vertical as brick and mortar banking has dominated the space in the past. The growth of the Internet and development of the peer-to-peer lending framework allowed licensed online lending to flourish and push the boundaries of personal finance options. Whilst new, the industry has come a long way since its inception allowing people to access money with a few clicks. Our involvement with this vertical has a very clear mission: provide fast, easy access to money to everyone without any hassle or bureaucracy. </p>
                                    </div>

                                    <div class="col-12 col-md-6 pb-5 company-cont mx-auto">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="equfin-logo" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/equfin.jpg"></div></div>
                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">Equfin</span>
                                            <p class="text-left">Equfin Holding was established in 2012 with the vision of giving anyone with an Internet connection access to finance through the latest technologies and most advanced channels. The company has been growing fast and steadily ever since, with presence and significant market share in Spain, Georgia and Ukraine, and Armenia. Equfin’s product range includes short-term consumer loans to people with financial deficit. Its customer-driven service and an advanced in-house online lending platform make it a prominent name in the online lending space. The company currently employs over 250 people across its five operational locations. </p>
                                            <a class="btn-axios btn-axios-light" href="https://equfin.com/" target="_blank">visit website</a>
                                        </div>
                                    </div>
                                    <!--<div class="col-12 col-md-6 pb-5 company-cont">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/cerberus_company_img.jpg"></div></div>

                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">CC Loan</span>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ornare suscipit ligula sit amet interdum. In efficitur leo purus, et maximus tortor feugiat non. Nulla ornare suscipit ligula sit amet interdum. </p>
                                            <a class="btn-axios btn-axios-light" href="#">visit website</a>
                                        </div>
                                    </div>-->
                                </div>
                                <!-- Verticals Companies End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vertical Container End-->
            </div>
        </div>
        <!-- Exclusive Partners-->
        <div id="exclusive-partners" class="vertical-container">
            <div class="container-fluid">
                <!-- Back Button Container -->
                <div class="row mx-auto pb-3 back-button-cont">
                    <div class="col-12 back-button">
                        <div class="container px-0 mx-auto row">
                            <div class="col-12 px-0">
                                <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                                        <svg class="arrow-icon" width="32" height="32">
                                            <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                                <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                                <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    Back to what we do
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Back Button Container End-->
                <!-- Vertical Container -->
                <div class="row mx-auto vertical-cont-inner">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!-- Verticals Title-->
                                <div class="row title">
                                    <div class="col-12">
                                        <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">Exclusive Partners</h2>
                                    </div>
                                </div>
                                <!-- Verticals Companies-->
                                <div class="row companies">

                                    <div class="text-center hero-text">
                                        <p class=" mx-auto text-center axios-text-light"></p>
                                    </div>

                                    <!--div class="col-12 col-md-6 pb-5 company-cont mx-auto">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="naspay-logo" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/naspay.jpg"></div></div>

                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">Naspay</span>
                                            <p class="text-left">Naspay is an <strong>exclusive service provider</strong> for Axios Holding. The company offers e-cashier and payment processing services to merchants and other businesses that collect or want to collect money online. Naspay was founded in 2016 by a team of industry experts with a vision to give merchants full control of their financial flows through an intuitive interface. The company’s services match the most advanced security standards (PCI DSS Level 1 and GDPR). It is notable that in 3 years Naspay had only 4 seconds of downtime; making it an industry leader in reliability. This all makes the company a go-to solution for merchants who wish to process payments from all over the globe. Currently Naspay successfully caters for European and Middle East clients.</p>
                                            <a class="btn-axios btn-axios-light" href="https://naspay.com/" target="_blank">visit website</a>
                                        </div>
                                    </div -->
                                    <div class="col-12 col-md-12 pb-5 company-cont">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="fincue-logo" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/verticals/fincue.jpg"></div></div>

                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">Fincue</span>
                                            <p class="text-left">Fincue’s cloud based loan management CRM platform is a true leader in the niche. The technology supports the whole business cycle, with full automation on the majority of loan application and disbursement processes and robust integration with third party service providers. The company offers both a sound technological backbone for online lending providers and granular insights in data analysis. A best of breed and speed solution. </p>
                                            <a class="btn-axios btn-axios-light" href="http://www.fincue.com/" target="_blank">visit website</a>
                                        </div>
                                    </div>
                                    <!--<div class="col-12 col-md-6 pb-5 company-cont">
                                        <div class="position-relative d-flex m-auto company-img"><div class="mx-auto bg-img"><img alt="" class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/cerberus_company_img.jpg"></div></div>

                                        <div class="text-center text-cont">
                                            <span class="d-block vertical-title">CC Loan</span>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ornare suscipit ligula sit amet interdum. In efficitur leo purus, et maximus tortor feugiat non. Nulla ornare suscipit ligula sit amet interdum. </p>
                                            <a class="btn-axios btn-axios-light" href="#">visit website</a>
                                        </div>
                                    </div>-->
                                </div>
                                <!-- Verticals Companies End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vertical Container End-->
            </div>
        </div>
        <!-- Verticals End-->
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/nextparticle.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/attract.min.js"></script>
<script>

    $(window).on('load', function() {

        function is_touch_device() {
            return (('ontouchstart' in window)
                || (navigator.MaxTouchPoints > 0)
                || (navigator.msMaxTouchPoints > 0));
        }

        if (!is_touch_device()) {
            $(".vertical-cont-inner").niceScroll({
                cursorwidth: 5,
                cursorborder: 0,
                cursorcolor: '#0A0B0B',
                autohidemode: true,
                zindex: 999999999,
                horizrailenabled: false,
                cursorborderradius: 0,
            });
        }


        $('#hotspot-container a').on('click', function(e){
            e.preventDefault();
            var vertical_id = $(this).data('slide');
            $('#what-we-do-slides').addClass('active');
            $('#'+vertical_id).addClass('active');
            var tweenPosts =new TimelineMax();
            tweenPosts.add([
                TweenMax.staggerFromTo(".vertical-container.active .company-cont",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1', delay:0.8}, 0.15),
            ]);

        });
        $('.back-button').on('click', function(e){
            e.preventDefault();
            $('#what-we-do-slides').removeClass('active');
            $('.vertical-container').each(function () {
                $(this).removeClass('active');
            })
        });

    });



</script>
</body>
</html>