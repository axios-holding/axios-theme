<?php
/**
 * Template Name: About
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>

    <title>Axios Holding - About Us</title>

    <?php include("_metatags.php"); ?>
    <style>

        #about-us-section-1{
            background: url(/wp-content/uploads/2020/05/aboutus-header_BG.jpg);
        }
        .axios_team{

            height: 500px;
            position: absolute;
            top: -30%;
            left: 10%;

            }
        .arm {

            position: absolute;
            left: 50%;
            top: 50%;
            width:255px;
            height:1px;
           background: rgba(255,255,255,0.4);

        }
        .arm2 {

            position: absolute;
            left: 50%;
            top: 50%;
            width:300px;
            height:1px;
        //background: rgba(255,255,255,0.7);

        }

        .box {
            position:relative;
            width: 130px;
            height: 130px;
            left:150px;
            top: -50px;
            font-size: 22px;
            color: #000;
            line-height: 100px;
            text-align: center;
            background: #fff;
            border-radius: 50%;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }
        .box:hover{
            filter: none;
        }
        .box2 {
            position:relative;
            width: 130px;
            height: 130px;
            left:250px;
            top: -50px;
            font-size: 22px;
            color: #000;
            line-height: 100px;
            text-align: center;
            background: #fff;
            border-radius: 50%;
            background: url(https://axiosholding.com/wp-content/themes/axios/assets/img/team/rati.jpg);
            background-size: cover;
            z-index: 2;
        }
        .box3 {
            position:relative;
            width: 130px;
            height: 130px;
            left:190px;
            top: -50px;
            font-size: 22px;
            color: #000;
            line-height: 100px;
            text-align: center;
            background: #fff;
            border-radius: 50%;
            background: url(https://axiosholding.com/wp-content/themes/axios/assets/img/team/valerian.jpg);
            background-size: cover;
        }
        .box4 {
            position:relative;
            width: 130px;
            height: 130px;
            left:200px;
            top: -50px;
            font-size: 22px;
            color: #000;
            line-height: 100px;
            text-align: center;
            background: #fff;
            border-radius: 50%;
            background: url(https://axiosholding.com/wp-content/themes/axios/assets/img/team/gia.jpg);
            background-size: cover;
        }
        .box5 {
            position:relative;
            width: 130px;
            height: 130px;
            left:200px;
            top: -50px;
            font-size: 22px;
            color: #000;
            line-height: 100px;
            text-align: center;
            background: #fff;
            border-radius: 50%;
            background: url(https://axiosholding.com/wp-content/themes/axios/assets/img/team/alexandre.jpg);
            background-size: cover;
        }
        .box7 {
            position:relative;
            width: 130px;
            height: 130px;
            left:200px;
            top: -50px;
            font-size: 22px;
            color: #000;
            line-height: 100px;
            text-align: center;
            background: #fff;
            border-radius: 50%;
            background: url(https://axiosholding.com/wp-content/themes/axios/assets/img/team/karmiotou.jpg);
            background-size: cover;
        }
        .box8 {
            position:relative;
            width: 130px;
            height: 130px;
            left:200px;
            top: -50px;
            font-size: 22px;
            color: #000;
            line-height: 100px;
            text-align: center;
            background: #fff;
            border-radius: 50%;
            background: url(https://axiosholding.com/wp-content/themes/axios/assets/img/team/giorgi-alexidze.jpg);
            background-size: cover;
        }

        .logo{
            position: absolute;
            top: 34%;
            left: -36%;
            z-index: 10;
            width: 130px;
        }
        .about_content{
            float: right;
            height: 500px;
            left: 55%;
            top: -35%;
            position: absolute;
            width: 475px;
            text-align: center;
            padding: 5% 5%;
            color: #fff;
        }
        .about_content h1, .about_content h2, .about_content h3, .about_content h4, .about_content p{
            color: #fff;
        }
        .test_content{
            margin-top: 150px;
            background: #fff;
            height: 400px;
            width: 400px;


        }
        #about-us-section-1{
            display: none;
        }
        #about-us-section-2{
            display: none;
        }
        @media screen and (min-width: 768px){
            #about-us-section-1{
                display: block;
            }
        }
        @media screen and (max-width: 768px){
            #about-us-section-2{
                display: block;
            }
        }
        @media screen and (min-width: 768px) and (max-width: 1350px){
            #about-us-section-1{
                display: block;
            }
            .about_content {
                float: right;
                height: 500px;
                left: 0%;
                top: 55%;
                position: absolute;
                width: 475px;
                text-align: center;
                padding: 5% 5%;
                color: #fff;
            }
            .axios_team {
                height: 500px;
                position: absolute;
                top: -30%;
                left: 43%;
            }
        }
        @media screen and (min-width: 1350px){
            #about-us-section-1{
                display: block;
            }
            .about_content {
                float: right;
                height: 500px;
                left: 62%;
                top: -35%;
                position: absolute;
                width: 475px;
                text-align: center;
                padding: 5% 5%;
                color: #fff;
            }
            .axios_team {
                height: 500px;
                position: absolute;
                top: -30%;
                left: 22%;
            }
        }
        .scale{

            filter: none!important;
        }
    </style>
    <meta name="description" content="Our motto is creating the most innovative, comprehensive and embracive FinTech hub. To make it real we leverage our knowledge and technology.">
</head>
<body>

<?php include("_header.php"); ?>

<main id="about-us">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="about-us-header-background" src="/wp-content/uploads/2020/05/assets/img/aboutus-header_BG.jpg">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="hero-content-container">
                                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">About us</h1>
                                    <div class="content">
                                        <h3 class="text-center text-sm-left">Our Mission, Vision & Strategy</h3>
                                        <div class="text-center text-sm-left hero-text">
                                            <p class="axios-text-light">Our mission is to create the most innovative, comprehensive and embracive FinTech hub in the world. Whether we’re developing products and services from scratch or nurturing the ideas of other creative minds in our space, we want to be the go-to brand for FinTech solutions.</p>
                                            <p class="axios-text-light">The way we plan to turn our vision into reality is by leveraging our knowledge and technology. Our tech and experience allows us to build an ecosystem of transparent, robust and reliable solutions that can strengthen strategic partnerships for the benefit of both our customers and stakeholders.</p>
                                            <p class="axios-text-light">The company's innovation and growth is directly correlated to the growth and stimulation of our people. What brought us to the dance and what will continue to fuel our quest for excellence is the motto of “Always question, learn and improve.'' Companies are nothing without people and we are firm believers in empowering people to be themselves.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--<div class="d-none d-sm-block container position-relative hero-bottom-img">
                        <div class="position-relative px-0 px-sm-3">
                            <div class="row position-absolute w-100 bg-img">
                                <img alt="about-us-hero-img" src="<?php echo get_template_directory_uri(); ?>/assets/img/aboutus_hero_bottom.jpg">
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
        <!--div id="about-us-section-1" class="container-fluid">
            <div class="container" style="position: relative; height: 700px;">
                <div class="row mx-0">
        <div class="axios_team bg-img hero-bg">
            <div class="cont">
                <div class="">
                    <img id="about_axios_team" class="logo" src=<?php //echo get_template_directory_uri(); ?>/assets/img/axios_about.svg">

                </div>

                <div class="arm arm2">

                    <div id="about_rati" class="box box2">
                        <div style="display: none;" class="test_content">

                            <h4> Rati</h4>
                            <p>
                                Rati brings a wealth of experience to the company combining a unique set of legal and senior management skills. From working as a Chief Legal Advisor to then becoming a partner in a Law Firm, he has accumulated substantial knowledge about the inner workings and intricacies of the legal business framework. As the Director of Axios, he’s a constant source of valuable advice, leadership and strategic guidance, always steering the company in the right direction.
                            </p>
                        </div>
                    </div>



                </div>
                <div class="arm">
                    <div id="about_aleksandre" class="box box5"></div>
                </div>
                <div class="arm">
                    <div id="about_valerian" class="box box3"></div>
                </div>
                <div class="arm">
                    <div id="about_gia" class="box box4"></div>
                </div>
                <div class="arm">
                    <div id="about_aliki" class="box box7"></div>
                </div>
                <div class="arm">
                    <div id="about_giorgi" class="box box8"></div>
                </div>
            </div>
            <div class="col-sm-6">

            </div>

            <div class="col-sm-6">

            </div>
        </div></div>
                <div id="about_content" class="about_content">
                    <h3 class="text-left">Meet the Leadership Team</h3>
                    <p class="text-left">
                        Hover and click over the bios on the left to gain insights on our Leadership Team, their experience and contribution.
                    </p>
                </div>
            </div>
        </div -->

        <!-- div id="about-us-section-2" class="py-5 axios-bg-light">
            <div class="container section-1-cont">
                <div class="row mx-0">
                    <div class="col-12 p-0">
                        <h3 class="text-center text-sm-left">Our Leadership</h3>
                        <div class="container team-container">
                            <div class="row py-3 text-center team-member">
                                <a class="col-sm-6 col-lg-3 px-0 member-photo-link" href=""><div class="bg-img member-photo"><img src="<?php// echo get_template_directory_uri(); ?>/assets/img/team/rati.jpg" class="img-fluid" alt="rati-img"></div></a>
                                <div class="col-sm-6 col-lg-3 text-center text-sm-left pt-3 pt-sm-0 member-info">
                                    <span class="d-block pt-2 name">Rati Tchelidze</span>
                                    <span class="d-block title">Director</span>
                                    <span class="d-block pt-3 pb-2  py-sm-4 social"><a href="#"><i class="fab fa-linkedin-in"></i></a> </span>
                                </div>
                                <div class="col-sm-12 col-lg-6 pt-3 text-center text-sm-left px-0 member-description">
                                    <p>
                                        Rati brings a wealth of experience to the company combining a unique set of legal and senior management skills. From working as a Chief Legal Advisor to then becoming a partner in a Law Firm, he has accumulated substantial knowledge about the inner workings and intricacies of the legal business framework. As the Director of Axios, he’s a constant source of valuable advice, leadership and strategic guidance, always steering the company in the right direction.
                                    </p>
                                </div>
                            </div>
                            <div class="row py-3 text-center team-member">
                                <a class="col-sm-6 col-lg-3 px-0 member-photo-link" href=""><div class="bg-img member-photo"><img src="<?php// echo get_template_directory_uri(); ?>/assets/img/team/valerian.jpg" class="img-fluid" alt="valerian-img"></div></a>
                                <div class="col-sm-6 col-lg-3 text-center text-sm-left pt-3 pt-sm-0 member-info">
                                    <span class="d-block pt-2 name">Valerian Mezvrishvili</span>
                                    <span class="d-block title">CFO</span>
                                    <span class="d-block pt-3 pb-2  py-sm-4 social"><a href="#"><i class="fab fa-linkedin-in"></i></a> </span>
                                </div>
                                <div class="col-sm-12 col-lg-6 pt-3 text-center text-sm-left px-0 member-description">
                                    <p>
                                        Valeri is the CFO of the Group with more than 14 years of experience in corporate finance under his belt. He is responsible for the smooth operation and workings of the Group’s financial activities ensuring they are in line with legal and safety protocols. He is an expert in cost reduction and budgeting with a demonstrated history of producing accurate and timely reports on the financial health of companies.
                                    </p>
                                </div>
                            </div>
                            <div class="row py-3 text-center team-member">
                                <a class="col-sm-6 col-lg-3 px-0 member-photo-link" href=""><div class="bg-img member-photo"><img src="<?php// echo get_template_directory_uri(); ?>/assets/img/team/gia.jpg" class="img-fluid" alt="gia-img"></div></a>
                                <div class="col-sm-6 col-lg-3 text-center text-sm-left pt-3 pt-sm-0 member-info">
                                    <span class="d-block pt-2 name">Gia Janashvili</span>
                                    <span class="d-block title">Executive Board Member</span>
                                    <span class="d-block pt-3 pb-2  py-sm-4 social"><a href="#"><i class="fab fa-linkedin-in"></i></a> </span>
                                </div>
                                <div class="col-sm-12 col-lg-6 pt-3 text-center text-sm-left px-0 member-description">
                                    <p>
                                        Gia is an experienced professional bringing business savviness and vigour to the Group. He offers valuable insights on developing a business strategy focused on financial gain, arranges business development meetings with prospective clients and decides on investment strategies by considering cash and liquidity risks. His contributions give the Group an added level of expertise and opens doors to new business opportunities.
                                    </p>
                                </div>
                            </div>
                            <div class="row py-3 text-center team-member">
                                <a class="col-sm-6 col-lg-3 px-0 member-photo-link" href=""><div class="bg-img member-photo"><img src="<?php// echo get_template_directory_uri(); ?>/assets/img/team/alexandre.jpg" class="img-fluid" alt="alexandre-img"></div></a>
                                <div class="col-sm-6 col-lg-3 text-center text-sm-left pt-3 pt-sm-0 member-info">
                                    <span class="d-block pt-2 name">Aleksandre Matcharashvili</span>
                                    <span class="d-block title">Chief Legal Officer</span>
                                    <span class="d-block pt-3 pb-2  py-sm-4 social"><a href="#"><i class="fab fa-linkedin-in"></i></a> </span>
                                </div>
                                <div class="col-sm-12 col-lg-6 pt-3 text-center text-sm-left px-0 member-description">
                                    <p>
                                        Aleksandre heads the Legal Department of the Group, making sure the company’s interests are both protected and compliant. His experience at two of the Big 4 accountancy firms (PwC + Ernst & Young) alongside his LLM in International Business Law and Master of Law and Business make him the ideal person to spearhead the department and give Axios the necessary safety and stability.
                                    </p>
                                </div>
                            </div>
                     
                            <div class="row py-3 text-center team-member">
                                <a class="col-sm-6 col-lg-3 px-0 member-photo-link" href=""><div class="bg-img member-photo"><img src="<?php// echo get_template_directory_uri(); ?>/assets/img/team/karmiotou.jpg" class="img-fluid" alt="aliki-img"></div></a>
                                <div class="col-sm-6 col-lg-3 text-center text-sm-left pt-3 pt-sm-0 member-info">
                                    <span class="d-block pt-2 name">Aliki Karmiotou</span>
                                    <span class="d-block title">Group Head of Human Resources</span>
                                    <span class="d-block pt-3 pb-2  py-sm-4 social"><a href="#"><i class="fab fa-linkedin-in"></i></a> </span>
                                </div>
                                <div class="col-sm-12 col-lg-6 pt-3 text-center text-sm-left px-0 member-description">
                                    <p>
                                        Aliki is the person responsible for Axios' most important asset, its people. She manages the full recruitment and staffing process as well as designing and implementing our performance management and improvement systems. Aliki spearheads employee on-boarding, training and development along with ensuring employee safety, welfare, health and wellness. Her commitment and contribution to Axios is invaluable; as talent is the bedrock of our business.
                                    </p>
                                </div>
                            </div>
                            <div class="row py-3 text-center team-member">
                                <a class="col-sm-6 col-lg-3 px-0 member-photo-link" href=""><div class="bg-img member-photo"><img src="<?php// echo get_template_directory_uri(); ?>/assets/img/team/giorgi-alexidze.jpg" class="img-fluid" alt="daniel-img"></div></a>
                                <div class="col-sm-6 col-lg-3 text-center text-sm-left pt-3 pt-sm-0 member-info">
                                    <span class="d-block pt-2 name">Giorgi Alexidze</span>
                                    <span class="d-block title">CTO</span>
                                    <span class="d-block pt-3 pb-2  py-sm-4 social"><a href="#"><i class="fab fa-linkedin-in"></i></a> </span>
                                </div>
                                <div class="col-sm-12 col-lg-6 pt-3 text-center text-sm-left px-0 member-description">
                                    <p>
                                        With a degree in Computer Software Engineering and work experience as a CTO in various companies, George brings a wealth of both technical and managerial expertise to Axios Holding. As our Chief Information Officer, he is in charge of information technology (IT) strategy and the computer systems required to support the organisation's unique objectives and goals. His team leads the way in designing the technologies and platforms that push our brands to excellence.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-us-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div -->

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<script>
    $(window).on('load ', function() {

        var about_us_scroll_ctrl = new ScrollMagic.Controller();

        /***************
         * About Us Hero Scroll Reveal Animation
         **************/
        var tween_about_us_hero = new TimelineMax();
        tween_about_us_hero.add([
            TweenMax.fromTo("#about-us .hero-container h2", 1.5,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#about-us .content", 1.3,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#about-us .hero-bottom-img .bg-img", 1.1,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
        ]);
        var scene_about_us_hero = new ScrollMagic.Scene({
            triggerElement: '#about-us',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_about_us_hero.setTween(tween_about_us_hero);
        scene_about_us_hero.addTo(about_us_scroll_ctrl);
        scene_about_us_hero.reverse(true);

        /***************
         * About Us Section 1 Scroll Reveal Animation
         **************/
        var tween_about_us_section_1 = new TimelineMax();
        tween_about_us_section_1.add([
            TweenMax.staggerFromTo("#about-us-section-1 .team-container .team-member",0.4, {x: "-220px", opacity: '0'}, {ease: Power2.easeOut, x: 0, opacity: '1'}, 0.25),
        ]);
        var scene_tween_about_us_section_1 = new ScrollMagic.Scene({
            triggerElement: '.team-container',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_tween_about_us_section_1.setTween(tween_about_us_section_1);
        scene_tween_about_us_section_1.addTo(about_us_scroll_ctrl);
        scene_tween_about_us_section_1.reverse(true);
        TweenLite.defaultEase = Linear.easeNone;

        var arms = $(".arm").length;

        $(".arm").each(function(index, element){
            var space = 360/arms
            TweenLite.set(this, {rotation:space * index,  x: "50"})
            TweenLite.set($(this).find(".box"), {rotation:-space * index, x: "-50"})


        })

        var tl = new TimelineMax({repeat:-1})
        tl.to(".arm", 28, {rotation:"+=360", transformOrigin:"left"})
            .to(".box", 28, {rotation:"-=360"}, 0)
            .from(".box", 7, {x:"+40",y:"-20", ease: Back.easeOut.config(1.7)}, 0)
            .from(".arm", 7, {x:"+40",y:"-20", ease: Back.easeOut.config(3)}, 0)
            .to(".box", 7, {x:"+40",y:"-20", ease: Back.easeOut.config(2), delay: 7}, 0)
            .to(".arm", 7, {x:"+40",y:"-20", ease: Back.easeOut.config(4), delay: 7}, 0)
            .from(".box", 7, {x:"+40",y:"-20", ease: Back.easeOut.config(1.7), delay: 14}, 0)
            .from(".arm", 7, {x:"+40",y:"-20", ease: Back.easeOut.config(3),delay: 14}, 0)
            .to(".box", 7, {x:"+40",y:"-20", ease: Back.easeOut.config(2), delay: 21}, 0)
            .to(".arm", 7, {x:"+40",y:"-20", ease: Back.easeOut.config(4), delay: 21}, 0)



        $("#about_axios_team").click(function() {
            $( "#about_content" ).html("<div class='text-left'><h3>Meet the Leadership Team</h3><p>Hover and click over the bios on the left to gain insights on our Leadership Team, their experience and contribution.</p></div>");
            $("div").removeClass('scale');
        });
        $("#about_valerian").click(function() {
            $( "#about_content" ).html("<div class='text-left'><h3>Valerian Mezvrishvili</h3><h4>CFO</h4><p> \n" +
                "CFO\n" +
                "Valeri is the CFO of the Group with more than 14 years of experience in corporate finance under his belt. He is responsible for the smooth operation and workings of the Group’s financial activities ensuring they are in line with legal and safety protocols. He is an expert in cost reduction and budgeting with a demonstrated history of producing accurate and timely reports on the financial health of companies. </p></div>");
            $("div").removeClass('scale');
            $("#about_valerian").addClass("scale");
        });
        $("#about_gia").click(function() {
            $( "#about_content" ).html("<div class='text-left'><h3>Gia Janashvili</h3><h4>Executive Board Member</h4><p> Gia is an experienced professional bringing business savviness and vigour to the Group. He offers valuable insights on developing a business strategy focused on financial gain, arranges business development meetings with prospective clients and decides on investment strategies by considering cash and liquidity risks. His contributions give the Group an added level of expertise and opens doors to new business opportunities.</p></div>");
            $("div").removeClass('scale');
            $("#about_gia").addClass("scale");
        });
        $("#about_aliki").click(function() {
            $( "#about_content" ).html("<div class='text-left'><h3>Aliki Karmiotou</h3><h4>Group Head of Human Resources</h4><p> Aliki is the person responsible for Axios' most important asset, its people. She manages the full recruitment and staffing process as well as designing and implementing our performance management and improvement systems. Aliki spearheads employee on-boarding, training and development along with ensuring employee safety, welfare, health and wellness. Her commitment and contribution to Axios is invaluable; as talent is the bedrock of our business.</p></div>");
            $("div").removeClass('scale');
            $("#about_aliki").addClass("scale");
        });
        $("#about_giorgi").click(function() {
            $( "#about_content" ).html("<div class='text-left'><h3>Giorgi Alexidze</h3><h4>CTO</h4><p>With a degree in Computer Software Engineering and work experience as a CTO in various companies, George brings a wealth of both technical and managerial expertise to Axios Holding. As our Chief Information Officer, he is in charge of information technology (IT) strategy and the computer systems required to support the organisation's unique objectives and goals. His team leads the way in designing the technologies and platforms that push our brands to excellence.</p></div>");
            $("div").removeClass('scale');
            $("#about_giorgi").addClass("scale");
        });

        $("#about_rati").click(function() {
            $( "#about_content" ).html("<div class='text-left'><h3>Rati Tchelidze</h3><h4>Director</h4><p>Rati brings a wealth of experience to the company combining a unique set of legal and senior management skills. From working as a Chief Legal Advisor to then becoming a partner in a Law Firm, he has accumulated substantial knowledge about the inner workings and intricacies of the legal business framework. As the Director of Axios, he’s a constant source of valuable advice, leadership and strategic guidance, always steering the company in the right direction.</p></div>");
            $("div").removeClass('scale');
            $("#about_rati").addClass("scale");
        });
        $("#about_aleksandre").click(function() {
            $( "#about_content" ).html("<div class='text-left'><h3>Aleksandre Matcharashvili</h3><h4>Chief Legal Officer</h4><p>Aleksandre heads the Legal Department of the Group, making sure the company’s interests are both protected and compliant. His experience at two of the Big 4 accountancy firms (PwC + Ernst & Young) alongside his LLM in International Business Law and Master of Law and Business make him the ideal person to spearhead the department and give Axios the necessary safety and stability.</p></div>");
            $("div").removeClass('scale');
            $("#about_aleksandre").addClass("scale");
        });
        $(".arm").hover(function() {
            tl.pause()
            TweenLite.to($(this).find(".box"), 0.2, {scale:1.4})

        }, function() {
            TweenLite.to($(this).find(".box"), 0.2, {scale:1})

            tl.resume();
        })

    });
</script>
</body>
</html>