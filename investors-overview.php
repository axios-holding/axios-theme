<?php
/**
 * Template Name: Investors overview
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>

    <title>Axios Holding - Investors Overview</title>

    <?php include("_metatags.php"); ?>
    <meta name="description" content="Interested in investment opportunities and relevant information on how you can engage with Axios Holding, get in touch with our dedicated Investor Relations office.">

</head>
<body>

<?php include("_header.php"); ?>

<main id="investors-overview" class="axios-bg-light">
    <div class="position-relative investors-overview-cont">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0 position-relative hero-inner">
                    <div class="bg-img hero-bg">
                        <img alt="investors-overview-header" src="<?php echo get_template_directory_uri(); ?>/assets/img/investorsoverview-header_BG.jpg">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="hero-content-container">
                                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">Investors overview</h1>
                                    <div class="content mx-auto">
                                        <h3 class="pb-3 text-center">Data and documentation for investors, companies and market participants.</h3>
                                        <p class="text-center axios-text-light">For any investment opportunities and relevant information about how you can engage with Axios Holding, please get in touch with our dedicated Investor Relations contact.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
                </div>
            </div>
        </div>
        <div id="investors-overview-section" class="py-5">
            <div class="container section-cont">
                <div class="row mx-0 section-cont-inner">
                    <div class="row h-100">
                        <!--<div class="col-12 col-md-6 py-3 overview-container">
                            <a class="h-100 w-100 axios-bg-white" href="#">
                                <div class="p-4 text-cont">
                                    <div class="pl-2 text-cont-in">
                                        <span class="d-block pb-2 title">Reports & Results</span>
                                        <p class="d-block description">Explore our latest reports, documents and results. Get a clear, concise and transparent insight to AXIOS financial and economic state.</p>
                                        <span class="d-block text-uppercase arrow-icon-cont">Read more<svg class="arrow-icon" width="32" height="32">
                                    <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                        <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                        <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                    </g>
                                </svg></span>
                                    </div>
                                </div>
                            </a>
                        </div>-->
                        <div class="col-12 col-md-6 py-3 overview-container">
                            <a class="h-100 w-100 axios-bg-white" href="<?php echo esc_url(home_url() . '/investors-relations/');?>">
                                <div class="p-4 text-cont">
                                    <div class="pl-2 text-cont-in">
                                        <span class="d-block pb-2 title">IR Contact</span>
                                        <p class="d-block description">For a more thorough and detailed breakdown of AXIOS investment packages, our Investor Relations team is ready to assist you.</p>
                                        <span class="d-block text-uppercase arrow-icon-cont">Read more<svg class="arrow-icon" width="32" height="32">
                                    <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                        <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                        <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                    </g>
                                </svg></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-6 py-3 overview-container">
                            <a class="h-100 w-100 axios-bg-white" href="<?php echo esc_url(home_url() . '/press-releases/');?>">
                                <div class="p-4 text-cont">
                                    <div class="pl-2 text-cont-in">
                                        <span class="d-block pb-2 title">Press Releases</span>
                                        <p class="d-block description">Stay informed with the latest company news and updates from AXIOS.</p>
                                        <span class="d-block text-uppercase arrow-icon-cont">Read more<svg class="arrow-icon" width="32" height="32">
                                    <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                        <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                        <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                    </g>
                                </svg></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--<div class="col-12 col-md-6 py-3 overview-container">
                            <a class="h-100 w-100 axios-bg-white" href="#">
                                <div class="p-4 text-cont">
                                    <div class="pl-2 text-cont-in">
                                        <span class="d-block pb-2 title">Key Figures</span>
                                        <p class="d-block description">Here you will find the latest key figures for AXIOS. With more visualisation, innovate data presentation formats and shorter texts, we tried to make this easier, more accessible and digestible for everyone.</p>
                                        <span class="d-block text-uppercase arrow-icon-cont">Read more<svg class="arrow-icon" width="32" height="32">
                                    <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                        <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                        <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                    </g>
                                </svg></span>
                                    </div>
                                </div>
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>
    </div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nice-select.min.js"></script>
<script>
    $(document).ready(function() {

    });
    $(window).on('load ', function() {
        var tweenPosts =new TimelineMax();
        tweenPosts.add([
            TweenMax.staggerFromTo("#investors-overview-section .overview-container",0.4, {x: "-220px", opacity: '0'}, {ease: Power1.easeOut, x: 0, opacity: '1', delay:1}, 0.15),

        ]);
    });
</script>
</body>
</html>
