$(document).ready(function() {

    cookie_policy();

    set_bg_img();

    //Search Menu
    $(document).on("click",'#search-button', function(e){
        e.preventDefault();
        $(this).addClass('search-active');
        $('.menu-trigger').toggleClass('active search-close-active');
        $("#search").toggleClass('active search-btn-close');
        $("body").toggleClass('search-active');
    });


//Submit search on icon click
    $(document).on("click",'#searchIcon', function(e){
        e.preventDefault();
        closeSearch();
        processSearchForm();
    });

//Search close on press esc
    $(document).on('keyup',function(evt) {
        if (evt.keyCode === 27) {
            closeSearch();
        }
    });


//Search close on press enter or esc
    jQuery("#search-field").on('keyup', function (e) {
        e.preventDefault();

        //Enter key
        if (e.keyCode === 13) {
            closeSearch();
            processSearchForm();
        }

        //Escape key
        if (e.keyCode === 27) {
            closeSearch();
        }
    });

    function processSearchForm(){
        //Process Search Form here
        //...
    }

    //Close Search
    function closeSearch(){
        var  trigger = $('.menu-trigger');
        if (trigger.hasClass('search-close-active')){
            trigger.removeClass('active');
            trigger.removeClass('search-close-active');
        }
        $("#search").removeClass('active');
        $('#search-button').removeClass('search-active');
        $("body").removeClass('search-active');
    }

    $('.menu-trigger').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('search-close-active')) {
            closeSearch();
        } else if ($(this).hasClass('menu-close-active')) {
            close_menu();
        } else{
            $(this).toggleClass('active menu-close-active');
            $('#mega-menu').toggleClass('open');
            $("body").toggleClass('menu-open');
            setTimeout(function() {
                $('#search-button').addClass('d-none');
            }, 600);
        }

    });

    //Search close on press esc
    $(document).on('keyup',function(evt) {
        if (evt.keyCode === 27) {
            close_menu();
        }
    });
    //If in landscape and keyboard not focused show landscape warning
        $(window).on("deviceorientation", function( event ) {
            if (window.matchMedia("(orientation: portrait)").matches) {
                $("html").removeClass("isLandscape");
            }
            if (window.matchMedia("(orientation: landscape)").matches) {
                if($("input").is(":focus")){}else{$("html").addClass("isLandscape");}
            }
        });
    //Close Search
    function close_menu(){
        var trigger = $('.menu-trigger');
        trigger.removeClass('active');
        trigger.removeClass('menu-close-active');
        $("#mega-menu").removeClass('open');
        $("body").removeClass('menu-open');
        setTimeout(function() {
            $('#search-button').removeClass('d-none');
        }, 300);
    }

    $(window).on('scroll', function () {
        var offset = this.pageYOffset;
        var menu = $('#menu-navbar');
        var cookie = $('#cookie-policy');
        if ((offset > 0))  {
            menu.addClass('sticky-menu');
            cookie.addClass('sticky-cookie');
        } else {
            menu.removeClass('sticky-menu');
        }
    });

});

$(window).on('load',function () {

    //Hide preloader and animate body
    setTimeout(function() {
        $(".preloader").fadeOut(400);
        }, 1200);
    setTimeout(function() {
        $(".preloader").addClass('loaded');
    }, 600);



});

/** Contact From*/

//Make label smaller on input focus
$(document).on("focus",'input:not([type="checkbox"]),textarea', function(){
    $(this).parents('.form-group').addClass('focused');
});

//Resize back to normal the label on input blur
$(document).on("blur",'input:not([type="checkbox"]),textarea', function(){
    var inputValue = $(this).val();
    if ( inputValue === "" ) {
        $(this).removeClass('filled');
        $(this).parents('.form-group').removeClass('focused');
    } else {
        $(this).addClass('filled');
    }
});

//Check inputs if filled and resize on page load
function checkInputs(){

    $('input:not([type="checkbox"]),textarea').each(function() {
        var inputValue = $(this).val();
        if ((inputValue!="")) {
            $(this).addClass('filled');
            $(this).parents('.form-group').addClass('focused');
        } else {
            $(this).removeClass('filled');
            $(this).parents('.form-group').removeClass('focused');

        }
    });
}
/** Mouse Cursor */
document.addEventListener("DOMContentLoaded", function(event) {
    var cursor = document.querySelector(".custom-cursor");
    var links = document.querySelectorAll("a");
    var inputs = document.querySelectorAll("input");


    var initCursor = false;

    for (var i = 0; i < links.length; i++) {
        var selfLink = links[i];
        selfLink.addEventListener("mouseover", function() {
            cursor.classList.add("custom-cursor--link");
        });
        selfLink.addEventListener("mouseout", function() {
            cursor.classList.remove("custom-cursor--link");
        });
    }

    for (var j = 0; j < inputs.length; j++) {
        var selfInput = inputs[j];
        selfInput.addEventListener("mouseover", function() {
            cursor.classList.add("custom-cursor--link");
        });
        selfInput.addEventListener("mouseout", function() {
            cursor.classList.remove("custom-cursor--link");
        });
    }


    window.onmousemove = function(e) {
        var mouseX = e.clientX;
        var mouseY = e.clientY;

        if (!initCursor) {
            // cursor.style.opacity = 1;
            TweenLite.to(cursor, 0.3, {
                opacity: 1
            });
            initCursor = true;
        }

        TweenLite.to(cursor, 0, {
            top: mouseY + "px",
            left: mouseX + "px"
        });
    };

    window.onmouseout = function(e) {
        TweenLite.to(cursor, 0.3, {
            opacity: 0
        });
        initCursor = false;
    };
});

/** Cookie Warning */
function cookie_policy(){

    checkCookie();

    $("#accept-cookie").click(function () {
        setCookie("acceptCookies", "true", 30);
        $('.cookie-policy').hide();
    });


    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function checkCookie() {
        var cookiePolicy = getCookie("acceptCookies");
        if (cookiePolicy === "" || cookiePolicy == null) {
            $('.cookie-policy').show();
        } else {

        }
    }
}

/** Set background images */
function set_bg_img() {
    $('.bg-img>img').each(function() {
        var src = $(this).attr('src');
        $(this).parent().css({
            'background-image' : 'url(' + src + ')',
            'background-repeat' : 'no-repeat'
        });
        $(this).remove();
    });
}




