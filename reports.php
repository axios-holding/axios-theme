<?php
/**
 * Template Name: Reports
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>

    <title>Axios Holding - Reports</title>

    <?php include("_metatags.php"); ?>
</head>
<body class="withBreadcrumb">

<?php include("_header.php"); ?>

<main id="keyfigures" class="keyfigures position-relative">

    <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <div class="bg-img hero-bg">
                    <img alt="reports-header" src="<?php echo get_template_directory_uri(); ?>/assets/img/reports-header_BG.jpg">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-content-container">
                                <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">Reports & results</h1>
                                <div class="content">
                                    <div class="text-center hero-text">
                                        <p class="col-12 col-lg-6 px-0 mx-auto text-center axios-text-light">Whether you are a current or prospective shareholder, this section will provide you with the most up-to-date information about the company’s results and recent performance.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-us-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none"></div>
            </div>
        </div>
    </div>

    <div class="content axios-bg-light content-container">
        <div class="container">
            <div class="row d-block">
                <div class="col-12 pb-3 back-button">
                    <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0">
                        <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                            <svg class="arrow-icon" width="32" height="32">
                                <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                                    <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                                    <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                                </g>
                            </svg>
                        </span>
                        Back to investors Overview
                    </a>
                </div>
            </div>
            <div class="row mt-10 d-md-none"><div class="col-12">
                    <div class="selectTab__openMobile"><a href="#"><span id="sectionName">2018</span> <i class="fas fa-angle-down"></i></a></div>
                    <div class="selectTab__mobile" id="selectTab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="selectTab__button"><a href="#" data-tab="tab2018">2018</a> </li>
                            <li class="selectTab__button"><a href="#" data-tab="tab2017">2017</a> </li>
                            <li class="selectTab__button"><a href="#" data-tab="tab2016">2016</a> </li>
                            <li class="selectTab__button"><a href="#" data-tab="tab2015">2015</a> </li>
                            <li class="selectTab__button"><a href="#" data-tab="tab2014">2014</a> </li>
                            <li class="selectTab__button"><a href="#" data-tab="tabarchive">Archive</a> </li>
                        </ul>
                    </div>
                </div></div>
            <div class="row mt-10 d-none d-md-block"><div class="col-12">
                    <ul class="nav nav-tabs" id="tableTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="2018-tab" data-toggle="tab" href="#tab2018" role="tab" aria-controls="tab2018" aria-selected="true">2018</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="2017-tab" data-toggle="tab" href="#tab2017" role="tab" aria-controls="tab2017" aria-selected="false">2017</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="2016-tab" data-toggle="tab" href="#tab2016" role="tab" aria-controls="tab2016" aria-selected="false">2016</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="2015-tab" data-toggle="tab" href="#tab2015" role="tab" aria-controls="tab2015" aria-selected="false">2015</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="2014-tab" data-toggle="tab" href="#tab2014" role="tab" aria-controls="tab2014" aria-selected="false">2014</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="archive-tab" data-toggle="tab" href="#tabarchive" role="tab" aria-controls="tabarchive" aria-selected="false">Archive</a>
                        </li>
                    </ul>
                </div></div>
            <div class="row">
                <div class="col-12">
                    <div class="tab-content" id="tabsContent">
                        <div class="tab-pane fade show active" id="tab2018" role="tabpanel" aria-labelledby="2018-tab">
                            <div class="row tableHeader d-none d-md-flex">
                                <div class="col-7">Title</div>
                                <div class="col-2 text-center">Report</div>
                                <div class="col-3 text-center">Representation</div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2018</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2018</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2018</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2018</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2017" role="tabpanel" aria-labelledby="2017-tab">
                            <div class="row tableHeader d-none d-md-flex">
                                <div class="col-7">Title</div>
                                <div class="col-2 text-center">Report</div>
                                <div class="col-3 text-center">Representation</div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2017</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2017</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2016" role="tabpanel" aria-labelledby="2016-tab">
                            <div class="row tableHeader d-none d-md-flex">
                                <div class="col-7">Title</div>
                                <div class="col-2 text-center">Report</div>
                                <div class="col-3 text-center">Representation</div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2016</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2016</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2 text-center">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2 text-center">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2015" role="tabpanel" aria-labelledby="2015-tab">
                            <div class="row tableHeader d-none d-md-flex">
                                <div class="col-7">Title</div>
                                <div class="col-2 text-center">Report</div>
                                <div class="col-3 text-center">Representation</div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2015</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2015</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2014" role="tabpanel" aria-labelledby="2014-tab">
                            <div class="row tableHeader d-none d-md-flex">
                                <div class="col-7">Title</div>
                                <div class="col-2 text-center">Report</div>
                                <div class="col-3 text-center">Representation</div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2014</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2014</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabarchive" role="tabpanel" aria-labelledby="archive-tab">
                            <div class="row tableHeader d-none d-md-flex">
                                <div class="col-7">Title</div>
                                <div class="col-2 text-center">Report</div>
                                <div class="col-3 text-center">Representation</div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2012</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tableRow">
                                <div class="row">
                                    <div class="col-md-7 mt-2 mb-4 mb-md-0"><i class="far fa-chart-bar"></i> Axios Group Annual Report 2010</div>
                                    <div class="col-6 col-md-2 text-center">
                                        <div class="d-md-none mb-2">Report</div>
                                        <a href="#"><div class="pdfIcon"></div></a>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <div class="d-md-none mb-2">Representation</div>
                                        <a href="#"><div class="representationIcon"></div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none"></div>

</main>

<?php include("_footer.php"); ?>
<?php include("_scripts.php"); ?>
<script>
    $(document).ready(function(e) {

        jQuery(".selectTab__openMobile a").click(function(e){
            e.preventDefault();
            jQuery(".selectTab__mobile").slideToggle();
            jQuery(".selectTab__openMobile i").toggleClass("fa-angle-down");
            jQuery(".selectTab__openMobile i").toggleClass("fa-angle-up");
        });

        jQuery(".selectTab__button a").click(function(e){
            e.preventDefault();
            jQuery(".selectTab__button a").removeClass("active");
            $(this).addClass("active");
            var goTo=$(this).attr("data-tab")

            $('#tableTab a[href="#'+goTo+'"]').tab('show') // Select tab by name

            if($(window).width()<992){
                jQuery(".selectTab__mobile").slideToggle();
                jQuery(".selectTab__openMobile i").toggleClass("fa-angle-down");
                jQuery(".selectTab__openMobile i").toggleClass("fa-angle-up");

                jQuery(".selectTab__openMobile a span").html($(this).html());
            }
        });

    });
</script>
</body>
</html>