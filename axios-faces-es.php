<?php
/**
 * Template Name: Axios Faces Es
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
            ol, li {
            font-family: "Nunito Sans", sans-serif;
            font-size: 15px;
        }
            .newsletter-interview p {
            color: #000;
        }
        @media screen and (max-width: 600px){
            .news2_stats h1{
                font-size: 16px;
            }
            .news2_stats h2{
                 font-size: 8px;
             }
            .news2_stats h3{
                font-size: 12px;
            }
        }
        /*start carousel */        
    	@media (min-width: 768px) {

    /* show 3 items */
    .carouselPrograms .carousel-inner .active,
    .carouselPrograms .carousel-inner .active + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        display: block;
    }

    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,
	.carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
        transition: none;
    }

    .carouselPrograms .carousel-inner .carousel-item-next,
    .carouselPrograms .carousel-inner .carousel-item-prev {
        position: relative;
        transform: translate3d(0, 0, 0);
    }

    

    /* left or forward direction */
    .carouselPrograms .active.carousel-item-left + .carousel-item-next.carousel-item-left,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* farthest right hidden item must be abso position for animations */
    .carouselPrograms .carousel-inner .carousel-item-prev.carousel-item-right {
        position: absolute;
        top: 0;
        left: 0%;
        z-index: -1;
        display: block;
        visibility: visible;
    }

    /* right or prev direction */
    .carouselPrograms .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
.carousel-item{
        margin: 2% 1.6%;
        -webkit-transition: none;
        transition: none;
}
img.img-fluid.mx-auto.d-block{
    height: 190px;
    width: 350px;
}
.thumb img{
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
}
.thumb img:hover{
    -webkit-filter: unset;
    filter: unset;
}
.panel-thumbnail:hover .thumb img{
    -webkit-filter: unset;
    filter: unset;
}
.carousel-control-prev, .carousel-control-next{
    width: 2%;
}
.number-overlay{
    position: absolute;
    top: 35%;
    left: 38%;
    color: #fff;
    font-size: 100px;
    text-align: center;
}
.places-to-go {
    height: 110px;
}
@media screen and (max-width: 598px){
    iframe{
        width: 100%!important;
    }
}
    </style>
</head>
<body>

    <!--Preloader-->
    <div class="preloader position-fixed w-100">
        <div class="loaderContainer">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>

    <!--Mobile Device Landscape Mode Message-->
    <div class="landscape">
        <div class="landscape__text">Please turn your device</div>
    </div>


    <main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0 newsletter-axios-companies">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                    <li class="d-inline-block px-2"><a href="/axios-universe-es">Universo Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-es">Empresas Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-es">Axios en caras</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-es">Noticias de Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-es">Lecturas Interesantes</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-es">Axios Recomienda</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-es">Encuesta Axios</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ESPAÑOL
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-faces-en">ENGLISH</a></li>
                                    <li><a href="/axios-faces-ru">РУССКИЙ</a></li>
                                </ul>
                            </div>
                        </ul>
                        <h1 class="text-center underline underline-light inner-template-heading">Las Caras de Axios: David Alimbarashvili</h1>
                        <div class="content mx-auto">
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center text-black">
                            Continuamos presentándote las caras de Axios, personas que diariamente contribuyen al desarrollo de nuestra innovadora plataforma tecnológica (Fintech) con su talento, pasión y esfuerzo.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center pb-5 text-black">
                            Nuestro héroe hoy es David Alimbarashvili, Director de Operaciones en Equfin Holdings. Durante una entrevista con nuestra Oficial de Relaciones Públicas, Maria Babchenko, David no solo compartió sus principales lecciones de vida, sino que también nos compartió que lo hace feliz y dónde le gustaría tener las vacaciones de sus sueños.
                            </p>
                        </div>
                    </div>
                    <div class="pt-5"></div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="container newsletter-interview">
       
                <div class="row mx=0">
                    <div class="col-12 px-0 pb-5">
                        <div class="">
                            <h2 class="text-center inner-template-heading text-white p-5">MEET DAVID ALIMBARASHVILI, <br> COO EQUFIN HOLDINGS</h2>
                            <div class="text-center pb-5">
                                <img style="width: 220px;" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/david-alimbarashvili.jpg">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row m-md-5 bg-white p-5"  style="margin-top: -160px!important;">
                        <div class="col-md-6 p-5">
                            <p>Maria B.: <strong>¡Hola David! ¿Estás listo para asumir el desafío #axiosfaces y responder algunas preguntas sobre David, la persona?</strong></p>
                            <p>David A.: <strong>¡Hola Maria! me encantan los desafíos.</strong></p>
                            <p>Maria B.: <strong>OK, te haré 7 preguntas, tu tarea es responderlas en 15 minutos. ¿Estás listo?</strong></p>
                            <p>David A.: <strong>¡Vamos, estoy listo!</strong></p>
                            <p><strong>¿Cuál es tu pasatiempo favorito? ¿Qué haces en tu tiempo libre?</strong></p>
                            <p>Me encanta nadar y esquiar. Solía ​​ser un nadador medio profesional. Además, me gusta leer. Solía ​​leer mucho, pero ahora, debido a los largos viajes, tengo tiempo casi solo para la literatura profesional. Pero, aun así, trato de encontrar tiempo para leer algunos bestsellers, por ejemplo "Sapiens: Una breve historia de la humanidad" de Yuval Noah Harari.</p>
                            <p><strong>¿Cuándo viajas, cuál es tu destino favorito?</strong></p>
                            <p>Me gusta la diversidad, así que no puedo decir que tenga un solo destino favorito. Pero en general me encanta viajar, y no importa si voy a España, Kazajstán o Zambia. Cuando voy a algún lugar, quiero tener algo de tiempo para ver los lugares de interés dentro y fuera de la capital.</p>
                            <p><strong>¿Cuál es el destino de tus sueños?</strong></p>
                            <p>Nueva York. Tal Vez te sorprenderías, pero nunca he estado allí.</p>
                        </div>
                        <div class="col-md-6 p-5">
                            <p><strong>¿Cómo es tu fin de semana ideal?</strong></p>
                            <p>Barbacoa con mi familia y amigos en el patio trasero.</p>
                            
                            <p><strong>¿Cuál es la aplicación de teléfono que usas con más frecuencia?</strong></p>
                            <p>Facebook, para ver publicaciones, fotos de mis amigos, que viven en todo el mundo, y leer noticias de la industria.</p>

                            <p><strong>¿Cuáles son esas tres principales lecciones que has aprendido en tu vida que te gustaría compartir con el mundo?</strong></p>
                            <p>
                                <ol>
                                    <li>Si desea tener éxito, debe establecer un objetivo, crear un plan para lograrlo y seguirlo paciente y meticulosamente cada día.</li>
                                    <li>Si enfrentas un desafío, da un paso atrás y usa la información que tienes tanto como sea posible. Al retroceder, relájate y distraete de las actividades cotidianas, deja que tu mente encuentre una solución al problema que ha estado buscando. Además, cuando tu mente encuentre una solución al problema que no parece ser muy racional, es mejor dar un paso atrás y consultarlo con la almohada. Al día siguiente verás si la solución es racional o no.</li>
                                    <li>Tenemos que asumir la responsabilidad. Tenemos que rezar lo que predicamos. Necesitamos exigir de nosotros mismos lo que exigimos de los demás.</li>
                                </ol>
                            </p>

                            <p><strong>¿Cuál es tu definición de felicidad?</strong></p>
                            <p>
                                 La felicidad para mí es más un proceso continuo que una meta final. Pero la clave de la felicidad es saber exactamente lo que quieres.
                            </p>

                           
                        </div>

                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>
</main>

    <footer>
        <div class="axios-bg-white">
            <div class="container py-4 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                    <li class="d-inline-block px-2"><a href="/axios-universe-es">Universo Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-es">Empresas Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-es">Axios en caras</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-es">Noticias de Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-es">Lecturas Interesantes</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-es">Axios Recomienda</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-es">Encuesta Axios</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ESPAÑOL
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-faces-en">ENGLISH</a></li>
                                    <li><a href="/axios-faces-ru">РУССКИЙ</a></li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <h2 class="pt-3 text-center">THE AXIOS HOLDING <br> NEWSLETTER</h2>
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 cookie-policy-content">
                    <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>

<script>
    $('#carouselExample').on('slide.bs.carousel', function (e) {

    
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
    });





    $(document).ready(function() {
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function(event){
    event.preventDefault();
    var content = $('.modal-body');
    content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    });

    });
</script>
</body>
</html>