<?php
/**
 * Template Name: Axios Universe Ru
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
        @media screen and (max-width: 600px){
            .news2_stats h1{
                font-size: 16px;
            }
            .news2_stats h2{
                 font-size: 8px;
             }
            .news2_stats h3{
                font-size: 12px;
            }
        }
        /*start carousel */        
    	@media (min-width: 992px) {

    /* show 3 items */
    .carouselPrograms .carousel-inner .active,
    .carouselPrograms .carousel-inner .active + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        display: block;
    }

    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,
	.carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
        transition: none;
    }

    .carouselPrograms .carousel-inner .carousel-item-next,
    .carouselPrograms .carousel-inner .carousel-item-prev {
        position: relative;
        transform: translate3d(0, 0, 0);
    }

    

    /* left or forward direction */
    .carouselPrograms .active.carousel-item-left + .carousel-item-next.carousel-item-left,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* farthest right hidden item must be abso position for animations */
    .carouselPrograms .carousel-inner .carousel-item-prev.carousel-item-right {
        position: absolute;
        top: 0;
        left: 0%;
        z-index: -1;
        display: block;
        visibility: visible;
    }

    /* right or prev direction */
    .carouselPrograms .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
.carousel-item{
        margin: 2% 1.6%;
        -webkit-transition: none;
        transition: none;
}
img.img-fluid.mx-auto.d-block{
    height: 190px;
    width: 350px;
}
.thumb img{
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
}
.thumb img:hover{
    -webkit-filter: unset;
    filter: unset;
}
.panel-thumbnail:hover .thumb img{
    -webkit-filter: unset;
    filter: unset;
}
.carousel-control-prev, .carousel-control-next{
    width: 2%;
}
.number-overlay{
    position: absolute;
    top: 35%;
    left: 38%;
    color: #fff;
    font-size: 100px;
    text-align: center;
}
.places-to-go {
    height: 110px;
}
.carousel-control-next {
    right: -5px;
}
.places-to-go .company-newsletter-size{
    width: auto;
}
.newsletter-axios-companies-img img{
    width: 200px;
}
ol.text-white.text-center.pb-5.ol-top-things {
    padding: 0;
}
@media screen and (min-width:992px) and (max-width:1140px){
    .news2_stats h2{
        font-size: 12px;
    }
    .news2_stats h3{
        font-size: 11px;
    }
    .news2_stats p{
        font-size: 10px;
    }
}

    </style>
</head>
<body>

    <!--Preloader-->
    <div class="preloader position-fixed w-100">
        <div class="loaderContainer">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>

    <!--Mobile Device Landscape Mode Message-->
    <div class="landscape">
        <div class="landscape__text">Please turn your device</div>
    </div>


    <main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                        <li class="d-inline-block px-2"><a href="/axios-universe-ru">Вселенная Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-ru">Бренды Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-ru">Axios в лицах</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-ru">Новости Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-ru">Интересное чтение</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-ru">Axios рекомендует</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-ru">Опрос</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">РУССКИЙ
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-universe-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-universe-en">ENGLISH</a></li>
                                </ul>
                            </div>
                        </ul>
                        <h2 class="text-center underline underline-light inner-template-heading">География Axios</h2>
                        <div class="content mx-auto">
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center text-black">
                            В этом новом разделе нашего ньюслеттера мы будем знакомить вас со странами, в которых находятся компании холдинга. Небольшой урок географии ещё никому не помешал, не правда ли? Даже наоборот, он поможет вам больше узнать о разных частях Вселенной Axios и познакомиться с вашими коллегами со всей Европы.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center pb-5 text-black">
                                <strong>Героем этого выпуска стала Украина - одна из крупнейших стран Европы и родина большинства сотрудников Axios.
                                </strong>
                            </p>
                        </div>
                    </div>
                    <div class="pt-5"></div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">Несколько фактов о Украине</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/kiev.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-lg-6 mx-auto pb-5 news2_stats">
                <div class="col-6 col-md-6 col-lg-3 text-center mx-auto">
                    <h2 class="underline underline-light text-white">Местоположение</h2>
                    <h3 class="text-white"><strong>Восточная Европа</strong></h3>
                </div>
                <div class="col-6 col-md-6 col-lg-3 text-center mx-auto">
                    <h2 class="underline underline-light text-white">Население</h2>
                    <h3 class="text-white"><strong>42+M</strong></h3>
                </div>
                <div class="col-6 col-md-6 col-lg-3 text-center mx-auto">
                    <h2 class="underline underline-light text-white">Язык</h2>
                    <h3 class="text-white"><strong>украинский </strong></h3>
                    <p class="text-center text-white">(большая часть населения страны, особенно в восточных и центральных регионах, также говорит по-русски)</p>
                </div>
                <div class="col-6 col-md-6 col-lg-3 text-center mx-auto">
                    <h2 class="underline underline-light text-white">Столица</h2>
                    <h3 class="text-white"><strong>Киев</strong></h3>
                    <p class="text-center text-white">(а также дом для 5 компаний холдинга)</p>
                </div>
                <div class="col-12 text-center mx-auto">
                    <h2 class="text-center underline underline-light inner-template-heading p-5 text-white">Топ 5 идей для уикенда в Киеве:</h2>
                </div>
        
            </div>
            <div class="row pb-5">
                <!-- start of carousel -->
                <div id="carouselExample" class="carouselPrograms carousel  mx-auto" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner row w-100 mx-auto" role="listbox">
                            <div class="carousel-item col-lg-2 col-md-12  active">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 1" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-1.png" alt="slide 1">
                                    </a>
                                    <h1 class="number-overlay">1</h1>
                                </div>
                                </div>
                            </div>
                            <div class="carousel-item col-lg-2 col-md-12  ">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 3" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-2.png" alt="slide 2">
                                    </a>
                                    <h1 class="number-overlay">2</h1>
                                </div>
                                </div>
                            </div>
                            <div class="carousel-item col-lg-2 col-md-12  ">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 4" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-3.png" alt="slide 3">
                                    </a>
                                    <h1 class="number-overlay">3</h1>
                                </div>
                                </div>
                            </div>
                            <div class="carousel-item col-lg-2 col-md-12  ">
                                <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 5" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-4.png" alt="slide 4">
                                    </a>
                                    <h1 class="number-overlay">4</h1>
                                </div>
                                </div>
                            </div>
                            <div class="carousel-item col-lg-2 col-md-12  ">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail">
                                    <a href="#" title="image 6" class="thumb">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/do-in-kiev-5.png" alt="slide 5">
                                    </a>
                                    <h1 class="number-overlay">5</h1>
                                </div>
                                </div>
                            </div>
                            
                        </div>
                        <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                        <!-- end of carousel -->
                    <div class="col-12 pt-3 justify-content-center text-white">
                            <ol class="text-white text-center pb-5 ol-top-things">
                                <li>Пройтись по легендарному Крещатику до Майдана незалежности </li>
                                <li>Посетить церкви и пещеры Киево-печерской лавры </li>
                                <li>Спуститься по Андреевскому спуску (или подняться для зарядки)</li>
                                <li>Прокатиться на киевском фуникулёре </li>
                                <li>Попробовать традиционную украинскую кухню: борщ, вареники и сало</li>
                            </ol>
                 </div>
            </div>
            <div class="row justify-content-md-center pb-5 text-white text-center">
            <div class="col-12 px-0">
                 <h2 class="text-center inner-template-heading p-5 text-white">Куда пойти:</h2>
            </div>
                <div class="col col-lg-2">
                <div class="places-to-go">
                    <img class="company-newsletter-size" alt="puzata hata" src="<?php echo get_template_directory_uri(); ?>/assets/img/to-go-1.png">
                </div>
                    <p class="text-white">
                    Пузата хата ($) <br> Жизнь замечательных людей ($$) <br> Nam ($$$)
                    </p>
                </div>
                <div class="col-lg-2">
                  <div class="places-to-go">
                    <img class="company-newsletter-size" alt="Lviv Croissants" src="<?php echo get_template_directory_uri(); ?>/assets/img/to-do-2.png">
                  </div>
                    <p class="text-white">
                         Lviv Croissants <br> One Love espresso bar <br> Чашка
                    </p>
                </div>
                <div class="col col-lg-2">
                    <div class="places-to-go">
                     <img class="company-newsletter-size" alt="Loggerhead" src="<?php echo get_template_directory_uri(); ?>/assets/img/to-do-3.png">
                    </div>
                    <p class="text-white">
                         Loggerhead <br> Parovoz <br> Кеды искусствоведа
                    </p>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator newsletter-block-separator-gray separator-bottom position-absolute fixed-bottom angled-separator flip-x separate-gray">
                    </div>
                </div>
            </div>
        </div>
        <!-- Start of Axios Companies -->
        <div class="container-fluid bg-gray newsletter-axios-companies newsletter-axios-companies-img">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5 pt-5">
                    <div class="">
                        <h1 class="text-center inner-template-heading py-4 px-2">Компании Axios с офисами в Киеве</h1>
                        <h2 class="text-center underline underline-light inner-template-heading">Поставщики технологий</h2>
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/new-age-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                            NewAge Solutions - это технический центр под брендом Axios. Компания помогает инкубировать и развивать стартапы холдинга, снабжая их подходящими техническими решениями. В частности, NewAge Solutions разрабатывает программное обеспечение для платформ форекс, CRM для форекс и гейминга. Видение компании - быть технопарком холдинга и поддерживать его в процессе его стремительного роста.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                <a href="https://newage.io/" target="_blank"> Узнайте больше о NewAge Solutions </a>
                            </p>    
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx=0">
                <div class="col-12 px-0">
                    <div class="">
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/fincue-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                            Разработанная Fincue CRM-платформа для микрозаймов является настоящим лидером в своей нише. Её технология поддерживает все стадии развития продукта и помогает автоматизировать большинство процессов, связанных с заявками на получение займа и выплатой денежных средств. Эта платформа также свободно интегрируется со сторонними сервисами и предоставляет подробную аналитику данных. Одним словом, это лучшее в своём роде решение для выдачи микрозаймов онлайн.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                <a href="http://www.fincue.com/" target="_blank"> Узнайте больше о Fincue: http://www.fincue.com/ </a>
                            </p>  
                            
                        </div>
                    </div>
                </div>
            </div> <!-- END OF FINCUE -->
            <div class="row mx=0">
                <div class="col-12 px-0">
                    <div class="">
                        <div class="content mx-auto">
                            <div class="text-center">
                                <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/overonix-black.png">
                            </div>
                            <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                            С момента своего основания в 2017 году, Overonix является лидером в предоставлении таких технологических решений для Forex, как CRM-система, инструменты для вовлечения аудитории, хранилище данных и торговый движок. Миссия компании - разрабатывать самые передовые и удобные для пользователя системы, решающие широкий спектр проблем клиентов.
                            </p>
                        </div>

                    </div>
                </div>
            </div><!-- end of overonix -->
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading p-5">Микрозаймы онлайн</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/equfin-black.png">
                        </div>
                        <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                        Холдинг был основан в 2012 году с видением дать любому человеку, подключённому к Интернету, доступ к финансам посредством самых современных технологий. За это время Equfin успел укрепить свою позицию на рынке Испании, Грузии, Украины и Армении. Компания предоставляет потребительские микрозаймы людям, испытывающим финансовый дефицит.

                        </p>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                <a href="https://equfin.com/" target="_blank"> Узнайте больше о Equfin Holding </a>
                        </p>  

                    </div>
                </div>
            </div><!-- end of equfin -->
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading p-5">Брокераж и ликвидность</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/everfx-black.png">
                        </div>
                        <p class="col-12 col-lg-6 px-3 mx-auto text-center">
                        Наш эксклюзивный партнёр EverFX - это международная брокерская онлайн-компания, предоставляющая розничным и институциональным инвесторам доступ к более чем 130 торговым инструментам по 6 классам активов. EverFX успешно работает во всем мире и делает основной упор на оказании надежных профессиональных услуг широкому кругу клиентов. Успех компании основан на разнообразии, гибкости, прозрачности предлагаемых решений и на открытой, хорошо обоснованной структуре ценообразования. Целью EverFX является налаживание долгосрочных и прочных отношений со своими клиентами путем предоставления им необходимых инструментов, знаний и навыков, а также оказания поддержки в достижении успеха и стабильности в их торговой деятельности. С 2018 года EverFX выступает официальным глобальным партнёром футбольного клуба “Севилья”.
                        </p>
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center">
                                <a href="https://everfx.com/" target="_blank"> Узнайте больше о EverFX </a>
                        </p>  

                    </div>
                </div>
            </div><!-- end of everfx -->
            
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div><!-- End of Axios Companies -->
        <div class="container-fluid axios-bg-white newsletter-axios-companies">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5 pt-5">
                    <div class="">
                        <h1 class="text-center underline inner-template-heading">AXIOS В УКРАИНЕ</h1>
                        <h2 class="text-center underline underline-light inner-template-heading p-5">85% сотрудников Axios Holding</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="ukraine statistics" src="<?php echo get_template_directory_uri(); ?>/assets/img/ukraine-stats.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h2 class="text-center underline underline-light inner-template-heading text-white p-5">Национальность сотрудников Axios Holding в Украине</h2>
                        <div class="text-center pb-5">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/nationality.png">
                        </div>
                        

                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>
</main>

    <footer>
        <div class="axios-bg-white">
            <div class="container py-4 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                    <li class="d-inline-block px-2"><a href="/axios-universe-ru">Вселенная Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-ru">Бренды Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-ru">Axios в лицах</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-ru">Новости Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-ru">Интересное чтение</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-ru">Axios рекомендует</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-ru">Опрос</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">РУССКИЙ
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-universe-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-universe-en">ENGLISH</a></li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <h2 class="pt-3 text-center">THE AXIOS HOLDING <br> NEWSLETTER</h2>
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 cookie-policy-content">
                    <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>

<script>
    $('#carouselExample').on('slide.bs.carousel', function (e) {

    
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
    });





    $(document).ready(function() {
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function(event){
    event.preventDefault();
    var content = $('.modal-body');
    content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    });

    });
</script>
</body>
</html>