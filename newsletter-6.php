<?php
/**
 * Template Name: Newsletter 6
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
</head>
<body>

    <!--Preloader-->
    <div class="preloader position-fixed w-100">
        <div class="loaderContainer">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>

    <!--Mobile Device Landscape Mode Message-->
    <div class="landscape">
        <div class="landscape__text">Please turn your device</div>
    </div>


    <main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <ul class="p-0 text-center mb-4 pb-5">
                            <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
                        </ul>
                        <h2 class="text-center underline underline-light inner-template-heading">AXIOS RECOMMENDS</h2>
                        <div class="content mx-auto pb-5">
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center pb-5">
                                Summer is the ideal time to enjoy a weekend brunch with your friends, have a romantic dinner outdoors and then drink a signature cocktail at a
                                cozy bar in the old town. Check out the best places to brunch, dine and have a cocktail in Limassol, Kiev and Tbilisi.
                            </p>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h1 class="text-center underline underline-light inner-template-heading text-white p-5">LIMASSOL, CYPRUS</h1>
                        <div class="col-md-4 mx-auto text-center">
                            <img style="width: 30%;" class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-daniel.png">
                        </div>
                        <div class="col-md-8 mx-auto text-center">
                            <h3 class="pt-5 pb-5 text-white">Recommended by Daniel Abdel Malak, CMO of Axios Holding </h3>
                        </div>
                        <div class="text-center">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news6-limassol.png">
                        </div>

                    </div>
                </div>
            </div>
            <div class="row col-md-8 mx-auto pb-5">
                <div class="col-md-4 text-center mx-auto">
                    <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-brunch.png">
                    <h2 class="underline underline-light pt-4 text-white">Brunch</h2>
                    <p class="text-center text-white">
                        Dionysus Mansion <br>
                        & Gulya's Bar & Restaurant

                    </p>
                </div>
                <div class="col-md-4 text-center mx-auto">
                    <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-dinner.png">
                    <h2 class="underline underline-light pt-4 text-white">Dinner</h2>
                    <p class="text-center text-white">
                        To Kipriakon
                    </p>
                </div>
                <div class="col-md-4 text-center mx-auto">
                    <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-cocktail.png">
                    <h2 class="underline underline-light pt-4 text-white">Cocktail</h2>
                    <p class="text-center text-white">
                        Madame Bar
                    </p>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h1 class="text-center underline underline-light inner-template-heading text-white p-5">KIEV, UKRAINE</h1>
                        <div class="col-md-4 mx-auto text-center">
                            <img style="width: 30%;" class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/team/georgi_alexidze.png">
                        </div>
                        <div class="col-md-8 mx-auto text-center">
                            <h3 class="pt-5 pb-5 text-white">Recommended by Georgi Alexidze, CTO of Axios Holding</h3>
                        </div>
                        <div class="text-center">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-kiev.png">
                        </div>

                    </div>
                </div>
            </div>
            <div class="row col-md-8 mx-auto pb-5">
                <div class="col-md-4 text-center mx-auto">
                    <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-brunch.png">
                    <h2 class="underline underline-light pt-4 text-white">Brunch</h2>
                    <p class="text-center text-white">
                        Lyubimy Dyadya
                    </p>
                </div>
                <div class="col-md-4 text-center mx-auto">
                    <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-dinner.png">
                    <h2 class="underline underline-light pt-4 text-white">Dinner</h2>
                    <p class="text-center text-white">
                        Nam <br>
                        & Chang
                    </p>
                </div>
                <div class="col-md-4 text-center mx-auto">
                    <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-cocktail.png">
                    <h2 class="underline underline-light pt-4 text-white">Cocktail</h2>
                    <p class="text-center text-white">
                        Loggerhead <br>
                        Barman Dictat
                    </p>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid axios-bg-black heading-gray">
            <div class="row mx=0">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                        <h1 class="text-center underline underline-light inner-template-heading text-white p-5">TBILISI, GEORGIA</h1>
                        <div class="col-md-4 mx-auto text-center">
                            <img style="width: 30%;" class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-alexander.png">
                        </div>
                        <div class="col-md-8 mx-auto text-center">
                            <h3 class="pt-5 pb-5 text-white"> Recommended by Alexander Ketishvili, CEO of BigWallet </h3>
                        </div>
                        <div class="text-center">
                            <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-georgia.png">
                        </div>

                    </div>
                </div>
            </div>
            <div class="row col-md-8 mx-auto pb-5">
                <div class="col-md-4 text-center mx-auto">
                    <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-brunch.png">
                    <h2 class="underline underline-light pt-4 text-white">Brunch</h2>
                    <p class="text-center text-white">
                        Sakhachapure N1
                    </p>
                </div>
                <div class="col-md-4 text-center mx-auto">
                    <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-dinner.png">
                    <h2 class="underline underline-light pt-4 text-white">Dinner</h2>
                    <p class="text-center text-white">
                        Barbarestan <br>
                        & Tabla

                    </p>
                </div>
                <div class="col-md-4 text-center mx-auto">
                    <img class="company-newsletter-size" alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/news-cocktail.png">
                    <h2 class="underline underline-light pt-4 text-white">Cocktail</h2>
                    <p class="text-center text-white">
                        Kopala
                    </p>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
                    </div>
                </div>
            </div>
        </div>

    </div>


</main>

    <footer>
        <div class="axios-bg-white">
            <div class="container py-4 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                        <ul class="p-0 text-center mb-4">
                            <li class="d-inline-block px-3"><a href="/newsletter">About Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-2">Our Universe</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-3">Faces of Axios</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-4">News</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-5">Useful Reads</a></li>
                            <li class="d-inline-block px-3"><a href="/newsletter-6">Axios Recommends</a></li>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <h2 class="pt-3 pb-3">THE AXIOS HOLDING NEWSLETTER</h2>
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 cookie-policy-content">
                    <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>
<script>
    $(window).on('load ', function() {

        var newsletter_scroll_ctrl = new ScrollMagic.Controller();

        /***************
         * newsletter Hero Scroll Reveal Animation
         **************/
        var tween_newsletter_hero = new TimelineMax();
        tween_newsletter_hero.add([
            TweenMax.fromTo("#newsletter .hero-container h2", 1.5,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#newsletter .content", 1.3,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
            TweenMax.fromTo("#newsletter .hero-bottom-img .bg-img", 1.1,{opacity: '0'}, {ease: Power2.easeOut, opacity: '1'}),
        ]);
        var scene_anewsletter_hero = new ScrollMagic.Scene({
            triggerElement: '#newsletter',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_newsletter_hero.setTween(tween_newsletter_hero);
        scene_newsletter_hero.addTo(newsletter_scroll_ctrl);
        scene_newsletter_hero.reverse(true);

        /***************
         * newsletter Section 1 Scroll Reveal Animation
         **************/
        var tween_newsletter_section_1 = new TimelineMax();
        tween_newsletter_section_1.add([
            TweenMax.staggerFromTo("#newsletter-section-1 .team-container .team-member",0.4, {x: "-220px", opacity: '0'}, {ease: Power2.easeOut, x: 0, opacity: '1'}, 0.25),
        ]);
        var scene_tween_newsletter_section_1 = new ScrollMagic.Scene({
            triggerElement: '.team-container',
            triggerHook: 'onEnter',
            offset: 100,
        });
        scene_tween_newsletter_section_1.setTween(tween_newsletter_section_1);
        scene_tween_newsletter_section_1.addTo(newsletter_scroll_ctrl);
        scene_tween_newsletter_section_1.reverse(true);


    });
</script>
</body>
</html>