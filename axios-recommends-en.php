<?php
/**
 * Template Name: Axios Recommends En
 * Created by PhpStorm.
 * User: astavrou
 */?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include("_styles.php"); ?>
    <style>
        h2,h3,h1,ol,li{
            font-family: "Nunito Sans", sans-serif;
        }
    </style>
    <title>Axios Holding</title>
    <meta name="robots" content="noindex">
    <?php include("_metatags.php"); ?>
    <style>
        @media screen and (max-width: 600px){
            .news2_stats h1{
                font-size: 16px;
            }
            .news2_stats h2{
                 font-size: 8px;
             }
            .news2_stats h3{
                font-size: 12px;
            }
        }
        /*start carousel */        
    	@media (min-width: 768px) {

    /* show 3 items */
    .carouselPrograms .carousel-inner .active,
    .carouselPrograms .carousel-inner .active + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        display: block;
    }

    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,
	.carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
        transition: none;
    }

    .carouselPrograms .carousel-inner .carousel-item-next,
    .carouselPrograms .carousel-inner .carousel-item-prev {
        position: relative;
        transform: translate3d(0, 0, 0);
    }

    

    /* left or forward direction */
    .carouselPrograms .active.carousel-item-left + .carousel-item-next.carousel-item-left,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item{
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* farthest right hidden item must be abso position for animations */
    .carouselPrograms .carousel-inner .carousel-item-prev.carousel-item-right {
        position: absolute;
        top: 0;
        left: 0%;
        z-index: -1;
        display: block;
        visibility: visible;
    }

    /* right or prev direction */
    .carouselPrograms .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
.carousel-item{
        margin: 2% 1.6%;
        -webkit-transition: none;
        transition: none;
}
img.img-fluid.mx-auto.d-block{
    height: 190px;
    width: 350px;
}
.thumb img{
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
}
.thumb img:hover{
    -webkit-filter: unset;
    filter: unset;
}
.panel-thumbnail:hover .thumb img{
    -webkit-filter: unset;
    filter: unset;
}
.carousel-control-prev, .carousel-control-next{
    width: 2%;
}
.number-overlay{
    position: absolute;
    top: 35%;
    left: 38%;
    color: #fff;
    font-size: 100px;
    text-align: center;
}
.places-to-go {
    height: 110px;
}
.book-info h2, .book-info p{
    color: #fff;
}
    </style>
</head>
<body>

    <!--Preloader-->
    <div class="preloader position-fixed w-100">
        <div class="loaderContainer">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>

    <!--Mobile Device Landscape Mode Message-->
    <div class="landscape">
        <div class="landscape__text">Please turn your device</div>
    </div>


    <main id="newsletter">
    <div class="position-relative">
        <div class="container-fluid px-0 hero-container">
            <div class="row mx-0">
                <div class="col-12 px-0">
                    <div class="bg-img hero-bg">
                        <img alt="newsletter-header-background" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-bg-header.png">
                    </div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12 text-left">
                                <div class="hero-content-container">

                                    <img style="width: 150px;" alt="axios-logo-horizontal" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_horizontal.svg">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">


                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-white px-0 newsletter-axios-companies">
            <div class="row mx-0 pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                             <li class="d-inline-block px-2"><a href="/axios-universe-en">Axios Universe</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-en">Axios Brands</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-en">Faces of Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-en">Axios News</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-en">Interesting Reads</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-en">Axios Recommends</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-en">Axios Poll</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ENGLISH
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-recommends-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-recommends-ru">РУССКИЙ</a></li>
                                </ul>
                            </div>
                        </ul>
                        <h1 class="text-center underline underline-light inner-template-heading">AXIOS RECOMMENDS</h1>
                        <div class="content mx-auto">
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center text-black">
                            Winter is the perfect time to spend cozy evenings at home, sitting in your beloved armchair, sipping hot tea or coffee and reading a great book. With so many books out there in any possible variations and formats, you don't always know which one to choose.
                            </p>
                            <p class="col-12 col-lg-6 px-0 mx-auto text-center text-black">
                            We got you covered one more time! Our Axios superstars picked for you the three books that you will not only enjoy but also get a lot value from.
                            </p>
                        
                        </div>
                    </div>
                    <div class="pt-5"></div>
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none separate-black">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid axios-bg-black heading-gray">
            <div class="container">
                <div class="row py-5 text-center">  
                    <div class="col-12 py-2 text-center mx-auto d-block d-sm-inline-flex">
                        <div class="col-md-4 text-left text-md-right">
                            <img alt="A Brief History of Time - Stephen Hawcking" class="pb-4" src="<?php echo get_template_directory_uri(); ?>/assets/img/stephen-hawking-book.png">
                        </div>
                        <div class="col-md-8 col-lg-6 text-left book-info">
                            <h2 class="text-left inner-template-heading">"A Brief History of Time" by Stephen Hawking</h1>
                            <p>In the ten years since its publication in 1988, Stephen Hawking's classic work has become a landmark volume in scientific writing, 
                                with more than nine million copies in forty languages sold worldwide. That edition was on the cutting edge of what was then known 
                                about the origins and nature of the universe. But the intervening years have seen extraordinary advances in the technology of 
                                observing both the micro- and the macrocosmic worlds (source: Goodreads).
                            </p>
                            <p>Recommended by Marios Antoniou, CEO of Inflyx</p>
                            <img alt="Marios Antoniou" class="" src="<?php echo get_template_directory_uri(); ?>/assets/img/mantoniou.png">
                        </div>    
                    </div>
                </div><!-- end of first book -->
                <div class="row py-5 text-center">  
                    <div class="col-12 py-2 text-center mx-auto d-block d-sm-inline-flex">
                        <div class="col-md-4 text-left text-md-right">
                            <img alt="The One Minute Manager - Kenneth H. Blanchard and Spencer Johnson" class="pb-4" src="<?php echo get_template_directory_uri(); ?>/assets/img/one-minute-manager-book.png">
                        </div>
                        <div class="col-md-8 col-lg-6 text-left book-info">
                            <h2 class="text-left inner-template-heading">"The One Minute Manager" by Kenneth H. Blanchard and Spencer Johnson</h1>
                            <p>Success Can Be Yours With The One Minute Manager. The world's foremost producer of personal development and motivational audio 
                                programs offers you the most important minute of your career. With more than two million hardcover copies in print, 
                                The One Minute Manager ranks as one of the most successful management books ever published. Now, you can listen and 
                                learn the strategies of one-minute management to save time and increase your productivity whether it be in your business, 
                                your home or even managing your children (source: Amazon).
                            </p>
                            <p>Recommended by Mila Hasaya, PM of New Age Recruiting</p>
                            <img alt="Mila Hasaya" class="" src="<?php echo get_template_directory_uri(); ?>/assets/img/mhasaya.png">
                        </div>    
                    </div>
                </div> <!-- end of second book -->
                <div class="row py-5 text-center">  
                    <div class="col-12 py-2 text-center mx-auto d-block d-sm-inline-flex">
                        <div class="col-md-4 text-left text-md-right">
                            <img alt="Influence: The Psychology of Persuasion - Robert Cialdini" class="pb-4" src="<?php echo get_template_directory_uri(); ?>/assets/img/influence-cialdini-book.png">
                        </div>
                        <div class="col-md-8 col-lg-6 text-left book-info">
                            <h2 class="text-left inner-template-heading">"Influence: The Psychology of Persuasion" by Robert Cialdini</h1>
                            <p>Influence, the classic book on persuasion, explains the psychology of why people say "yes" —and how to 
                                apply these understandings. Dr. Robert Cialdini is the seminal expert in the rapidly expanding field of 
                                influence and persuasion. His thirty-five years of rigorous, evidence-based research along with a three-year 
                                program of study on what moves people to change behavior has resulted in this highly acclaimed book (source: Goodreads).
                            </p>
                            <p>Recommended by Olexandr Diachenko, CMO of CC Loan Ukraine</p>
                            <img alt="Olexandr Diachenko" class="" src="<?php echo get_template_directory_uri(); ?>/assets/img/odiachenko.png">
                        </div>    
                    </div>
                </div><!-- end of third book -->
            </div>    
            <div class="row pt-5">
                <div class="col-12 px-0 pb-5">
                    <div class="newsletter-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x">
                    </div>
                </div>
            </div>
        </div>
</main>

    <footer>
        <div class="axios-bg-white">
            <div class="container py-4 px-md-0">
                <div class="row">
                    <div class="col-12 text-center logo-container">
                        <img alt="axios-logo-vertical" class="logo-img svg" src="<?php echo get_template_directory_uri(); ?>/assets/img/axios-logo_vertical.svg">
                    </div>
                    <div class="col-12 text-center">
                    <ul class="p-0 text-center mb-4 pb-5 newsletter-navigation">
                            <li class="d-inline-block px-2"><a href="/axios-universe-en">Axios Universe</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-brands-en">Axios Brands</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-faces-en">Faces of Axios</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-news-digest-en">Axios News</a></li>
                            <li class="d-inline-block px-2"><a href="/fintech-reads-en">Interesting Reads</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-recommends-en">Axios Recommends</a></li>
                            <li class="d-inline-block px-2"><a href="/axios-poll-en">Axios Poll</a></li>
                            <div class="dropdown d-inline drop-newsletter">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ENGLISH
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/axios-recommends-es">ESPAÑOL</a></li>
                                    <li><a href="/axios-recommends-ru">РУССКИЙ</a></li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                    <div class="col-12 social-menu">
                        <h2 class="pt-3 text-center">THE AXIOS HOLDING <br> NEWSLETTER</h2>
                        <ul class="p-0 d-flex justify-content-center text-center pt-5 pb-5">
                            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-fb.png"></a></li>
                            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-instagram.png"></a></li>
                            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><img alt="social-medial" src="<?php echo get_template_directory_uri(); ?>/assets/img/newsletter-linkedin.png"></a></li>
                        </ul>


                    </div>
                    <div class="col-12 text-center">
                        <p style="font-size: 12px;">
                            The information transmitted by this email is intended only for the employees of Axios Holding Group of Companies. This email may contain proprietary, business - confidential and/or privileged material.
                            The recipients of this email shall not forward nor copy, alter or further distribute in any way this email along with its attachments to any third party who is not currently employed by Axios Holding Group of Companies.
                        </p>
                    </div>
                    <div class="col-12 copyright-container">
                        <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© 2019 Axios Holding. All rights reserved.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 cookie-policy-content">
                    <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content"><p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better. For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie" class="btn-axios btn-axios-light" href="#">accept</a>.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-cursor"></div>
<?php include("_scripts.php"); ?>

<script>
    $('#carouselExample').on('slide.bs.carousel', function (e) {

    
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
    });





    $(document).ready(function() {
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function(event){
    event.preventDefault();
    var content = $('.modal-body');
    content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    });

    });
</script>
</body>
</html>